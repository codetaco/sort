FROM openjdk:11-jre-slim

RUN mkdir -p /sort ; \
    mkdir -p /data1 ; \
    mkdir -p /data2 ; \
    mkdir -p /work ; \
    mkdir -p /log
ADD ./target/classes/docker/etc /sort/etc/
ADD ./target/classes/common/spec /sort/spec/
ADD ./target/sort.jar \
    ./target/dependentJars/* \
    ./target/classes/docker/entrypoint.sh \
    /sort/
RUN chmod +x /sort/entrypoint.sh;

VOLUME /data1 /data2 /work /spec /log
WORKDIR /data1
ENV PATH "/sort:$PATH"
ENTRYPOINT ["/sort/entrypoint.sh"]
