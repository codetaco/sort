@echo off
java ^
 -ea:com.codetaco... ^
 -Ddebug=on ^
 -Dsort.config="^InstallBuilderInstallDir^\sort.cfg" ^
 -jar "^InstallBuilderInstallDir^\sort.jar" ^
 --diskWork ^
 --workDirectory "%TEMP%" ^
 %*