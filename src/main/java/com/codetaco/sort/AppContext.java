package com.codetaco.sort;

import com.codetaco.cli.CmdLine;
import com.codetaco.cli.ParserType;
import com.codetaco.cli.annotation.Arg;

import java.io.File;

public class AppContext {

    @Arg(caseSensitive = true, defaultValues = "UNKNOWN")
    public String version;

    @Arg(multimin = 1)
    public String[] specPath;

    @Arg(longName = "log4j", caseSensitive = true)
    public String log4jConfigFileName;

    public AppContext() {
        super();
    }

    public AppContext(final String workingDirectory) {
        final String configFileName = System.getProperty("sort.config",
                                                         workingDirectory + "/src/test/resources/sort.cfg");
        CmdLine.builder()
          .target(this)
          .parserType(ParserType.NAME_SPACE)
          .propertyFile(new File(configFileName))
          .build();
    }
}
