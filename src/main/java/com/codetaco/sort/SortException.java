package com.codetaco.sort;

public class SortException extends RuntimeException {

    public static class Builder {

        private Throwable cause;


        public Builder cause(Throwable cause) {
            this.cause = cause;
            return this;
        }

        public SortException build() {
            if (cause instanceof SortException)
                return (SortException) cause;
            return new SortException(cause);
        }
    }

    static public Builder builder() {
        return new Builder();
    }

    private SortException() {
    }

    private SortException(String message) {
        super(message);
    }

    private SortException(String message, Throwable cause) {
        super(message, cause);
    }

    private SortException(Throwable cause) {
        super(cause);
    }

    public String getMessage() {
        return getCause().getMessage();
    }

    private SortException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
