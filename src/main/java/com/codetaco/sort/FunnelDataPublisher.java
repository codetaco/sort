package com.codetaco.sort;

import com.codetaco.sort.segment.SourceProxyRecord;

public interface FunnelDataPublisher {
    void close() throws Exception;

    long getDuplicateCount();

    long getWriteCount();

    void openInput() throws Exception;

    boolean publish(SourceProxyRecord item, long phase) throws Exception;
}
