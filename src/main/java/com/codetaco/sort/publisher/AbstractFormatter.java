package com.codetaco.sort.publisher;

import com.codetaco.sort.App;
import com.codetaco.sort.Sort;
import com.codetaco.sort.aggregation.Aggregate;
import com.codetaco.sort.columns.FormatPart;
import com.codetaco.sort.parameters.DuplicateDisposition;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.FileSource;
import com.codetaco.sort.provider.RandomAccessInputSource;
import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;

@Slf4j
@Getter
@Setter
public abstract class AbstractFormatter {

    protected static final int ReasonableMaximumRecordLength = 1024;

    AbstractDestination destinationPublisher;

    private FunnelContext context;
    private byte[] originalBytes;
    private RandomAccessInputSource originalFile;
    private SourceProxyRecord previousItem;
    private byte[] previousOriginalBytes;
    private long writeCount;
    private long duplicateCount;

    public AbstractFormatter(FunnelContext funnelContext) throws ParseException, IOException {
        this.context = funnelContext;
        initialize();
    }

    protected void loadOriginalBytes(final int originalFileNumber,
                                     final SourceProxyRecord item) throws IOException {
        originalFile.read(originalFileNumber,
                          originalBytes,
                          item.getOriginalLocation(),
                          item.getOriginalSize());
    }

    public void openInput() throws ParseException, IOException {
        try {
            originalFile.open();
        } catch (final IOException e) {
            App.abort(-1, e);
        }
    }

    public boolean publish(final SourceProxyRecord item,
                           final long phase) throws Exception {
        /*
         * The same goes for the original file number. But it is important not
         * to loose this information because it is needed to get the original
         * data.
         */
        final int originalFileNumber = item.getOriginalInputFileIndex();
        item.setOriginalInputFileIndex(0);

        loadOriginalBytes(originalFileNumber, item);
        context.getProvider().loadColumns(originalBytes, item.getOriginalRecordNumber());

        if (previousItem != null) {
            /*
             * check to see if this item is in order, return false if not. The
             * originalRecordNumber is only used to order duplicates. At this
             * point it should not be used for comparisons since we want to make
             * sure we know a duplicate has been found.
             */
            int comparison = previousItem.compareTo(item, false);

            if (comparison > 0) {
                return false;
            }
            if (comparison == 0) {
                /*
                 * A duplicate record has been found.
                 */
                if (context.isAggregating()) {
                    /*
                     * Rather than write anything during an aggregation run we
                     * just aggregate until the key changes.
                     */
                    Aggregate.aggregate(context,
                                        item.getOriginalSize(),
                                        item.getOriginalRecordNumber());
                    return true;
                }
                duplicateCount++;
                if (DuplicateDisposition.FirstOnly == context.getDuplicateDisposition()
                  || DuplicateDisposition.LastOnly == context.getDuplicateDisposition())
                    /*
                     * Since the file is sorted so that the duplicate we want to
                     * retain is first, and because it was not a duplicate until
                     * after it has been seen, we can easily ignore all
                     * duplicates.
                     */ {
                    return true;
                }
            } else if (context.isUserSpecifiedOrder() && context.isAggregating()) {
                /*
                 * If there is no orderBy then there is no key to be changed. So
                 * aggregates operate on the entire file.
                 */
                Aggregate.aggregate(context,
                                    item.getOriginalSize(),
                                    item.getOriginalRecordNumber());
                return true;
            }

        } else {
            publishHeader();
            if (context.isAggregating()) {
                /*
                 * Never write the first record when aggregating. Wait until the
                 * key changes.
                 */
                Aggregate.aggregate(context,
                                    item.getOriginalSize(),
                                    item.getOriginalRecordNumber());
                previousOriginalBytes = Arrays.copyOf(originalBytes, item.getOriginalSize());
                previousItem = item;
                return true;
            }
        }

        if (context.isAggregating()) {
            /*
             * We must reload the previous values into the columns since the new
             * set of records has already started.
             */
            context.getProvider().loadColumns(previousOriginalBytes, previousItem.getOriginalRecordNumber());
            formatOutputAndWrite(previousItem, previousOriginalBytes);
            /*
             * Now reload the newest record into the columns for processing.
             */
            context.getProvider().loadColumns(originalBytes, item.getOriginalRecordNumber());
            Aggregate.aggregate(context, item.getOriginalSize(), item.getOriginalRecordNumber());

        } else {
            formatOutputAndWrite(item, originalBytes);
        }
        /*
         * Return the instance for reuse.
         */
        if (previousItem != null) {
            previousItem.release();
        }

        previousItem = item;
        previousOriginalBytes = Arrays.copyOf(originalBytes, item.getOriginalSize());
        return true;
    }

    public long getWriteCount() {
        return writeCount;
    }

    private void writeColumn(FormatPart part, ByteArrayOutputStream formattedColumn) {
        //
    }

    private void writeRow(ByteArrayOutputStream formattedRow) throws RuntimeException {
        try {
            getDestinationPublisher().write(formattedRow.toByteArray(), 0, formattedRow.size());
        } catch (IOException e) {
            log.error("attempting to write header output", e);
            throw new RuntimeException("attempting to write header output", e);
        }
    }

    protected void publishHeader() throws Exception {
        if (context.getHeaderOutHelper().isWaitingToWrite()) {
            context.getHeaderOutHelper().format(context,
                                                destinationPublisher,
                                                this::writeColumn,
                                                this::writeRow);
            newLine();
        }
    }

    protected abstract void newLine() throws IOException;

    protected void formatOutputAndWrite(final SourceProxyRecord item,
                                        final byte[] rawData) throws Exception {
        writeCount++;
        /*
         * Prepare the aggregations for the next set of data.
         */
        Aggregate.reset(context);
    }

    private void initialize() throws ParseException, IOException {
        if (context.isCacheInput() || context.isSysin()) {
            originalFile = context.getInputCache();
        } else {
            originalFile = new FileSource(context);
        }
        writeCount = duplicateCount = 0;
    }

    public void close() throws Exception {
        if (context.isAggregating() && previousItem != null)
            /*
             * Write last aggregation to disk
             */ {
            formatOutputAndWrite(previousItem, previousOriginalBytes);
        }
        if (destinationPublisher.needsFlushing()) {
            destinationPublisher.flushWritesToDisk();
        }
        originalFile.close();

        context.outputCounters(duplicateCount, writeCount);

        if (duplicateCount > 0) {
            log.debug("{} duplicate rows", Sort.ByteFormatter.format(duplicateCount));
        }
        log.debug("{} rows written", Sort.ByteFormatter.format(writeCount));
    }
}
