package com.codetaco.sort.publisher;

import com.codetaco.sort.App;
import com.codetaco.sort.columns.ColumnWriter;
import com.codetaco.sort.parameters.FunnelContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.ParseException;

@Slf4j
@Getter
@Setter
abstract public class AbstractDestination implements ColumnWriter {

    static final int WriteBufferSize = 1 << 15;

    private FunnelContext context;
    private DataOutput writer;
    private byte[] writeBuffer;
    private ByteBuffer bb;

    protected AbstractDestination(final FunnelContext _context) throws ParseException, IOException {
        context = _context;
        initialize();
        writeBuffer = new byte[WriteBufferSize];
        bb = ByteBuffer.wrap(writeBuffer, 0, WriteBufferSize);

        log.debug("write buffer size is {} bytes", WriteBufferSize);
    }

    void flushWritesToDisk() throws IOException {
        writer.write(bb.array(), 0, bb.position());
        bb.position(0);
    }

    protected abstract void close() throws Exception;

    private void initialize() throws ParseException, IOException {
        try {
            openOutput(context);
        } catch (final IOException e) {
            App.abort(-1, e);
        }
    }

    boolean needsFlushing() {
        return bb.position() != 0;
    }

    protected abstract void openOutput(final FunnelContext context) throws IOException;

    @Override
    public void write(final byte[] sourceBytes,
                      final int offset,
                      final int length) throws IOException {
        if (length + bb.position() >= WriteBufferSize) {
            flushWritesToDisk();
        }
        bb.put(sourceBytes, offset, length);
    }
}
