package com.codetaco.sort.publisher.formatter;

import com.codetaco.sort.columns.FormatPart;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthProvider;
import com.codetaco.sort.publisher.AbstractFormatter;
import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class FixedLengthFormatter extends AbstractFormatter {

    public FixedLengthFormatter(final FunnelContext context) throws ParseException, IOException {
        super(context);
        setOriginalBytes(new byte[VariableLengthProvider.MAX_VARIABLE_LENGTH_RECORD_SIZE]);
    }

    private void writeRow(ByteArrayOutputStream formattedRow) throws RuntimeException {
        try {
            getDestinationPublisher().write(formattedRow.toByteArray(), 0, formattedRow.size());
        } catch (IOException e) {
            log.error("attempting to write output", e);
            throw new RuntimeException("attempting to write output", e);
        }
    }

    private void writeColumn(FormatPart part, ByteArrayOutputStream formattedColumn) {
        //
    }

    @Override
    protected void newLine() throws IOException {
        //
    }

    @Override
    protected void formatOutputAndWrite(final SourceProxyRecord item,
                                        final byte[] rawData) throws Exception {
        getContext().getFormatOutHelper().format(getOriginalBytes(),
                                                 getContext().getFixedRecordLengthOut(),
                                                 item,
                                                 false,
                                                 this::writeColumn,
                                                 this::writeRow);
        super.formatOutputAndWrite(item, rawData);
    }

    @Override
    protected void loadOriginalBytes(final int originalFileNumber, final SourceProxyRecord item)
      throws IOException {
        /*
         * Make sure to delimit the current record length in the input buffer.
         */
        for (int b = 0; b < getOriginalBytes().length; b++) {
            getOriginalBytes()[b] = ' ';
        }
        super.loadOriginalBytes(originalFileNumber, item);
    }
}
