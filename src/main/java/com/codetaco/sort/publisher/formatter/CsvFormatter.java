package com.codetaco.sort.publisher.formatter;

import com.codetaco.sort.columns.FormatPart;
import com.codetaco.sort.parameters.CSVDef;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

@Slf4j
public class CsvFormatter extends VariableLengthFormatter {

    public CsvFormatter(final FunnelContext context) throws ParseException, IOException {
        super(context);
    }

    @Override
    protected void publishHeader() throws Exception {
        super.publishHeader();
        /*
         * This is the first time publishing to this file. So lets see if there
         * is a header tucked away in the csvIn context area. We will write that
         * out first.
         */
//        CSVDef csvDefIn = getContext().getCsvDefIn();
        CSVDef csvDefOut = getContext().getCsvDefOut();
//        if (csvDefIn != null && csvDefIn.header && csvDefIn.headerContents != null) {
//            getDestinationPublisher().write(csvDefIn.headerContents,
//                                            0,
//                                            csvDefIn.headerContents.length);
//            newLine();
//        }
        if (csvDefOut.header) {
            if (getContext().getFormatOutDefs() == null) {
                byte[] originalHeader = getContext().getCsvDefOut().getHeaderContents();
                getDestinationPublisher().write(originalHeader,
                                                0,
                                                originalHeader.length);
                newLine();
            } else {
                getContext().getFormatOutDefs()
                  .forEach(colOutDef -> columnObjects.add(colOutDef.getColumnName()));
                String row = getContext().getCsvDefOut().getFormat()
                  .withFirstRecordAsHeader()
                  .format(columnObjects.toArray());
                columnObjects.clear();
                getDestinationPublisher().write(row.getBytes(), 0, row.length());
                newLine();
            }
        }
    }

    private ArrayList<String> columnObjects = new ArrayList<>();

    private void writeRow(ByteArrayOutputStream formattedRow) throws RuntimeException {
        try {
            if (columnObjects.isEmpty()) {
                byte[] rowAsBytes = formattedRow.toByteArray();
                getDestinationPublisher().write(rowAsBytes, 0, rowAsBytes.length);
            } else {
                String row = getContext().getCsvDefOut().getFormat()
                  .format(columnObjects.toArray());

                columnObjects.clear();
                row = row.trim();
                getDestinationPublisher().write(row.getBytes(), 0, row.length());
            }
            newLine();
        } catch (IOException e) {
            log.error("attempting to write output", e);
            throw new RuntimeException("attempting to write output", e);
        }
    }

    private void writeColumn(FormatPart part, ByteArrayOutputStream formattedColumn) {
        if (getContext().getCsvDefOut().ignoreSurroundingSpaces) {
            columnObjects.add(formattedColumn.toString().trim());
        } else {
            columnObjects.add(formattedColumn.toString());
        }
    }

    @Override
    protected void writeOutput(SourceProxyRecord item, byte[] rawData) throws Exception {
        /*
         * The --format can be empty, causing the entire row to be written
         */
        getContext().getFormatOutHelper().format(rawData,
                                                 item.getOriginalSize(),
                                                 item,
                                                 true,
                                                 this::writeColumn,
                                                 this::writeRow);
    }
}
