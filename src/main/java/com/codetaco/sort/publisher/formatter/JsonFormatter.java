package com.codetaco.sort.publisher.formatter;

import com.codetaco.sort.columns.FormatPart;
import com.codetaco.sort.orderby.KeyType;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.segment.SourceProxyRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JsonFormatter extends VariableLengthFormatter {

    private static final byte[] ArrayStart = "[".getBytes();
    private static final byte[] ArrayEnd = "]".getBytes();
    private static final byte[] RowSeparator = ",".getBytes();

    private final Map<String, Object> jsonRowMap = new HashMap<>();

    public JsonFormatter(final FunnelContext context) throws ParseException, IOException {
        super(context);
    }

    @Override
    protected void formatOutputAndWrite(SourceProxyRecord item, byte[] rawData) throws Exception {
        if (getWriteCount() > 0) {
            getDestinationPublisher().write(RowSeparator, 0, RowSeparator.length);
        }
        super.formatOutputAndWrite(item, rawData);
    }

    private void writeRow(ByteArrayOutputStream formattedContents) throws RuntimeException {

        /*
        Ignore the formattedRow and have the CSVFormat do the work with the column objects we have collected.
         */
        try {
            byte[] rowBytes = null;
            if (jsonRowMap.isEmpty()) {
                rowBytes = formattedContents.toByteArray();
            } else {

                ObjectMapper mapper = new ObjectMapper();
                ObjectWriter writer = mapper.writer();
                StringWriter stringWriter = new StringWriter();

                writer.writeValue(stringWriter, jsonRowMap);
                jsonRowMap.clear();

                rowBytes = stringWriter.toString().getBytes();
            }
            getDestinationPublisher().write(rowBytes, 0, rowBytes.length);
            newLine();
        } catch (IOException e) {
            log.error("attempting to write output", e);
            throw new RuntimeException("attempting to write output", e);
        }
    }

    private void writeColumn(FormatPart part, ByteArrayOutputStream formattedContents) {
        try {
            if (part.getColumn().getTypeName() == KeyType.Integer) {
                jsonRowMap.put(part.getColumnName(), new Integer(formattedContents.toString()));
                return;
            }
            if (part.getColumn().getTypeName() == KeyType.Float) {
                jsonRowMap.put(part.getColumnName(), new Float(formattedContents.toString()));
                return;
            }
        } catch (Exception e) {
            //
        }
        jsonRowMap.put(part.getColumnName(), formattedContents.toString().trim());
    }

    @Override
    protected void writeOutput(SourceProxyRecord item, byte[] rawData) throws Exception {
        /*
         * The --format can be empty, causing the entire row to be written
         */
        getContext().getFormatOutHelper().format(rawData,
                                                 item.getOriginalSize(),
                                                 item,
                                                 true,
                                                 this::writeColumn,
                                                 this::writeRow);
    }

    @Override
    protected void newLine() {
//        super.newLine();
    }

    @Override
    public void openInput() throws IOException, ParseException {
        super.openInput();
        getDestinationPublisher().write(ArrayStart, 0, ArrayStart.length);
    }

    @Override
    public void close() throws Exception {
        getDestinationPublisher().write(ArrayEnd, 0, ArrayEnd.length);
        super.close();
    }
}
