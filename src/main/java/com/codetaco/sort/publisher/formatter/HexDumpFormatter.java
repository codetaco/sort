package com.codetaco.sort.publisher.formatter;

import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.parameters.HexDump;
import com.codetaco.sort.segment.SourceProxyRecord;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.Formatter;

public class HexDumpFormatter extends VariableLengthFormatter {

    private static final int CHARS_PER_DUMP_ROW = 16;
    private static final byte[] HEX_CHARS = new byte[CHARS_PER_DUMP_ROW];

    static {
        byte b;
        int i = 0;
        for (b = '0'; b <= '9'; b++) {
            HEX_CHARS[i++] = b;
        }
        for (b = 'A'; b <= 'F'; b++) {
            HEX_CHARS[i++] = b;
        }
    }

    private static int appendDecChars(final int i, final byte[] chars, final int startOffset) {
        int offset;
        int value;

        offset = startOffset;
        value = i;

        StringBuilder sb;
        try (final Formatter fmt = new Formatter(sb = new StringBuilder())) {
            fmt.format("%04d", value);
        }

        final byte[] ba = sb.toString().getBytes();

        int bao = 0;
        // chars[offset++] = ba[bao++];
        // chars[offset++] = ba[bao++];
        // chars[offset++] = ba[bao++];
        // chars[offset++] = ba[bao++];
        chars[offset++] = ba[bao++];
        chars[offset++] = ba[bao++];
        chars[offset++] = ba[bao++];
        chars[offset++] = ba[bao];

        return offset;
    }


    private static int appendHexChars(final byte b,
                                      final byte[] chars,
                                      final int startOffset) {
        final int BITS_IN_NIBBLE = 4;
        final int HIGH_VALUE_NIBBLE = 0x0F;

        int byteAsInt, offset;

        byteAsInt = b;

        offset = startOffset;
        chars[offset++] = HEX_CHARS[byteAsInt >> BITS_IN_NIBBLE & HIGH_VALUE_NIBBLE];
        chars[offset++] = HEX_CHARS[byteAsInt & HIGH_VALUE_NIBBLE];
        return offset;

    }

    private static int dumpBuffSize() {
        final int constantSpace = 13;
        final int byteOutputSize = 2;
        final int charGroupSize = 4;
        return constantSpace
          + (CHARS_PER_DUMP_ROW * byteOutputSize)
          + (CHARS_PER_DUMP_ROW / charGroupSize)
          + CHARS_PER_DUMP_ROW;
    }

    public HexDumpFormatter(final FunnelContext context) throws ParseException, IOException {
        super(context);
    }

    private void formatHexDumpAndWrite(final SourceProxyRecord item,
                                       final byte[] rawData) throws Exception {
        StringWriter sw;
        PrintWriter pw;
        String line;

        pw = new PrintWriter(sw = new StringWriter());
        pw.printf("# %d @ %d for %d",
                  item.getOriginalRecordNumber(),
                  item.getOriginalLocation(),
                  item.getOriginalSize());
        line = sw.toString();
        getDestinationPublisher().write(line.getBytes(), 0, line.length());
        newLine();

        for (final HexDump dumpee : getContext().getHexDumps()) {
            final KeyPart column = getContext().getColumnHelper().get(dumpee.columnName);
            if (column != null) {
                /*
                 * field header
                 */
                pw = new PrintWriter(sw = new StringWriter());
                pw.printf("%s", dumpee.columnName);

                line = sw.toString();
                getDestinationPublisher().write(line.getBytes(), 0, line.length());
                newLine();
            }

            if (column != null) {
                hexDump(column.getContentsAsByteArray(), column.getOffset());
            } else {
                hexDump(rawData, 0, item.getOriginalSize());
            }
        }
        /*
         * record separator
         */
        newLine();
    }

    @Override
    protected void formatOutputAndWrite(final SourceProxyRecord item,
                                        final byte[] rawData) throws Exception {
        super.formatOutputAndWrite(item, rawData);
        formatHexDumpAndWrite(item, rawData);
    }

    private void hexDump(final byte[] bytesToDump,
                         final int fieldOffsetInRow) throws IOException {
        hexDump(bytesToDump, fieldOffsetInRow, bytesToDump.length);
    }

    private void hexDump(final byte[] bytesToDump,
                         final int fieldOffsetInRow,
                         final int length) throws IOException {
        hexDump(bytesToDump, fieldOffsetInRow, length, new byte[dumpBuffSize()]);
    }

    @Override
    protected void writeOutput(SourceProxyRecord item, byte[] rawData) throws Exception {
        if (getContext().getFormatOutDefs() != null) {
            super.writeOutput(item, rawData);
        }
    }

    private void hexDump(final byte[] p_array,
                         final int p_startPrintedOffset,
                         final int p_numBytes,
                         final byte[] byteBuffer) throws IOException {
        byte asciiByteValue;
        int offset, i, relOffset, endOffset, outLen, printedOffset;
        /*
         * Compute endOffset and adjust if it would cause us to index out of
         * bounds. endOffset is 1 more than the last p_array cell we will
         * reference.
         */

        endOffset = p_numBytes;
        if (endOffset > p_array.length) {
            endOffset = p_array.length;
        }

        printedOffset = p_startPrintedOffset;
        offset = 0;

        while (offset < endOffset) {
            // Append the offset as a hex number and 1 of the 2 spaces that
            // follow it.

            outLen = 0;
            outLen = appendDecChars(printedOffset, byteBuffer, outLen);
            byteBuffer[outLen++] = ' ';

            // Now build the hex representation.
            // On the 0th, 4th, 8th, , etc. element we'll pre-pend a blank
            // If the relOffset is >= endOffset, we put in 2 blanks, not hex
            // digits.

            for (i = 0; i < CHARS_PER_DUMP_ROW; i++) {
                relOffset = offset + i;
                if ((i & 0x03) == 0) {
                    byteBuffer[outLen++] = ' ';
                }
                if (relOffset < endOffset) {
                    outLen = appendHexChars(p_array[relOffset], byteBuffer, outLen);
                } else {
                    byteBuffer[outLen++] = ' ';
                    byteBuffer[outLen++] = ' ';
                }
            }

            /*
             * Now append 2 spaces and the "| delimiter; we'll format the ASCII
             * portion now.
             */
            byteBuffer[outLen++] = ' ';
            byteBuffer[outLen++] = ' ';
            byteBuffer[outLen++] = '|';

            for (i = 0; i < CHARS_PER_DUMP_ROW; i++) {
                relOffset = offset + i;
                if (relOffset < endOffset) {
                    asciiByteValue = p_array[relOffset];
                    if (asciiByteValue < ' ' || asciiByteValue > '~') {
                        asciiByteValue = '.';
                    }
                } else {
                    asciiByteValue = ' ';
                }
                byteBuffer[outLen++] = asciiByteValue;
            }
            /*
             * Add on final "|" at the end, print to print stream, and increment
             * offset by charsPerRow.
             */
            byteBuffer[outLen++] = '|';

            getDestinationPublisher().write(byteBuffer, 0, outLen);
            newLine();

            offset += CHARS_PER_DUMP_ROW;
            printedOffset += CHARS_PER_DUMP_ROW;
        }
    }
}
