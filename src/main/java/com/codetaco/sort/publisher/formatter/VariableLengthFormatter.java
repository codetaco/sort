package com.codetaco.sort.publisher.formatter;

import com.codetaco.sort.columns.FormatPart;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.publisher.AbstractFormatter;
import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class VariableLengthFormatter extends AbstractFormatter {

    public VariableLengthFormatter(final FunnelContext context) throws ParseException, IOException {
        super(context);
        setOriginalBytes(new byte[ReasonableMaximumRecordLength]);
    }

    private void writeRow(ByteArrayOutputStream formattedRow) throws RuntimeException {
        try {
            getDestinationPublisher().write(formattedRow.toByteArray(), 0, formattedRow.size());
            newLine();
        } catch (IOException e) {
            log.error("attempting to write output", e);
            throw new RuntimeException("attempting to write output", e);
        }
    }

    private void writeColumn(FormatPart part, ByteArrayOutputStream formattedColumn) {
        //
    }

    @Override
    protected void formatOutputAndWrite(final SourceProxyRecord item,
                                        final byte[] rawData) throws Exception {
        writeOutput(item, rawData);
        super.formatOutputAndWrite(item, rawData);
    }

    protected void writeOutput(SourceProxyRecord item, byte[] rawData) throws Exception {
        /*
         * The --format can be empty, causing the entire row to be written
         */
        getContext().getFormatOutHelper().format(rawData,
                                                 item.getOriginalSize(),
                                                 item,
                                                 true,
                                                 this::writeColumn,
                                                 this::writeRow);
    }

    @Override
    protected void loadOriginalBytes(final int originalFileNumber,
                                     final SourceProxyRecord item) throws IOException {

        if (item.getOriginalSize() > getOriginalBytes().length) {
            setOriginalBytes(new byte[item.getOriginalSize() + ReasonableMaximumRecordLength]);
        }
        /*
         * Make sure to delimit the current record length in the input buffer.
         */
        getOriginalBytes()[item.getOriginalSize()] = 0x00;
        super.loadOriginalBytes(originalFileNumber, item);
    }

    @Override
    protected void newLine() throws IOException {
        getDestinationPublisher().write(getContext().getEndOfRecordDelimiterOut(),
                                        0,
                                        getContext().getEndOfRecordDelimiterOut().length);
    }
}
