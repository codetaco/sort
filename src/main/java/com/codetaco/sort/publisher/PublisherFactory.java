package com.codetaco.sort.publisher;

import com.codetaco.sort.FunnelDataPublisher;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.publisher.destination.FileDestination;
import com.codetaco.sort.publisher.destination.SysoutDestination;
import com.codetaco.sort.publisher.formatter.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.text.ParseException;

public class PublisherFactory {

    static public FunnelDataPublisher create(final FunnelContext context) throws ParseException, IOException {
        return Publisher.builder()
          .destination(createDestinationPublisher(context))
          .formatter(createPublishingFormatter(context))
          .build()
          .initialize();
    }

    @NotNull
    private static AbstractDestination createDestinationPublisher(FunnelContext context) throws ParseException, IOException {
        if (context.isSysout()) {
            return new SysoutDestination(context);
        }
        return new FileDestination(context);
    }

    @NotNull
    private static AbstractFormatter createPublishingFormatter(FunnelContext context) throws ParseException, IOException {
        if (context.isJsonOutputFormat()) {
            return new JsonFormatter(context);
        }
        if (context.isCsvOutputFormat()) {
            return new CsvFormatter(context);
        }
        if (context.isHexDumping()) {
            return new HexDumpFormatter(context);
        }
        if (context.isFixedOutputFormat()) {
            return new FixedLengthFormatter(context);
        }
        return new VariableLengthFormatter(context);
    }
}
