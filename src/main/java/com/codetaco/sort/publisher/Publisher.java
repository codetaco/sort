package com.codetaco.sort.publisher;

import com.codetaco.sort.FunnelDataPublisher;
import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.Builder;

@Builder
public class Publisher implements FunnelDataPublisher {

    private AbstractFormatter formatter;
    private AbstractDestination destination;

    Publisher initialize() {
        formatter.setDestinationPublisher(destination);
        return this;
    }

    @Override
    public void close() throws Exception {
        formatter.close();
        destination.close();
    }

    @Override
    public long getDuplicateCount() {
        return formatter.getDuplicateCount();
    }

    @Override
    public long getWriteCount() {
        return formatter.getWriteCount();
    }

    @Override
    public void openInput() throws Exception {
        formatter.openInput();
    }

    @Override
    public boolean publish(SourceProxyRecord item, long phase) throws Exception {
        return formatter.publish(item, phase);
    }
}
