package com.codetaco.sort.publisher.destination;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.publisher.AbstractDestination;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;

@Slf4j
public class FileDestination extends AbstractDestination {

    private File sortedTempFile;

    public FileDestination(final FunnelContext context) throws ParseException, IOException {
        super(context);
        log.debug("variable length file publisher activated");
    }

    @Override
    public void close() throws Exception {

        ((RandomAccessFile) getWriter()).close();

        if (getContext().getOutputFile().delete()) {
            log.trace("deleted {}", getContext().getOutputFile().getAbsolutePath());
        }

        if (!sortedTempFile.renameTo(getContext().getOutputFile())) {
            throw new IOException("failed to rename "
                                    + sortedTempFile.getAbsolutePath()
                                    + " to "
                                    + getContext().getOutputFile().getAbsolutePath());
        }

        log.trace("renamed {} to {}",
                  sortedTempFile.getAbsolutePath(),
                  getContext().getOutputFile().getAbsolutePath());

    }

    @Override
    protected void openOutput(final FunnelContext context) throws IOException {
        sortedTempFile = File.createTempFile("Sorted.",
                                             ".tmp",
                                             getContext().getOutputFile().getAbsoluteFile().getParentFile());
        sortedTempFile.deleteOnExit();
        setWriter(new RandomAccessFile(sortedTempFile, "rw"));

        log.debug("writing {}", sortedTempFile.getAbsolutePath());
    }
}
