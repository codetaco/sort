package com.codetaco.sort.publisher.destination;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.publisher.AbstractDestination;
import lombok.extern.slf4j.Slf4j;

import java.io.DataOutputStream;
import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class SysoutDestination extends AbstractDestination {

    public SysoutDestination(final FunnelContext context) throws ParseException, IOException {
        super(context);
        log.debug("variable length sysout publisher activated");
    }

    @Override
    public void close() throws Exception {
        ((DataOutputStream) getWriter()).close();
        log.debug("closing SYSOUT");
    }

    @Override
    protected void openOutput(final FunnelContext context) {
        setWriter(new DataOutputStream(System.out));
        log.debug("writing SYSOUT");
    }
}
