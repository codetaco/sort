package com.codetaco.sort.parameters;

import com.codetaco.cli.annotation.Arg;
import com.jayway.jsonpath.Configuration;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JsonDef {

    @Arg(caseSensitive = true)
    private String path;

    @Arg(inEnum = "com.jayway.jsonpath.Option",
      caseSensitive = true,
      defaultValues = "SUPPRESS_EXCEPTIONS")
    private List<String> options;

    private Configuration jsonConfiguration;
}
