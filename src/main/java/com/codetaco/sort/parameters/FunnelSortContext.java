package com.codetaco.sort.parameters;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.cli.type.WildFiles;
import com.codetaco.math.Equ;
import com.codetaco.sort.aggregation.Aggregate;
import com.codetaco.sort.columns.Field;
import com.codetaco.sort.columns.FormatPart;
import com.codetaco.sort.orderby.KeyPart;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.regex.Pattern;

public class FunnelSortContext {

    @Arg(longName = "inputFileName",
      positional = true,
      caseSensitive = true,
      multimin = 1,
      help = "The input file or files to be processed.  Wild cards are allowed in the filename only and the path (** indicates multiple path segments).  Sysin is assumed if this parameter is not provided.")
    WildFiles inputFiles;

    @Arg(longName = "url", shortName = 'u',
      caseSensitive = true,
      help = "The input URLs to be processed.")
    List<URL> inputURLs;

    @Arg(shortName = 'o',
      longName = "outputFileName",
      caseSensitive = true,
      help = "The output file to be written.  Sysout is assumed if this parameter is not provided.  The same name as the input file is allowed.")
    java.io.File outputFile;

    @Arg(shortName = 'r', longName = "replace", help = "Replace the input file with the results.")
    boolean inPlaceSort;

    @Arg(longName = "headerIn",
      help = "Column definitions defining the file header layout.",
      factoryMethod = "com.codetaco.sort.orderby.KeyType.create",
      factoryArgName = "typeName",
      excludeArgs = {"csvFieldNumber", "direction"})
    List<KeyPart> headerInDefs;

    @Arg(longName = "headerOut",
      help = "Column references defining the output file header layout.")
    List<FormatPart> headerOutDefs;

    @Arg(longName = "fixedIn",
      range = {"1", "4096"},
      help = "The record length in a fixed record length file.")
    int fixedRecordLengthIn;

    @Arg(longName = "fixedOut",
      range = {"1", "4096"},
      help = "The record length in a fixed record length file.  This is used to change an output file into a fixed format.  It is not necessary if --fixedIn is specified.")
    int fixedRecordLengthOut;

    @Arg(longName = "columnsIn",
      factoryMethod = "com.codetaco.sort.orderby.KeyType.create",
      factoryArgName = "typeName",
      multimin = 1,
      help = "Column definitions defining the input file layout.")
    List<KeyPart> inputColumnDefs;

    @Arg(longName = "field",
      help = "Create a new field")
    List<Field> fieldDefs;

    @Arg(longName = "formatOut",
      help = "Column references defining the output file layout.")
    List<FormatPart> formatOutDefs;

    @Arg(longName = "variableOutput",
      help = "The byte(s) that end each line in a variable length record file.  This will be used to write the output file as a variable length file.  If this is not specified then the --variableInput value will be used.")
    byte[] endOfRecordDelimiterOut;

    @Arg(shortName = 'w',
      longName = "where",
      allowMetaphone = true,
      help = "Rows that evaluate to TRUE are selected for Output.  See \"Math\" for details.  Columns are used as variables in this Math equation.")
    List<Equ> whereEqu;

    @Arg(shortName = 's',
      longName = "stopWhen",
      allowMetaphone = true,
      help = "The sort will stop reading input when this equation returns TRUE.  See \"Math\" for details.  Columns are used as variables in this Math equation.")
    List<Equ> stopEqu;

    @Arg(longName = "variableInput",
      help = "The byte(s) that end each line in a variable length record file.")
    byte[] endOfRecordDelimiterIn;

    @Arg(shortName = 'd',
      longName = "duplicate",
      defaultValues = {"original"},
      help = "Special handling of duplicate keyed rows.")
    DuplicateDisposition duplicateDisposition;

    @Arg(shortName = 'c',
      longName = "copy",
      defaultValues = {"byKey"},
      help = "Defines the process that will take place on the input.")
    CopyOrder copyOrder;

    @Arg(longName = "rowMax",
      defaultValues = {"9223372036854775807"},
      range = {"2"},
      help = "Used for variable length input, estimate the number of rows.  Too low could cause problems.")
    public long maximumNumberOfRows;

    @Arg(longName = "orderBy", help = "The sort keys defined from columns.")
    List<OrderBy> orderBys;

    @Arg(longName = "hexDump", help = "Columns that will be shown in hex format.")
    List<HexDump> hexDumps;

    @Arg(longName = "count",
      factoryMethod = "newCount",
      help = "Count the number of records per unique sort key",
      multimin = 1,
      multimax = 1,
      excludeArgs = {"columnName", "equation"})
    @Arg(longName = "avg",
      factoryMethod = "newAvg",
      help = "A list of columns that will be analyzed for their respective average values per unique sort key.")
    @Arg(longName = "max",
      factoryMethod = "newMax",
      help = "A list of columns that will be analyzed for their respective maximum values per unique sort key.")
    @Arg(longName = "min",
      factoryMethod = "newMin",
      help = "A list of columns that will be analyzed for their respective minimum values per unique sort key.")
    @Arg(longName = "sum",
      factoryMethod = "newSum",
      help = "A list of columns that will be analyzed for their respective summary values per unique sort key.")
    List<Aggregate> aggregates;

    @Arg(longName = "csvIn",
      help = "The definition of the CSV file being read as input.  Using this indicates that the input is in fact a CSV file and the columns parameter must use the --field arguments.")
    public CSVDef csvIn;

    @Arg(longName = "csvOut",
      help = "The definition of the CSV file being written.  Using this indicates that the output is in fact a CSV file.")
    public CSVDef csvOut;

    @Arg(longName = "jsonIn",
      help = "The definition of the JSON file being read as input.  Using this indicates that the input is in fact a JSON file and the columns parameter must use the --path arguments.")
    public JsonDef jsonIn;

    @Arg(longName = "jsonOut",
      excludeArgs = {"options", "path"},
      help = "The definition of the JSON file being written.  Using this indicates that the output is in fact a JSON file.")
    public JsonDef jsonOut;

    @Arg(caseSensitive = true, help = "The directory where temp files will be handled.")
    File workDirectory;

    @Arg(longName = "nocache",
      help = "Caching the input file into memory is faster.  This will turn off the feature.")
    boolean noCacheInput;

    @Arg(help = "Work files are stored on disk.  The amount of memory required to hold work areas in memory is about (2 * (keySize + 24)).")
    boolean diskWork;

    @Arg(longName = "power",
      defaultValues = "16",
      range = {"2", "16"},
      help = "The depth of the funnel.  The bigger this number is, the more memory will be used.  This is computed when --max or -f is specified.")
    int depth;

    @Arg(help = "Check the command - will not run")
    boolean syntaxOnly;

    @Arg(help = "Display the version of FunnelSort")
    public boolean version;

    @Arg(help = "The default time zone for date/times when no specific zone information is present.",
      caseSensitive = true)
    public String zoneId;

    @Arg(help = "Display the possible time zones to use with the zoneId parameter.")
    public Pattern showZoneIds;

    FunnelSortContext() {
    }
}
