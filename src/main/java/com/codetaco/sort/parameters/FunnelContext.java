package com.codetaco.sort.parameters;

import com.codetaco.cli.CmdLine;
import com.codetaco.cli.misc.ByteUtil;
import com.codetaco.cli.type.WildFiles;
import com.codetaco.date.CalendarFactory;
import com.codetaco.math.Equ;
import com.codetaco.sort.AppContext;
import com.codetaco.sort.FunnelDataProvider;
import com.codetaco.sort.FunnelDataPublisher;
import com.codetaco.sort.aggregation.Aggregate;
import com.codetaco.sort.aggregation.AggregateCount;
import com.codetaco.sort.columns.ColumnHelper;
import com.codetaco.sort.columns.Field;
import com.codetaco.sort.columns.FieldHelper;
import com.codetaco.sort.columns.FormatOutHelper;
import com.codetaco.sort.columns.FormatPart;
import com.codetaco.sort.columns.HeaderHelper;
import com.codetaco.sort.columns.HeaderOutHelper;
import com.codetaco.sort.orderby.Filler;
import com.codetaco.sort.orderby.KeyHelper;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.orderby.KeyType;
import com.codetaco.sort.provider.AbstractInputCache;
import com.codetaco.sort.provider.ProviderFactory;
import com.codetaco.sort.publisher.PublisherFactory;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Getter
@Setter
public class FunnelContext {

    static private void showSystemParameters() {
        @SuppressWarnings("unchecked") final Enumeration<String> pEnumerator = (Enumeration<String>) System.getProperties().propertyNames();
        while (pEnumerator.hasMoreElements()) {
            final String name = pEnumerator.nextElement();
            // if ("java.library.path".equalsIgnoreCase(name)
            // || "java.endorsed.dirs".equalsIgnoreCase(name)
            // || "sun.boot.library.path".equalsIgnoreCase(name)
            // || "java.class.path".equalsIgnoreCase(name)
            // || "java.home".equalsIgnoreCase(name)
            // || "java.ext.dirs".equalsIgnoreCase(name)
            // || "sun.boot.class.path".equalsIgnoreCase(name))
            // continue;

            if ("line.separator".equalsIgnoreCase(name)) {
                final byte[] ls = System.getProperties().getProperty(name).getBytes();
                if (ls.length == 1) {
                    log.debug("JVM: {}={}", name, ls[0]);
                } else {
                    log.debug("JVM: {}={} {}", name, ls[0], ls[1]);
                }
                continue;
            }
            if ("java.version".equalsIgnoreCase(name)) {
                log.debug("Java version: {}", System.getProperties().getProperty(name));
                continue;
            }
            log.debug("JVM: {}={}", name, System.getProperties().getProperty(name));
        }
    }

    private final FunnelSortContext fsc;

    String[] args;
    private int inputFileIndex;
    private String specDirectory;
    private FunnelDataProvider provider;
    private FunnelDataPublisher publisher;
    private AbstractInputCache inputCache;
    private KeyHelper keyHelper;
    private FormatOutHelper formatOutHelper;
    private HeaderOutHelper headerOutHelper;
    private ColumnHelper columnHelper;
    private FieldHelper fieldHelper;
    private HeaderHelper headerHelper;
    private long comparisonCounter;
    private long duplicateCount;
    private long writeCount;
    private long unselectedCount;
    private long recordCount;
    private List<KeyPart> keys;

    private boolean defaultVariableInputAssumed;
    private boolean defaultVariableOutputAssumed;
    private boolean defaultFixedOutputAssumed;
    private boolean defaultCsvOutputAssumed;
    private boolean defaultJsonOutputAssumed;

    private CmdLine parser;

    public FunnelContext(final AppContext cfg, final String... args) throws IOException, ParseException {

        fsc = new FunnelSortContext();
        parser = createCommandLineParser(cfg, fsc, args);

        if (isUsageRun()) {
            return;
        }
        if (isVersion()) {
            log.info("version {}", cfg.version);
            System.out.println("Sort " + cfg.version);
            return;
        }
        if (isShowZoneIds()) {
            showZoneIds();
            return;
        }
        if (isZoneId()) {
            setDefaultTimeZone(fsc.zoneId);
        }

        /*
         * The parser would normally apply defaults but the generator does not
         * provide for java code to be executed for default values.
         */
        if (fsc.workDirectory == null) {
            fsc.workDirectory = new File(System.getProperty("java.io.tmpdir"));
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("commandline:");
            for (final String arg : args) {
                sb.append(" ").append(arg);
            }
            log.info(sb.toString());

            showSystemParameters();
            postParseAnalysis();
            showParameters();
            provider = ProviderFactory.create(this);
            publisher = PublisherFactory.create(this);
            log.debug("============= INITIALIZED ================");
        } catch (final ParseException pe) {
            // log.fatal(pe.getMessage());
            pe.fillInStackTrace();
            throw pe;
        }
    }

    private CmdLine createCommandLineParser(AppContext cfg,
                                            FunnelSortContext context,
                                            String... args) {
        CmdLine.Builder builder = CmdLine.builder();
        builder.help("Sort is a sort / copy / merge utility.\n\nVersion "
                       + cfg.version
                       + ".  The log4j configuration file is "
                       + cfg.log4jConfigFileName
                       + ".");
        if (cfg.specPath != null) {
            for (int p = 0; p < cfg.specPath.length; p++) {
                builder.includeDirectory(new File(cfg.specPath[p]));
            }
        }
        return builder
                 .args(args)
                 .target(context)
                 .build();
    }

    public boolean isPreprocessOnlyRun() {
        return isUsageRun() || isVersion() || isSyntaxOnly() || isShowZoneIds();
    }

    private void setDefaultTimeZone(String zoneId) {
        ZoneId zone = ZoneId.of(zoneId);
        CalendarFactory.setZoneId(zone);
    }

    private void showZoneIds() {
        ZoneOffset.getAvailableZoneIds().stream()
          .filter(zoneId -> fsc.showZoneIds.matcher(zoneId).find())
          .forEach(System.out::println);
    }

    private Aggregate getAggregateByName(final String name) {
        if (getAggregates() != null) {
            for (final Aggregate agg : getAggregates()) {
                if (agg.name.equalsIgnoreCase(name)) {
                    return agg;
                }
            }
        }
        return null;
    }

    public List<Aggregate> getAggregates() {
        return fsc.aggregates;
    }

    public CopyOrder getCopyOrder() {
        return fsc.copyOrder;
    }

    public CSVDef getCsvDefIn() {
        return fsc.csvIn;
    }

    public CSVDef getCsvDefOut() {
        return fsc.csvOut;
    }

    public JsonDef getJsonDefIn() {
        return fsc.jsonIn;
    }

    private JsonDef getJsonDefOut() {
        return fsc.jsonOut;
    }

    public int getDepth() {
        return fsc.depth;
    }

    public long getDuplicateCount() {
        return duplicateCount;
    }

    public DuplicateDisposition getDuplicateDisposition() {
        return fsc.duplicateDisposition;
    }

    public byte[] getEndOfRecordDelimiterIn() {
        return fsc.endOfRecordDelimiterIn;
    }

    public byte[] getEndOfRecordDelimiterOut() {
        return fsc.endOfRecordDelimiterOut;
    }

    public int getFixedRecordLengthIn() {
        return fsc.fixedRecordLengthIn;
    }

    public int getFixedRecordLengthOut() {
        return fsc.fixedRecordLengthOut;
    }

    public List<FormatPart> getFormatOutDefs() {
        return fsc.formatOutDefs;
    }

    public List<Field> getFormatInDefs() {
        return fsc.fieldDefs;
    }

    private List<KeyPart> getHeaderInDefs() {
        return fsc.headerInDefs;
    }

    public List<FormatPart> getHeaderOutDefs() {
        return fsc.headerOutDefs;
    }

    public List<HexDump> getHexDumps() {
        return fsc.hexDumps;
    }

    public List<KeyPart> getInputColumnDefs() {
        return fsc.inputColumnDefs;
    }

    public File getInputFile(final int fileNumber) throws ParseException, IOException {
        return fsc.inputFiles.files().get(fileNumber);
    }

    public List<URL> getInputURLs() {
        return fsc.inputURLs;
    }

    public WildFiles getInputFiles() {
        return fsc.inputFiles;
    }

    public List<KeyPart> getKeys() {
        return keys;
    }

    public long getMaximumNumberOfRows() {
        return fsc.maximumNumberOfRows;
    }

    private List<OrderBy> getOrderBys() {
        return fsc.orderBys;
    }

    public File getOutputFile() {
        return fsc.outputFile;
    }

    public long getRecordCount() {
        return recordCount;
    }

    public List<Equ> getStopEqu() {
        return fsc.stopEqu;
    }

    public long getUnselectedCount() {
        return unselectedCount;
    }

    public List<Equ> getWhereEqu() {
        return fsc.whereEqu;
    }

    public File getWorkDirectory() {
        return fsc.workDirectory;
    }

    public long getWriteCount() {
        return writeCount;
    }

    public void inputCounters(final long p_unselectedCount, final long p_recordCount) {
        unselectedCount += p_unselectedCount;
        recordCount += p_recordCount;
    }

    public int inputFileCount() throws ParseException, IOException {
        if (getInputFiles() == null) {
            return 0;
        }
        return getInputFiles().files().size();
    }

    public int inputFileIndex() {
        return inputFileIndex;
    }

    public boolean isAggregating() {
        return getAggregates() != null && !getAggregates().isEmpty();
    }

    public boolean isCacheInput() {
        return !fsc.noCacheInput;
    }

    public boolean isCacheWork() {
        return !fsc.diskWork;
    }

    public boolean isDiskWork() {
        return fsc.diskWork;
    }

    public boolean isHexDumping() {
        return fsc.hexDumps != null;
    }

    private boolean isAnOutputFormatSpecified() {
        return isJsonOutputFormat()
                 || isCsvOutputFormat()
                 || isVariableLengthOutputFormat()
                 || isFixedOutputFormat();
    }

    private boolean isAnInputFormatSpecified() {
        return isJsonInputFormat()
                 || isCsvInputFormat()
                 || isVariableLengthInputFormat()
                 || isFixedInputFormat()
                 || isDefaultVariableInputAssumed();
    }

    public boolean isFixedOutputFormat() {
        return parser.isParsed("--fixedOut") || isDefaultFixedOutputAssumed();
    }

    private boolean isFixedInputFormat() {
        return parser.isParsed("--fixedIn");
    }

    private boolean noColumnsDefined() {
        return !parser.isParsed("--columnsIn");
    }

    public boolean isInPlaceSort() {
        return parser.isParsed("--replace");
    }

    public boolean isMultisourceInput() throws ParseException, IOException {
        return (isInputFile() && getInputFiles().files().size() > 1)
                 && (!isURLInput());
    }

    public boolean isURLInput() {
        return parser.isParsed("--url");
    }

    private boolean isFileInputExisting() throws ParseException, IOException {
        return isInputFile() && getInputFiles().files().size() > 0;
    }

    private boolean isCsvInputFormat() {
        return parser.isParsed("--csvIn");
    }

    public boolean isCsvOutputFormat() {
        return parser.isParsed("--csvOut") || isDefaultCsvOutputAssumed();
    }

    private boolean isJsonInputFormat() {
        return parser.isParsed("--jsonIn");
    }

    public boolean isJsonOutputFormat() {
        return parser.isParsed("--jsonOut") || isDefaultJsonOutputAssumed();
    }

    private boolean isInputFile() {
        return parser.isParsed("--inputfilename");
    }

    public boolean isNoCacheInput() {
        return fsc.noCacheInput;
    }

    public boolean isSyntaxOnly() {
        return fsc.syntaxOnly;
    }

    public boolean isSysin() {
        return !isInputFile() && !isURLInput();
    }

    public boolean isSysout() throws ParseException, IOException {
        if (isMultisourceInput() && isInPlaceSort()) {
            return false;
        }
        return getOutputFile() == null;
    }

    public boolean isUsageRun() {
        return parser.isUsage();
    }

    public boolean isUserSpecifiedOrder() {
        return getOrderBys() == null || getOrderBys().isEmpty();
    }

    private boolean isVariableLengthInputFormat() {
        return parser.isParsed("--variableIn") || isDefaultVariableInputAssumed();
    }

    boolean isVariableLengthOutputFormat() {
        return parser.isParsed("--variableOutput") || isDefaultVariableOutputAssumed();
    }

    public boolean isVersion() {
        return fsc.version;
    }

    private boolean isShowZoneIds() {
        return parser.isParsed("--showZoneIds");
    }

    private boolean isZoneId() {
        return parser.isParsed("--zoneId");
    }

    public void outputCounters(final long p_duplicateCount, final long p_writeCount) {
        duplicateCount += p_duplicateCount;
        writeCount += p_writeCount;
    }

    private void postParseAggregation() throws ParseException {
        if (getAggregates() != null) {
            final List<String> aggregateNamesFoundSoFar = new ArrayList<>();

            for (final Aggregate agg : getAggregates()) {
                if (aggregateNamesFoundSoFar.contains(agg.name)) {
                    throw new ParseException("aggregate \"" + agg.name + "\" must have a unique name", 0);
                }
                aggregateNamesFoundSoFar.add(agg.name);

                if (agg instanceof AggregateCount) {
                    continue;
                }

                if (columnHelper.exists(agg.name)) {
                    throw new ParseException("aggregate \"" + agg.name + "\" is already defined as a column", 0);
                }

                if (agg.columnName != null) {
                    if (!columnHelper.exists(agg.columnName)) {
                        throw new ParseException("aggregate \""
                                                   + agg.name
                                                   + "\" must reference a defined column: "
                                                   + agg.columnName, 0);
                    }

                    final KeyPart col = columnHelper.get(agg.columnName);
                    if ((col.isNumeric() && !agg.supportsNumber())
                          || (col.isDate() && !agg.supportsDate())
                          || (!col.isNumeric() && !col.isDate())) {
                        throw new ParseException("aggregate \""
                                                   + agg.name
                                                   + "\" must reference a numeric or date column: "
                                                   + agg.columnName
                                                   + " ("
                                                   + col.getTypeName()
                                                   + ")", 0);
                    }

                    if (agg.equation != null) {
                        throw new ParseException("aggregate \""
                                                   + agg.name
                                                   + "\" columnName and --equ are mutually exclusive", 0);
                    }
                }
            }
        }
    }

    private void postParseAnalysis() throws ParseException, IOException {

        fieldHelper = new FieldHelper();
        columnHelper = new ColumnHelper(fieldHelper);
        keyHelper = new KeyHelper();
        formatOutHelper = new FormatOutHelper(columnHelper, headerHelper);
        headerHelper = new HeaderHelper(fieldHelper);
        headerOutHelper = new HeaderOutHelper(headerHelper);

        postParseFormats();

        postParseInputFile();
        postParseInputURL();
        postParseHeaderIn();
        postParseHeaderOut();
        postParseInputColumns();
        postParseFormatIn();
        postParseOrderBy();
        postParseHexDumps();
        postParseAggregation();
        postParseFormatOut();
        postParseOutputFile();
        postParseEolIn();
        postParseEolOut();
        postParseJson();
        postParseCSV();
        postParseFixed();
    }

    private void postParseJson() {
        if (parser.isParsed("--jsonIn")) {
            log.debug("defining the JSON parser based on JacksonJsonNodeJsonProvider");
            Configuration.ConfigurationBuilder builder = Configuration.builder();
            builder.jsonProvider(new JacksonJsonNodeJsonProvider());
            if (getJsonDefIn().getOptions() != null) {
                builder.options(getJsonDefIn().getOptions().stream().map(Option::valueOf).collect(Collectors.toSet()));
            }
            getJsonDefIn().setJsonConfiguration(builder.build());
        }
        if (parser.isParsed("--jsonOut")) {
            log.debug("defining the JSON formatter based on JacksonJsonNodeJsonProvider");
            Configuration.ConfigurationBuilder builder = Configuration.builder();
            builder.jsonProvider(new JacksonJsonNodeJsonProvider());
            if (getJsonDefOut().getOptions() != null) {
                builder.options(getJsonDefOut().getOptions().stream().map(Option::valueOf).collect(Collectors.toSet()));
            }
            getJsonDefOut().setJsonConfiguration(builder.build());
        }
    }

    private void postParseCSV() {
        /*
         * Create a CSV parser if needed.
         */
        if (parser.isParsed("--csvIn")) {
            getCsvDefIn().format = getCsvDefIn().predefinedFormat.getFormat();
            log.debug("defining the CSV parser based on \"{}\"", getCsvDefIn().predefinedFormat.name());
            CmdLine csvParser = parser.subparser("--csvIn");

            if (!csvParser.isParsed("eol")) {
                getCsvDefIn().format = getCsvDefIn().format
                                         .withRecordSeparator(new String(System.lineSeparator().getBytes()));
            } else {
                getCsvDefIn().format = getCsvDefIn().format
                                         .withRecordSeparator(new String(getCsvDefIn().endOfRecordDelimiter));
            }

            if (csvParser.isParsed("--commentMarker")) {
                getCsvDefIn().format = getCsvDefIn().format.withCommentMarker((char) getCsvDefIn().commentMarker);
            }
            if (csvParser.isParsed("--delimiter")) {
                getCsvDefIn().format = getCsvDefIn().format.withDelimiter((char) getCsvDefIn().delimiter);
            }
            if (csvParser.isParsed("--escape")) {
                getCsvDefIn().format = getCsvDefIn().format.withEscape((char) getCsvDefIn().escape);
            }
            if (csvParser.isParsed("--ignoreEmptyLines")) {
                getCsvDefIn().format = getCsvDefIn().format.withIgnoreEmptyLines(getCsvDefIn().ignoreEmptyLines);
            }
            if (csvParser.isParsed("--ignoreSurroundingSpaces")) {
                getCsvDefIn().format = getCsvDefIn().format.withIgnoreSurroundingSpaces(getCsvDefIn().ignoreSurroundingSpaces);
            }
            if (csvParser.isParsed("--nullString")) {
                getCsvDefIn().format = getCsvDefIn().format.withNullString(getCsvDefIn().nullString);
            }
            if (csvParser.isParsed("--quote")) {
                if (getCsvDefIn().quote == 0) {
                    getCsvDefIn().format = getCsvDefIn().format.withQuote(null);
                } else {
                    getCsvDefIn().format = getCsvDefIn().format.withQuote((char) getCsvDefIn().quote);
                }
            }
        }
        /*
         * Create a CSV parser if needed.
         */
        if (parser.isParsed("--csvOut")) {
            getCsvDefOut().format = getCsvDefOut().predefinedFormat.getFormat();
            log.debug("defining the CSV formatter based on \"{}\"", getCsvDefOut().predefinedFormat.name());
            CmdLine csvParser = parser.subparser("--csvOut");

            if (!csvParser.isParsed("eol")) {
                getCsvDefOut().format = getCsvDefOut().format
                                         .withRecordSeparator(new String(System.lineSeparator().getBytes()));
            } else {
                getCsvDefOut().format = getCsvDefOut().format
                                         .withRecordSeparator(new String(getCsvDefOut().endOfRecordDelimiter));
            }

            if (csvParser.isParsed("--commentMarker")) {
                getCsvDefOut().format = getCsvDefOut().format.withCommentMarker((char) getCsvDefOut().commentMarker);
            }
            if (csvParser.isParsed("--delimiter")) {
                getCsvDefOut().format = getCsvDefOut().format.withDelimiter((char) getCsvDefOut().delimiter);
            }
            if (csvParser.isParsed("--escape")) {
                getCsvDefOut().format = getCsvDefOut().format.withEscape((char) getCsvDefOut().escape);
            }
            if (csvParser.isParsed("--ignoreEmptyLines")) {
                getCsvDefOut().format = getCsvDefOut().format.withIgnoreEmptyLines(getCsvDefOut().ignoreEmptyLines);
            }
            if (csvParser.isParsed("--ignoreSurroundingSpaces")) {
                getCsvDefOut().format = getCsvDefOut().format.withIgnoreSurroundingSpaces(getCsvDefOut().ignoreSurroundingSpaces);
            }
            if (csvParser.isParsed("--nullString")) {
                getCsvDefOut().format = getCsvDefOut().format.withNullString(getCsvDefOut().nullString);
            }
            if (csvParser.isParsed("--quote")) {
                if (getCsvDefOut().quote == 0) {
                    getCsvDefOut().format = getCsvDefOut().format.withQuote(null);
                } else {
                    getCsvDefOut().format = getCsvDefOut().format.withQuote((char) getCsvDefOut().quote);
                }
            }
        }
    }

    private void postParseEolIn() {
        if (getEndOfRecordDelimiterIn() == null) {
            fsc.endOfRecordDelimiterIn = System.lineSeparator().getBytes();
        }
    }

    private void postParseEolOut() {
        if (getEndOfRecordDelimiterOut() == null) {
            fsc.endOfRecordDelimiterOut = getEndOfRecordDelimiterIn();
        }
    }

    private void postParseFormats() throws ParseException {
        if (isFixedInputFormat() && isVariableLengthInputFormat()) {
            throw new ParseException("--fixedIn and --variableIn are mutually exclusive parameters", 0);
        }
        if (isFixedInputFormat() && isCsvInputFormat()) {
            throw new ParseException("--fixedIn and --csvIn are mutually exclusive parameters", 0);
        }
        if (isFixedInputFormat() && isJsonInputFormat()) {
            throw new ParseException("--fixedIn and --jsonIn are mutually exclusive parameters", 0);
        }

        if (isVariableLengthInputFormat() && isCsvInputFormat()) {
            throw new ParseException("--variableIn and --csvIn are mutually exclusive parameters", 0);
        }
        if (isVariableLengthInputFormat() && isJsonInputFormat()) {
            throw new ParseException("--variableIn and --jsonIn are mutually exclusive parameters", 0);
        }

        if (isCsvInputFormat() && isJsonInputFormat()) {
            throw new ParseException("--csvIn and --jsonIn are mutually exclusive parameters", 0);
        }

        if (isFixedOutputFormat() && isVariableLengthOutputFormat()) {
            throw new ParseException("--fixedOut and --variableOut are mutually exclusive parameters", 0);
        }
        if (isFixedOutputFormat() && isCsvOutputFormat()) {
            throw new ParseException("--fixedOut and --csvOut are mutually exclusive parameters", 0);
        }
        if (isFixedOutputFormat() && isJsonOutputFormat()) {
            throw new ParseException("--fixedOut and --jsonOut are mutually exclusive parameters", 0);
        }

        if (isVariableLengthOutputFormat() && isCsvOutputFormat()) {
            throw new ParseException("--variableOut and --csvOut are mutually exclusive parameters", 0);
        }
        if (isVariableLengthOutputFormat() && isJsonOutputFormat()) {
            throw new ParseException("--variableOut and --jsonOut are mutually exclusive parameters", 0);
        }

        if (isCsvOutputFormat() && isJsonOutputFormat()) {
            throw new ParseException("--csvOut and --jsonOut are mutually exclusive parameters", 0);
        }

        if (!isAnInputFormatSpecified()) {
            setDefaultVariableInputAssumed(true);
        }

        if (!isAnOutputFormatSpecified()) {
            if (isFixedInputFormat()) {
                setDefaultFixedOutputAssumed(true);
                fsc.fixedRecordLengthOut = fsc.fixedRecordLengthIn;
            } else if (isVariableLengthInputFormat()) {
                setDefaultVariableOutputAssumed(true);
                fsc.endOfRecordDelimiterOut = fsc.endOfRecordDelimiterIn;
            } else if (isCsvInputFormat()) {
                setDefaultCsvOutputAssumed(true);
                fsc.csvOut = fsc.csvIn;
            } else if (isJsonInputFormat()) {
                setDefaultJsonOutputAssumed(true);
                fsc.jsonOut = fsc.jsonIn;
            } else {
                throw new ParseException("No default output can be assumed", 0);
            }
        }
    }

    private void postParseFixed() throws ParseException {
        if (isFixedOutputFormat() && isVariableLengthOutputFormat()) {
            throw new ParseException("--fixedOut and --variableOutput are mutually exclusive parameters", 0);
        }
        if (isVariableLengthOutputFormat()) {
            return;
        }
        if (getFixedRecordLengthOut() == 0) {
            fsc.fixedRecordLengthOut = getFixedRecordLengthIn();
        }

    }

    private void postParseFormatIn() throws ParseException {
        if (getFormatInDefs() == null) {
            return;
        }

        final List<String> formatInNamesFoundSoFar = new ArrayList<>();

        for (Field fin : getFormatInDefs()) {
            if (formatInNamesFoundSoFar.contains(fin.getColumnName())) {
                throw new ParseException("formatIn \"" + fin.getColumnName() + "\" must have a unique name", 0);
            }
            if (columnHelper.exists(fin.getColumnName())) {
                throw new ParseException("formatIn \"" + fin.getColumnName() + "\" is already defined as a column", 0);
            }
            formatInNamesFoundSoFar.add(fin.getColumnName());
            this.getFieldHelper().add(fin, columnHelper);
        }
    }

    private void postParseFormatOut() throws ParseException {
        if (getFormatOutDefs() != null) {

            for (final FormatPart kdef : getFormatOutDefs()) {
                try {
                    if (kdef.getOffset() == -1) { // unspecified
                        kdef.setOffset(0);
                    }

                    if (kdef.getEquation() == null) {
                        if (kdef.getColumnName() != null) {
                            if (!columnHelper.exists(kdef.getColumnName())) {
                                if (!headerHelper.exists(kdef.getColumnName())) {
                                    if (getAggregateByName(kdef.getColumnName()) == null) {
                                        throw new ParseException("--formatOut must be a defined column or header: "
                                                                   + kdef.getColumnName(), 0);
                                    }
                                    throw new ParseException(
                                      "--formatOut must be a defined column, aggregates can only be used within --equ: "
                                        + kdef.getColumnName(),
                                      0);
                                }
                            }
                        }
//                        if (kdef.getFormat() != null) {
//                            throw new ParseException("--formatOut --format is only valid with --equ", 0);
//                        }
                    } else {
                        if (kdef.getLength() == 255) {
                            throw new ParseException("--formatOut --length is required when --equ is specified", 0);
                        }
                    }

                    formatOutHelper.add(kdef);
                } catch (final Exception e) {
                    throw new ParseException(e.getMessage(), 0);
                }
            }
        }
    }

    private void postParseHeaderIn() throws ParseException {
        headerHelper.setWaitingForInput(false);
        if (getHeaderInDefs() != null) {
            headerHelper.setWaitingForInput(true);

            // headerInDefs.size() > 1
            // || (headerInDefs.get(0).columnName != null ||
            // headerInDefs.get(0).equation != null)

            /*
             * This may be overridden in the postParseHeaderOut method.
             */
            headerOutHelper.setWaitingToWrite(true);

            KeyPart previousColDef = null;
            for (final KeyPart colDef : getHeaderInDefs()) {
                try {
                    /*
                     * Provide a default length when the format is specified and
                     * the length is not.
                     */
                    setColumnDefinitionLength(colDef);
                    if (getCsvDefIn() != null) {
                        throw new ParseException("headerIn not supported for csvIn files", 0);
                    }
                    if (getJsonDefIn() != null) {
                        throw new ParseException("headerIn not supported for jsonIn files", 0);
                    }

                    if (colDef.getOffset() == -1) // unspecified
                    {
                        if (previousColDef != null) {
                            colDef.setOffset(previousColDef.getOffset() + previousColDef.getLength());
                        } else {
                            colDef.setOffset(0);
                        }
                    }

                    if (!(colDef instanceof Filler)) {
                        if (headerHelper.exists(colDef.getColumnName())) {
                            throw new ParseException("headerIn must be unique: " + colDef.getColumnName(), 0);
                        }
                        headerHelper.add(colDef);
                    }
                    previousColDef = colDef;

                } catch (final Exception e) {
                    throw new ParseException(e.getMessage(), 0);
                }
            }
        }
    }

    /**
     * Provide a default length when the format is specified and the length is not.
     */
    private void setColumnDefinitionLength(KeyPart colDef) {
        if (colDef.getLength() == KeyHelper.MAX_KEY_SIZE
              && colDef.getParseFormat() != null
              && colDef.getParseFormat().length() > 0) {
            colDef.setLength(colDef.getParseFormat().length());
            log.debug("column \"{}\" length set to {} because of format",
                      colDef.getColumnName(),
                      colDef.getLength());
        }
    }

    private void postParseHeaderOut() throws ParseException {
        if (getHeaderOutDefs() != null) {
            if (isCsvOutputFormat()) {
                throw new ParseException("--csvOut and --headerOut are mutually exclusive parameters", 0);
            }
            if (isJsonOutputFormat()) {
                throw new ParseException("--jsonOut and --headerOut are mutually exclusive parameters", 0);
            }
            /*
             * --headerOut(), no args, means to suppress the headerIn from being
             * written.
             */
            headerOutHelper.setWaitingToWrite(
              getHeaderOutDefs().size() > 1
                || (getHeaderOutDefs().get(0).getColumnName() != null
                      || getHeaderOutDefs().get(0).getEquation() != null));

            for (final FormatPart kdef : getHeaderOutDefs()) {
                try {
                    if (kdef.getOffset() == -1) // unspecified
                    {
                        kdef.setOffset(0);
                    }

                    if (kdef.getColumnName() != null) {
                        if (!headerHelper.exists(kdef.getColumnName())) {
                            throw new ParseException(
                              "--headerOut must be a defined headerIn: " + kdef.getColumnName(), 0);
                        }
                    }
                    if (kdef.getColumnName() != null && kdef.getEquation() != null) {
                        throw new ParseException("--headerOut columnName and --equ are mutually exclusive", 0);
                    }
                    if (kdef.getFormat() != null && kdef.getEquation() == null) {
                        throw new ParseException("--headerOut --format is only valid with --equ", 0);
                    }

                    if (kdef.getEquation() != null) {
                        if (kdef.getLength() == 255) {
                            throw new ParseException("--headerOut --length is required when --equ is specified", 0);
                        }
                    }

                    headerOutHelper.add(kdef);
                } catch (final Exception e) {
                    throw new ParseException(e.getMessage(), 0);
                }
            }
        }
    }

    private void postParseHexDumps() throws ParseException {
        /*
         * Convert OrderBys into sort keys
         */
        if (getHexDumps() != null && !getHexDumps().isEmpty()) {
            if (getHexDumps().size() == 1 && getHexDumps().get(0).columnName == null)
                /*
                 * Full record dump
                 */ {
                return;
            }

            if (getAggregates() != null) {
                throw new ParseException("HexDump with aggregate processing is not supported", 0);
            }
            if (!isVariableLengthOutputFormat() && (getFixedRecordLengthIn() > 0 || getFixedRecordLengthOut() > 0)) {
                throw new ParseException("HexDump is only valid with variableOutput", 0);
            }
            if (isInPlaceSort()) {
                throw new ParseException("HexDump is not valid with --replace", 0);
            }

            for (final HexDump hexDump : getHexDumps()) {
                if (!columnHelper.exists(hexDump.columnName)) {
                    throw new ParseException("HexDump must be a defined column: " + hexDump.columnName, 0);
                }
                final KeyPart column = columnHelper.get(hexDump.columnName);
                if (KeyType.String != column.getTypeName() && KeyType.Byte != column.getTypeName()) {
                    throw new ParseException("HexDump can only be on String or Byte columns: " + hexDump.columnName, 0);
                }
            }
        }
    }

    private void postParseInputColumns() throws ParseException {
        int jsonFieldNumber = 0;
        if (getInputColumnDefs() != null) {
            KeyPart previousColDef = null;
            for (final KeyPart colDef : getInputColumnDefs()) {
                try {
                    setColumnDefinitionLength(colDef);
                    /*
                     * Compute an offset if one was not specified. But only for
                     * non-csvIn, non-jsonIn files since offset is not part of the those
                     * specification.
                     */
                    if (getCsvDefIn() == null && getJsonDefIn() == null) {
                        if (colDef.getOffset() == -1) // unspecified
                        {
                            if (previousColDef != null) {
                                colDef.setOffset(previousColDef.getOffset() + previousColDef.getLength());
                            } else {
                                colDef.setOffset(0);
                            }
                        }
                    } else {
                        colDef.setOffset(0);
                    }
                    if (getJsonDefIn() != null) {
                        colDef.setJsonFieldNumber(jsonFieldNumber++);
                    }
                    /*
                     * Since the parameter is 1-relative, an arbitrary decision,
                     * we have to subtract one from them before they can be
                     * used.
                     */
                    if (colDef.getCsvFieldNumber() > 0) {
                        colDef.setCsvFieldNumber(colDef.getCsvFieldNumber() - 1);
                    }

                    if (!(colDef instanceof Filler)) {
                        if (headerHelper.exists(colDef.getColumnName())) {
                            throw new ParseException("columnsIn must be unique from headerIn: "
                                                       + colDef.getColumnName(),
                                                     0);
                        }
                        columnHelper.add(colDef);
                    }
                    previousColDef = colDef;

                } catch (final Exception e) {
                    throw new ParseException(e.getMessage(), 0);
                }
            }
        }
        if (getJsonDefIn() != null && noColumnsDefined()) {
            throw new ParseException("--jsonIn requires at least one column", 0);
        }
        if (getCsvDefIn() != null && noColumnsDefined()) {
            throw new ParseException("--csvIn requires at least one column", 0);
        }
    }

    private void postParseInputFile() throws ParseException, IOException {

        if (isInputFile() && isURLInput()) {
            throw new ParseException("--inputFileName and --url can not be specified together", 0);
        }

        if (isInputFile() && !isFileInputExisting()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("file not found");
            if (getInputFiles() != null) {
                sb.append(": ");
                sb.append(getInputFiles().toString());
            }
            throw new ParseException(sb.toString(), 0);
        }
    }

    private void postParseInputURL() {
        //
    }

    private void postParseOrderBy() throws ParseException {
        /*
         * Convert OrderBys into sort keys
         */
        if (getOrderBys() != null && !getOrderBys().isEmpty()) {
            if (keys == null) {
                keys = new ArrayList<>();
            }
            for (final OrderBy orderBy : getOrderBys()) {
                if (!columnHelper.exists(orderBy.columnName)) {
                    throw new ParseException("OrderBy must be a defined column: " + orderBy.columnName, 0);
                }
                final KeyPart col = columnHelper.get(orderBy.columnName);
                final KeyPart newKey = col.newCopy();
                newKey.setDirection(orderBy.direction);
                keys.add(newKey);
            }
        }

        if (keys == null) {
            keyHelper.setUpAsCopy(this);
        }
        /*
         * Check for csv keys on a non-jsonIn file
         */
        if (keys != null && getJsonDefIn() == null) {
            for (final KeyPart kdef : keys) {
                if (kdef.isJson() && !kdef.isField()) {
                    throw new ParseException("unexpected jsonIn key (--path) on a non-jsonIn file", 0);
                }
            }
        }
        /*
         * Check for non-csv keys on a cvs file
         */
        if (keys != null && getJsonDefIn() != null) {
            for (final KeyPart kdef : keys) {
                if (!kdef.isJson() && !kdef.isField()) {
                    throw new ParseException("only jsonIn keys (--path) allowed on a jsonIn file", 0);
                }
            }
        }
        /*
         * Check for csv keys on a non-cvs file
         */
        if (keys != null && getCsvDefIn() == null) {
            for (final KeyPart kdef : keys) {
                if (kdef.isCsv() && !kdef.isField()) {
                    throw new ParseException("unexpected CSV key (--field) on a non-CSV file", 0);
                }
            }
        }
        /*
         * Check for non-csv keys on a cvs file
         */
        if (keys != null && getCsvDefIn() != null) {
            for (final KeyPart kdef : keys) {
                if (!kdef.isCsv() && !kdef.isField()) {
                    throw new ParseException("only CSV keys (--field) allowed on a CSV file", 0);
                }
            }
        }
        /*
         * Check for duplicate csvIn keys
         */
        if (keys != null && getCsvDefIn() != null) {
            for (final KeyPart k1 : keys) {
                for (final KeyPart k2 : keys) {
                    if (k1 != k2 && k1.getCsvFieldNumber() == k2.getCsvFieldNumber()) {
                        throw new ParseException(
                          "sorting on the same field (--field "
                            + k2.getCsvFieldNumber()
                            + ") is not allowed",
                          0);
                    }
                }
            }
        }

        if (keys != null) {
            for (final KeyPart kdef : keys) {
                try {
                    keyHelper.add(kdef, columnHelper);
                } catch (final Exception e) {
                    throw new ParseException(e.getMessage(), 0);
                }
            }
        }
    }

    private void postParseOutputFile() throws ParseException, IOException {
        if (isInPlaceSort() && getOutputFile() != null) {
            throw new ParseException("--replace and --outputFile are mutually exclusive parameters", 0);
        }

        if (isInPlaceSort() && !isInputFile()) {
            throw new ParseException("--replace requires --inputFile, redirection or piped input is not allowed", 0);
        }
        /*
         * -r is how the input file is replaced. If we set the outputFile here
         * it then becomes impossible to sort to the command line (sysout).
         */
        if (isInPlaceSort()) {
            fsc.outputFile = getInputFile(0);
        }
    }

    public void reset() throws IOException, ParseException {
        if (provider != null) {
            provider.reset();
        }
        if (publisher != null) {
            publisher = PublisherFactory.create(this);
        }
    }

    public void setDepth(final int optimalFunnelDepth) {
        fsc.depth = optimalFunnelDepth;
    }

    public void setInputFiles(final WildFiles wildFiles) {
        fsc.inputFiles = wildFiles;
    }

    public void setOutputFile(final File outputFile) {
        fsc.outputFile = outputFile;
    }

    private void showParameters() throws IOException, ParseException {
        if (isZoneId()) {
            showParametersLog(true, "setting default time-zone to \"{}\"", fsc.zoneId);
        }
        if (isSysin()) {
            showParametersLog(true, "input is SYSIN");
        }
        if (isFileInputExisting()) {
            for (final File file : getInputFiles().files()) {
                showParametersLog(true, "inputFilename = {}", file.getAbsolutePath());
            }
        }
        if (isURLInput()) {
            showParametersLog(true, "input is {}", getInputURLs());
        }

        if (isCacheInput()) {
            showParametersLog(false, "input caching enabled");
        }

        if (isSysout()) {
            showParametersLog(true, "output is SYSOUT");
        } else if (isInPlaceSort()) {
            showParametersLog(true, "outputFilename= input file name");
        } else {
            showParametersLog(true, "outputFilename= {}", getOutputFile().getAbsolutePath());
        }

        if (isCacheWork()) {
            showParametersLog(false, "work files are cached in memory");
        } else if (getWorkDirectory() != null) {
            showParametersLog(false, "work directory= {}", getWorkDirectory().getAbsolutePath());
        }

        if (specDirectory != null) {
            showParametersLog(false, "specification include path is {}", specDirectory);
        }

        if (getFixedRecordLengthIn() > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("FixedIn  = ").append(getFixedRecordLengthIn());
            if (isVariableLengthOutputFormat()) {
                sb.append(" adding VLR delimiters on output");
            }
            showParametersLog(false, sb.toString());
        } else {
            if (getMaximumNumberOfRows() != Long.MAX_VALUE) {
                showParametersLog(false, "max rows= {}", getMaximumNumberOfRows());
            }

            final StringBuilder bytes = new StringBuilder();
            bytes.append("in:");
            for (int b = 0; b < getEndOfRecordDelimiterIn().length; b++) {
                bytes.append(" ");
                bytes.append(ByteUtil.asLiteral(getEndOfRecordDelimiterIn()[b]));
            }
            if (getFixedRecordLengthOut() == 0) {
                bytes.append(", out:");
                for (int b = 0; b < getEndOfRecordDelimiterOut().length; b++) {
                    bytes.append(" ");
                    bytes.append(ByteUtil.asLiteral(getEndOfRecordDelimiterOut()[b]));
                }
            }

            showParametersLog(false, "End of line delimeter {}", bytes.toString());

            if (getCsvDefIn() != null) {
                final StringBuilder csvMsg = new StringBuilder();
                csvMsg.append("csvIn: ");
                if (getCsvDefIn().header) {
                    csvMsg.append("has header");
                } else {
                    csvMsg.append("no header");
                }
                showParametersLog(false, csvMsg.toString());
            }

            if (getJsonDefIn() != null) {
                showParametersLog(false, "jsonIn");
            }
        }
        if (getFixedRecordLengthOut() > 0) {
            showParametersLog(false, "FixedOut = " + getFixedRecordLengthOut());
        }

        showParametersLog(false, "power   = {}", getDepth());

        if (getDuplicateDisposition() != DuplicateDisposition.Original) {
            showParametersLog(false, "dups    = {}", getDuplicateDisposition().name());
        }

        for (final String colName : columnHelper.getNames()) {
            final KeyPart col = columnHelper.get(colName);
            if (getCsvDefIn() != null) {
                showParametersLog(false,
                                  "col {} {} csvField {} {}",
                                  col.getColumnName(),
                                  col.getTypeName(),
                                  col.getCsvFieldNumber(),
                                  (col.getParseFormat() == null
                                     ? ""
                                     : " format " + col.getParseFormat()));
            } else if (getJsonDefIn() != null) {
                showParametersLog(false,
                                  "col {} {} path:{} {}",
                                  col.getColumnName(),
                                  col.getTypeName(),
                                  col.getJsonFieldPath(),
                                  (col.getParseFormat() == null
                                     ? ""
                                     : " format " + col.getParseFormat()));
            } else {
                showParametersLog(false,
                                  "col \"{}\" {} offset {} length {} {}",
                                  col.getColumnName(),
                                  col.getTypeName(),
                                  col.getOffset(),
                                  col.getLength(),
                                  (col.getParseFormat() == null
                                     ? ""
                                     : " format " + col.getParseFormat()));
            }
        }

        for (final String colName : headerHelper.getNames()) {
            final KeyPart col = headerHelper.get(colName);
            showParametersLog(false, "headerIn \"{}\" {} offset {} length {} {}",
                              col.getColumnName(),
                              col.getTypeName(),
                              col.getOffset(),
                              col.getLength(),
                              (col.getParseFormat() == null
                                 ? ""
                                 : " format " + col.getParseFormat()));
        }


//        for (final String colName : fieldHelper.getNames()) {
//            final KeyPart col = fieldHelper.get(colName);
//            showParametersLog(false, "headerIn \"{}\" {} offset {} length {} {}",
//                              col.getColumnName(),
//                              col.getTypeName(),
//                              col.getOffset(),
//                              col.getLength(),
//                              (col.getParseFormat() == null
//                                 ? ""
//                                 : " format " + col.getParseFormat()));
//        }

        if (getAggregates() != null) {
            for (final Aggregate agg : getAggregates()) {
                if (agg instanceof AggregateCount) {
                    showParametersLog(false, "aggregate \"count\"");
                } else {
                    showParametersLog(false, "aggregate \"{}\" {}", agg.name, (agg.columnName == null
                                                                                 ? agg.equation.toString()
                                                                                 : agg.columnName));
                }
            }
        }

        if (getWhereEqu() != null) {
            for (final Equ equ : getWhereEqu()) {
                showParametersLog(true, "where \"{}\"", equ.toString());
            }
        }

        if (getStopEqu() != null) {
            for (final Equ equ : getStopEqu()) {
                showParametersLog(true, "stopWhen \"{}\"", equ.toString());
            }
        }

        if (keys == null) {
            log.debug("process = {} order", getCopyOrder().name());
        } else {
            for (final KeyPart def : keys) {
                showParametersLog(false,
                                  "orderBy {} {}",
                                  def.getColumnName(),
                                  def.getDirection().name());
            }
        }

        if (getFormatOutDefs() != null) {
            for (final FormatPart outDef : getFormatOutDefs()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("format ");
                formatColumnForLogOutput(outDef, sb);
            }
        }

        if (getHeaderOutDefs() != null) {
            for (final FormatPart outDef : getHeaderOutDefs()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("headerOut ");
                formatColumnForLogOutput(outDef, sb);
            }
        }
    }

    private void formatColumnForLogOutput(FormatPart outDef, StringBuilder sb) {
        if (outDef.getColumnName() != null) {
            sb.append("\"").append(outDef.getColumnName()).append("\"");
        }
        if (outDef.getEquation() != null) {
            sb.append("\"").append(outDef.getEquation().toString()).append("\"");
        }
        if (outDef.getTypeName() != null) {
            sb.append(" ").append(outDef.getTypeName().name());
        }
        if (outDef.getFormat() != null) {
            sb.append(" format \"").append(outDef.getFormat()).append("\"");
        }
        if (outDef.getFiller() != 0x00) {
            sb.append(" fill=").append(ByteUtil.asLiteral(outDef.getFiller()));
        }
        if (outDef.getLength() != 255) {
            sb.append(" length ").append(outDef.getLength());
        }
        if (outDef.getOffset() != 0) {
            sb.append(" offset ").append(outDef.getOffset());
        }
        if (outDef.getSize() != 255) {
            sb.append(" size ").append(outDef.getSize());
        }

        showParametersLog(false, sb.toString());

        if (outDef.getEquation() != null) {
            try {
                log.trace("\n{}", outDef.getEquation().showRPN());
            } catch (final Exception e) {
                log.warn("math", e);
            }
        }
    }

    private void showParametersLog(final boolean forceInfo, final String message, final Object... parms) {
        if (forceInfo || isSyntaxOnly()) {
            log.info(message, parms);
        } else {
            log.debug(message, parms);
        }
    }

    public boolean startNextInput() throws ParseException, IOException {
        /*
         * Has the last input file been read? Then return false.
         */
        if (inputFileIndex() >= (inputFileCount() - 1)) {
            return false;
        }
        inputFileIndex++;
        return true;
    }

    public boolean stopIsTrue() throws Exception {
        if (getStopEqu() == null) {
            return false;
        }

        for (final Equ equ : getStopEqu()) {
            /*
             * All of the stop equations must be true.
             */
            final Object result = equ.evaluate();
            if (result == null) {
                return false;
            }
            if (!(result instanceof Boolean)) {
                throw new Exception("--stopWhen clause must evaluate to true or false");
            }
            if (!((Boolean) result)) {
                return false;
            }
        }
        return true;
    }

    public boolean whereIsTrue() throws Exception {
        if (getWhereEqu() == null) {
            return true;
        }

        for (final Equ equ : getWhereEqu()) {
            /*
             * All of the where equations must be true.
             */
            final Object result = equ.evaluate();
            if (result == null) {
                return false;
            }
            if (!(result instanceof Boolean)) {
                throw new Exception("--where clause must evaluate to true or false");
            }
            if (!((Boolean) result)) {
                return false;
            }
        }
        return true;
    }
}
