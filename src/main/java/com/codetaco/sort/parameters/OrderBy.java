package com.codetaco.sort.parameters;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.sort.orderby.KeyDirection;

/**
 * <p>
 * OrderBy class.
 * </p>
 *
 * @author Chris DeGreef fedupforone@gmail.com
 */
public class OrderBy
{
    @Arg(positional = true,
            required = true,
            help = "A previously defined column name.")
    public String       columnName;

    @Arg(shortName = 'd',
            positional = true,
            defaultValues = "ASC",
            help = "The direction of the sort for this key. AASC and ADESC are absolute values of the key - the case of letters would not matter and the sign of numbers would not matter.")
    public KeyDirection direction;

    /**
     * <p>
     * Constructor for OrderBy.
     * </p>
     */
    public OrderBy()
    {
        super();
    }

}
