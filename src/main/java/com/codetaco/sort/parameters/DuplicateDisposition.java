package com.codetaco.sort.parameters;

/**
 * <p>
 * DuplicateDisposition class.
 * </p>
 *
 * @author Chris DeGreef fedupforone@gmail.com
 */
public enum DuplicateDisposition
{
    FirstOnly,
    LastOnly,
    Reverse,
    Original
}
