package com.codetaco.sort.parameters;

/**
 * <p>
 * CopyOrder class.
 * </p>
 *
 * @author Chris DeGreef fedupforone@gmail.com
 */
public enum CopyOrder
{
    // Random,
    Reverse,
    Original,
    ByKey
}
