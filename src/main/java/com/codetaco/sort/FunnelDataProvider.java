package com.codetaco.sort;

import java.io.IOException;
import java.text.ParseException;

public interface FunnelDataProvider {

    void loadColumns(byte[] originalData,
                     long originalRecordNumber) throws IOException;

    long actualNumberOfRows();

    void attachTo(FunnelItem item);

    void close() throws IOException, ParseException;

    long maximumNumberOfRows();

    boolean next(FunnelItem item, long phase) throws IOException, ParseException;

    void reset() throws IOException, ParseException;
}
