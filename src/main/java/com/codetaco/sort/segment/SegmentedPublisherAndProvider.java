package com.codetaco.sort.segment;

import com.codetaco.sort.FunnelDataProvider;
import com.codetaco.sort.FunnelDataPublisher;
import com.codetaco.sort.FunnelItem;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.EmptyProvider;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Stack;

@Slf4j
@Getter
@Setter
public class SegmentedPublisherAndProvider implements FunnelDataPublisher, FunnelDataProvider {

    private FunnelContext context;
    private SourceProxyRecord previousData;
    private boolean provider = false;
    private Segment writingSegment;
    private long activePhase;
    private WorkRepository workRepository;
    private Stack<Segment> segments;
    private long actualNumberOfRows;
    private long writeCount;
    private long duplicateCount;

    public SegmentedPublisherAndProvider(final FunnelContext context) throws IOException {
        /*
         * choose core or file here
         */
        this.context = context;
        if (context.isCacheWork()) {
            workRepository = new WorkCore(context);
        } else {
            workRepository = new WorkFile(context);
        }
    }

    @Override
    public void loadColumns(byte[] originalData,
                            long originalRecordNumber) {
        context.getColumnHelper().loadColumnsFromBytes(originalData, originalRecordNumber);
    }

    public void actAsProvider() {
        provider = true;
        log.trace("switched from publisher to provider");
    }

    @Override
    public long actualNumberOfRows() {
        return actualNumberOfRows;
    }

    @Override
    public void attachTo(final FunnelItem item) {
        /*
         * Attach an empty data provider if there are no segments to attach.
         */
        if (segments == null || segments.isEmpty()) {
            item.setProvider(new EmptyProvider());
            return;
        }
        /*
         * Attach the next segment to this specific top row node in the funnel.
         */
        final Segment segment = segments.pop();
        segment.setSegmentProvider(this);
        segment.attachTo(item);
    }

    @Override
    public void close() throws IOException {
        workRepository.close();
        if (provider) {
            workRepository.delete();
        }
    }

    @Override
    public long getDuplicateCount() {
        return duplicateCount;
    }

    @Override
    public long getWriteCount() {
        return writeCount;
    }

    @Override
    public long maximumNumberOfRows() {
        if (segments == null) {
            return 0L;
        }
        return segments.size();
    }

    @Override
    public boolean next(final FunnelItem item,
                        final long phase) {
        /*
         * segments handle this, see the attachTo method for details.
         */
        throw new RuntimeException("not to be called");
    }

    @Override
    public void openInput() throws IOException {
        if (!provider) {
            previousData = null;
            activePhase = -1;
        }
        workRepository.open();
    }

    @Override
    public boolean publish(final SourceProxyRecord data,
                           final long phase) throws IOException {
        if (activePhase == -1) {
            activePhase = phase;
        }
        /*
         * check to see if this item is in order, return false if not.
         */
        if (previousData != null) {
            if (previousData.compareTo(data) > 0) {
                segment(phase);
            } else if (writingSegment == null) {
                segment(phase);
            }
        }
        if (writingSegment == null) {
            segment(phase);
        }

        if (activePhase != phase) {
            log.trace("segment " + (segments.size() - 1) + " continuing into phase " + phase);
            activePhase = phase;
        }
        writingSegment.write(data);

        /*
         * Return the instance for reuse.
         */
        if (previousData != null) {
            previousData.release();
        }

        previousData = data;
        return true;
    }

    @Override
    public void reset() {
        // intentionally empty
    }

    public void segment(final long phase) throws IOException {
        activePhase = phase;

        if (segments == null) {
            segments = new Stack<>();
        }
        writingSegment = new Segment(workRepository);
        segments.push(writingSegment);
        actualNumberOfRows++;

        previousData = null;
    }
}
