package com.codetaco.sort.segment;

import com.codetaco.sort.FunnelDataProvider;
import com.codetaco.sort.FunnelItem;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.text.ParseException;

@Getter
@Setter
class Segment implements FunnelDataProvider {

    private SegmentedPublisherAndProvider segmentProvider;
    private final WorkRepository workfile;
    private long rowsInSegment;
    private long nextPosition;
    private long nextRow;

    Segment(final WorkRepository _workfile) {
        workfile = _workfile;
        nextPosition = _workfile.outputPosition();
        rowsInSegment = 0;
        nextRow = 0;
    }

    @Override
    public void loadColumns(byte[] originalData,
                            long originalRecordNumber) {
        getSegmentProvider().getContext().getColumnHelper().loadColumnsFromBytes(originalData, originalRecordNumber);
    }

    @Override
    public long actualNumberOfRows() {
        return rowsInSegment;
    }

    @Override
    public void attachTo(final FunnelItem item) {
        item.setProvider(this);
    }

    @Override
    public void close() {
        // intentionally empty
    }

    @Override
    public long maximumNumberOfRows() {
        return rowsInSegment;
    }

    @Override
    public boolean next(final FunnelItem item,
                        final long phase) throws IOException, ParseException {
        if (nextRow >= rowsInSegment) {
            /*
             * Only return 1 complete segment per phase.
             */
            if (item.getPhase() == phase) {
                item.setEndOfData(true);
                return false;
            }
            item.setPhase(phase);

            segmentProvider.attachTo(item);
            return item.next(phase);
        }

        /*
         * A new wrapper that gets passed around in the funnel.
         */
        item.setData(SourceProxyRecord.getInstance(workfile.getContext()));
        nextPosition += workfile.read(nextPosition, item.getData());
        nextRow++;

        item.setPhase(phase);

        return true;
    }

    @Override
    public void reset() {
        // intentionally empty
    }

    public void write(final SourceProxyRecord item) throws IOException {
        workfile.write(item);
        rowsInSegment++;
    }
}
