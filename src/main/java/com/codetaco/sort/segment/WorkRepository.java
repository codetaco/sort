package com.codetaco.sort.segment;

import com.codetaco.sort.parameters.FunnelContext;

import java.io.IOException;

public interface WorkRepository {
    void close() throws IOException;

    void delete() throws IOException;

    FunnelContext getContext();

    void open() throws IOException;

    long outputPosition();

    long read(final long position,
              final SourceProxyRecord rec) throws IOException;

    long write(SourceProxyRecord rec) throws IOException;
}
