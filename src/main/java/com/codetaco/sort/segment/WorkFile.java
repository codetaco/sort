package com.codetaco.sort.segment;

import com.codetaco.sort.Sort;
import com.codetaco.sort.parameters.FunnelContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

@Slf4j
@Getter
@Setter
public class WorkFile implements WorkRepository {

    static final int RecordHeaderSize = 28;
    static final int WriteBufferSize = 32768;

    private final FunnelContext context;
    private final File file;
    private RandomAccessFile raf;
    private final byte[] writeBuffer;
    private final ByteBuffer bb;
    private long writeFilePointer;

    WorkFile(final FunnelContext context) throws IOException {
        this.context = context;
        file = File.createTempFile("funnel.",
                                   ".tmp",
                                   context.getWorkDirectory());
        file.deleteOnExit();
        writeBuffer = new byte[WriteBufferSize];
        bb = ByteBuffer.wrap(writeBuffer, 0, WriteBufferSize);

        log.debug("buffer size is {} bytes", WriteBufferSize);
    }

    @Override
    public void close() throws IOException {
        if (bb.position() != 0) {
            flushWritesToDisk();
        }
        raf.close();
        /*
         * Only show the statistic when the file has been written to.
         */
        if (writeFilePointer > 0) {
            log.debug("{} bytes in work file", Sort.ByteFormatter.format(writeFilePointer).trim());
        }
        log.debug("closed {}", file.getAbsolutePath());
    }

    @Override
    public void delete() {
        if (file.delete()) {
            log.debug("deleted {}", file.getAbsolutePath());
        } else {
            log.debug("not deleted, {} not found", file.getAbsolutePath());
        }
    }

    private void flushWritesToDisk() throws IOException {
        raf.write(bb.array(), 0, bb.position());
        bb.position(0);
    }

    @Override
    public FunnelContext getContext() {
        return context;
    }

    @Override
    public void open() throws IOException {
        raf = new RandomAccessFile(file, "rw");
        bb.position(0);
        writeFilePointer = 0L;
        log.debug("opened {}", file.getAbsolutePath());
    }

    @Override
    public long outputPosition() {
        return writeFilePointer;
    }

    @Override
    public long read(final long position,
                     final SourceProxyRecord rec) throws IOException {
        raf.seek(position);
        rec.setOriginalInputFileIndex(raf.readInt());
        rec.setOriginalRecordNumber(raf.readLong());
        rec.setOriginalLocation(raf.readLong());
        rec.setOriginalSize(raf.readInt());
        rec.setSize(raf.readInt());
        rec.setSortKey(new byte[rec.getSize()]);
        final int readSize = raf.read(rec.getSortKey());
        return RecordHeaderSize + readSize;
    }

    @Override
    public long write(final SourceProxyRecord rec) throws IOException {
        final int sizeThisTime = RecordHeaderSize + rec.getSize();

        if (sizeThisTime + bb.position() >= WriteBufferSize) {
            flushWritesToDisk();
        }

        bb.putInt(rec.getOriginalInputFileIndex());
        bb.putLong(rec.getOriginalRecordNumber());
        bb.putLong(rec.getOriginalLocation());
        bb.putInt(rec.getOriginalSize());
        bb.putInt(rec.getSize());
        bb.put(rec.getSortKey(), 0, rec.getSize());

        final long startingPointer = writeFilePointer;
        writeFilePointer += sizeThisTime;

        return startingPointer;
    }
}
