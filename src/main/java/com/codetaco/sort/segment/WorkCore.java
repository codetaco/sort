package com.codetaco.sort.segment;

import com.codetaco.sort.Sort;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.AbstractInputCache;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@Setter
public class WorkCore implements WorkRepository {
    private static final int RecordHeaderSize = 28;
    private static final int WriteBufferIncrement = 32768;

    private final FunnelContext context;
    private final List<ByteBuffer> writeBuffers;
    private final List<Integer> writeBufferLengths;
    private long[] bufferStartingPosition;
    private long writeFilePointer;
    private ByteBuffer currentBuffer;

    WorkCore(final FunnelContext context) {
        this.context = context;
        writeBuffers = new ArrayList<>();
        writeBufferLengths = new ArrayList<>();
        writeFilePointer = 0L;
        currentBuffer = ByteBuffer.wrap(new byte[WriteBufferIncrement],
                                        0,
                                        WriteBufferIncrement);

        log.debug("buffer size is {} bytes", WriteBufferIncrement);
    }

    @Override
    public void close() {
        if (currentBuffer.position() > 0) {
            writeBuffers.add(currentBuffer);
            writeBufferLengths.add(currentBuffer.position());
        }

        if (writeFilePointer > 0) {
            log.debug("{} bytes used in work area", Sort.ByteFormatter.format(writeFilePointer).trim());
        }

        /*
         * Create the buffer starting position array. This will be used to
         * locate a buffer that contains a specific address.
         */
        bufferStartingPosition = new long[writeBufferLengths.size()];
        long pos = 0;
        for (int s = 0; s < writeBufferLengths.size(); s++) {
            bufferStartingPosition[s] = pos;
            pos += writeBufferLengths.get(s);
        }
    }

    @Override
    public void delete() {
        // intentionally empty
    }

    private long formatRecord(final long position,
                              final long begBufPos,
                              final SourceProxyRecord rec) {
        currentBuffer.position((int) (position - begBufPos));
        rec.setOriginalInputFileIndex(currentBuffer.getInt());
        rec.setOriginalRecordNumber(currentBuffer.getLong());
        rec.setOriginalLocation(currentBuffer.getLong());
        rec.setOriginalSize(currentBuffer.getInt());
        rec.setSize(currentBuffer.getInt());
        rec.setSortKey(new byte[rec.getSize()]);
        currentBuffer.get(rec.getSortKey());
        return RecordHeaderSize + rec.getSize();
    }

    @Override
    public void open() {
        log.trace("setting cache pointer to beginning");
        writeFilePointer = 0;
    }

    @Override
    public long outputPosition() {
        return writeFilePointer;
    }

    @Override
    public long read(final long position,
                     final SourceProxyRecord rec) {
        final long begBufPos = setCurrentBuffer(position);
        return formatRecord(position, begBufPos, rec);
    }

    private long setCurrentBuffer(final long position) {
        final int s = AbstractInputCache.findBufferIndexForPosition(position, bufferStartingPosition);
        currentBuffer = writeBuffers.get(s);
        return bufferStartingPosition[s];
    }

    @Override
    public long write(final SourceProxyRecord rec) {
        final int sizeThisTime = RecordHeaderSize + rec.getSize();

        if (sizeThisTime + currentBuffer.position() >= currentBuffer.capacity()) {
            writeBuffers.add(currentBuffer);
            writeBufferLengths.add(currentBuffer.position());
            currentBuffer = ByteBuffer.wrap(new byte[WriteBufferIncrement],
                                            0,
                                            WriteBufferIncrement);
        }

        currentBuffer.putInt(rec.getOriginalInputFileIndex());
        currentBuffer.putLong(rec.getOriginalRecordNumber());
        currentBuffer.putLong(rec.getOriginalLocation());
        currentBuffer.putInt(rec.getOriginalSize());
        currentBuffer.putInt(rec.getSize());
        currentBuffer.put(rec.getSortKey(), 0, rec.getSize());

        final long startingPointer = writeFilePointer;
        writeFilePointer += sizeThisTime;

        return startingPointer;
    }
}
