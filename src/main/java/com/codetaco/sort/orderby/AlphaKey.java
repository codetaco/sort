package com.codetaco.sort.orderby;

import com.codetaco.sort.columns.FormatOutHelper;
import com.codetaco.sort.columns.FormatPart;

import java.io.ByteArrayOutputStream;

public class AlphaKey extends KeyPart {

    static final private byte LowerA = (byte) 'a';
    static final private byte LowerZ = (byte) 'z';
    static final private byte LowerToUpper = (byte) ((byte) 'A' - LowerA);

    protected String contents;

    AlphaKey() {
        super();
    }

    void packObjectIntoKey(final KeyContext context,
                           final byte[] rawBytes) {
        int lengthThisTime = getLength();
        if (rawBytes.length < lengthThisTime) {
            lengthThisTime = rawBytes.length;
        }

        System.arraycopy(rawBytes, 0, context.getKey(), context.getKeyLength(), lengthThisTime);
        context.setKeyLength(context.getKeyLength() + lengthThisTime);

        if (getDirection() == KeyDirection.AASC || getDirection() == KeyDirection.ADESC) {
            for (int b = context.getKeyLength() - lengthThisTime; b < context.getKeyLength(); b++) {
                if (context.getKey()[b] >= LowerA && context.getKey()[b] <= LowerZ) {
                    context.getKey()[b] = (byte) (context.getKey()[b] + LowerToUpper);
                }
            }
        }

        /*
         * Delimiters at the end of strings are necessary so that unequal length
         * comparisons stop comparing rather than continue to compare into the
         * next part of the key.
         *
         * If the direction is DESC it is necessary to put an extra high-value
         * byte at the end of the keys so that the comparison of the generated
         * keys sorts shorter records to the end.
         */
        if (getDirection() == KeyDirection.DESC || getDirection() == KeyDirection.ADESC) {
            for (int b = context.getKeyLength() - lengthThisTime; b < context.getKeyLength(); b++) {
                context.getKey()[b] = (byte) (context.getKey()[b] ^ 0xff);
            }
            context.getKey()[context.getKeyLength()] = (byte) 0xff;
            context.setKeyLength(context.getKeyLength() + 1);
        } else {
            /*
             * It is necessary for ASC to put an extra low value byte at the end
             * so that shorter strings are sorted to the beginning.
             */
            context.getKey()[context.getKeyLength()] = (byte) 0x00;
            context.setKeyLength(context.getKeyLength() + 1);
        }
    }

    @Override
    public Object getContents() {
        return contents;
    }

    @Override
    public byte[] getContentsAsByteArray() {
        return contents.getBytes();
    }

    @Override
    public double getContentsAsDouble() {
        return 0;
    }

    @Override
    public boolean isNumeric() {
        return false;
    }

    @Override
    public boolean isString() {
        return true;
    }

    @Override
    public void pack(final KeyContext keyContext) throws Exception {
        parseObject(keyContext);
        packObjectIntoKey(keyContext, contents.getBytes());

        if (getNextPart() != null) {
            getNextPart().pack(keyContext);
        }
    }

    @Override
    public void parseObjectFromRawData(final byte[] bytes) {
        int endOffset = getOffset();
        for (; endOffset < getOffset() + getLength(); endOffset++) {
            if (bytes.length <= endOffset || bytes[endOffset] == 0) {
                break;
            }
        }

        setCurrentLength(FormatOutHelper.lengthToWrite(bytes,
                                                       getOffset(),
                                                       endOffset - getOffset(),
                                                       true));
        contents = new String(bytes, getOffset(), getCurrentLength());
        setUnformattedContents(contents.getBytes());
    }

    @Override
    public void formatForOutput(ByteArrayOutputStream outputBytes,
                                int originalSize,
                                FormatPart outputPart) {
        if (outputPart.getFormat() == null) {
            super.formatForOutput(outputBytes, originalSize, outputPart);
        } else {
            String result = new String(getUnformattedContents()).trim();
            final String sResult = outputPart.format(result);
            outputPart.writeToOutput(outputBytes,
                                     sResult.getBytes(),
                                     sResult.length(),
                                     sResult.length());
        }
    }
}
