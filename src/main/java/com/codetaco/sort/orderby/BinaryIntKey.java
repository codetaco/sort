package com.codetaco.sort.orderby;

import com.codetaco.sort.columns.FormatPart;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

public class BinaryIntKey extends KeyPart {

    private Long contents;

    public BinaryIntKey() {
        super();
        // assert parseFormat == null : "\"--format " + parseFormat +
        // "\" is not expected for \"BInteger\"";
        // assert length == 1 || length == 2 || length == 4 || length == 8 :
        // "Binary Integer lengths must be 1, 2, 4, or 8.";
    }

    @Override
    protected void packObjectIntoKey(final KeyContext context, final Long _longValue) {
        Long longValue = _longValue;
        if (longValue < 0) {
            if (getDirection() == KeyDirection.AASC || getDirection() == KeyDirection.ADESC) {
                longValue = 0 - longValue;
            }
        }

        if (getDirection() == KeyDirection.DESC || getDirection() == KeyDirection.ADESC) {
            longValue = 0 - longValue;
        }

        final ByteBuffer bb = ByteBuffer.wrap(context.getKey(), context.getKeyLength(), 8);
        setUnformattedContents(bb.array());

        /*
         * Flip the sign bit so negatives are before positives in ascending
         * sorts.
         */
        switch (getLength()) {
            case 1:
                bb.put((byte) ((longValue.byteValue()) ^ 0x80));
                break;
            case 2:
                bb.putShort((short) ((longValue.shortValue()) ^ 0x8000));
                break;
            case 4:
                bb.putInt(((longValue.intValue()) ^ 0x80000000));
                break;
            case 8:
                bb.putLong(longValue ^ 0x8000000000000000L);
                break;
        }
        context.setKeyLength(context.getKeyLength() + getLength());
    }

    @Override
    public Object getContents() {
        return contents;
    }

    @Override
    public double getContentsAsDouble() {
        return contents;
    }

    @Override
    public boolean isInteger() {
        return true;
    }

    @Override
    public boolean isNumeric() {
        return true;
    }

    @Override
    public void pack(final KeyContext context) throws Exception {
        parseObject(context);
        packObjectIntoKey(context, contents);

        if (getNextPart() != null) {
            getNextPart().pack(context);
        }
    }

    @Override
    public void parseObjectFromRawData(final byte[] rawBytes) throws Exception {
        if (rawBytes.length < getOffset() + getLength()) {
            throw new Exception("index out of bounds: " + (getOffset() + getLength()));
        }

        final ByteBuffer bb = ByteBuffer.wrap(rawBytes, getOffset(), 8);
        setCurrentLength(getLength());

        switch (getLength()) {
            case 1:
                contents = new Long(bb.get());
                return;
            case 2:
                contents = new Long(bb.getShort());
                return;
            case 4:
                contents = new Long(bb.getInt());
                return;
            case 8:
                contents = new Long(bb.getLong());
                return;
        }
        throw new Exception("invalid length for a binary integer field: " + getLength());
    }

    @Override
    public void formatForOutput(ByteArrayOutputStream outputBytes,
                                int originalSize,
                                FormatPart outputPart) {
        if (outputPart.getFormat() == null) {
            super.formatForOutput(outputBytes, originalSize, outputPart);
        } else {
            String result = new String(getUnformattedContents()).trim();
            final String sResult = outputPart.format(result);
            outputPart.writeToOutput(outputBytes,
                          sResult.getBytes(),
                          sResult.length(),
                          sResult.length());
        }
    }
}
