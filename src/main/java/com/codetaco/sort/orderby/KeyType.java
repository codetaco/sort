package com.codetaco.sort.orderby;

public enum KeyType {
    String(AlphaKey.class),
    Virtual(VirtualField.class),
    Integer(DisplayIntKey.class),
    Float(DisplayFloatKey.class),
    BInteger(BinaryIntKey.class),
    BFloat(BinaryFloatKey.class),
    Date(DateKey.class),
    Time(TimeKey.class),
    DateTime(DateTimeKey.class),
    Byte(ByteKey.class),
    Filler(Filler.class);

    static public KeyPart create(final String keyType) {
        final KeyType kt = KeyType.valueOf(keyType);
        return kt.create();
    }

    public KeyPart create()  {
        try {
            return (KeyPart) instanceClass.newInstance();
        } catch (Exception e) {
            return new AlphaKey();
        }
    }

    public Class<?> instanceClass;

    KeyType(final Class<?> _instanceClass) {
        this.instanceClass = _instanceClass;
    }
}
