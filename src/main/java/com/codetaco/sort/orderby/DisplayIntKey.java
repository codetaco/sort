package com.codetaco.sort.orderby;

import com.codetaco.sort.columns.FormatPart;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class DisplayIntKey extends KeyPart {

    private Long contents;
    private byte[] trimmed;

    public DisplayIntKey() {
        super();
    }

    @Override
    public Object getContents() {
        return contents;
    }

    @Override
    public double getContentsAsDouble() {
        return contents;
    }

    @Override
    public boolean isInteger() {
        return true;
    }

    @Override
    public boolean isNumeric() {
        return true;
    }

    @Override
    public void pack(final KeyContext context) throws Exception {
        parseObject(context);
        packObjectIntoKey(context, contents);

        if (getNextPart() != null) {
            getNextPart().pack(context);
        }
    }

    @SuppressWarnings("null")
    @Override
    public void parseObjectFromRawData(final byte[] rawBytes) {

        int lengthThisTime = getLength();
        if (rawBytes == null) {
            lengthThisTime = 0;
        } else if (rawBytes.length < getOffset() + getLength()) {
            lengthThisTime = rawBytes.length - getOffset();
        }

        setCurrentLength(lengthThisTime);

        if (trimmed == null) {
            trimmed = new byte[getLength()];
        }

        setUnformattedContents(Arrays.copyOfRange(rawBytes,
                                                  getOffset(),
                                                  getOffset() + lengthThisTime));

        int t = 0;
        boolean minusSignFound = false;
        for (int b = 0; b < lengthThisTime; b++) {
            final byte bb = rawBytes[getOffset() + b];
            if (bb >= (byte) '0') {
                if (bb <= (byte) '9') {
                    trimmed[t++] = bb;
                    continue;
                }
            }
            if (bb == (byte) ',') {
                continue;
            }
            if (bb == (byte) '-') {
                minusSignFound = true;
                continue;
            }
            if (bb == (byte) '.') {
                break;
            }
            if (t > 0) {
                break;
            }
        }

        if (trimmed[0] == 0x00) {
            contents = new Long(0);
            return;
        }

        contents = Long.parseLong(new String(trimmed, 0, t));
        if (minusSignFound) {
            contents = -contents;
        }
    }

    @Override
    public void formatForOutput(ByteArrayOutputStream outputBytes,
                                int originalSize,
                                FormatPart outputPart) {
        if (outputPart.getFormat() == null) {
            super.formatForOutput(outputBytes, originalSize, outputPart);
        } else {
            String result = new String(getUnformattedContents()).trim();
            final String sResult = outputPart.format(Integer.parseInt(result));
            outputPart.writeToOutput(outputBytes,
                          sResult.getBytes(),
                          sResult.length(),
                          sResult.length());
        }
    }
}
