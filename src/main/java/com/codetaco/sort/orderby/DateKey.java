package com.codetaco.sort.orderby;

import com.codetaco.date.CalendarFactory;
import com.codetaco.sort.columns.FormatPart;
import com.codetaco.sort.columns.IsoFormatsForDates;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class DateKey extends KeyPart {

    private Calendar contents;
    private SimpleDateFormat sdf;

    public DateKey() {
        super();
    }

    @Override
    public Object getContents() {
        return contents;
    }

    @Override
    public double getContentsAsDouble() {
        return contents.getTimeInMillis();
    }

    @Override
    public boolean isDate() {
        return true;
    }

    @Override
    public boolean isNumeric() {
        return false;
    }

    @Override
    public void pack(final KeyContext context) throws Exception {
        parseObject(context);
        packObjectIntoKey(context, contents.getTimeInMillis());

        if (getNextPart() != null) {
            getNextPart().pack(context);
        }
    }

    @Override
    public void parseObjectFromRawData(final byte[] rawBytes) throws Exception {
        contents = Calendar.getInstance();

        int lengthThisTime = getLength();
        if (rawBytes.length < getOffset() + getLength()) {
            lengthThisTime = rawBytes.length - getOffset();
        }

        final String trimmed = new String(rawBytes, getOffset(), lengthThisTime).trim();
        setUnformattedContents(Arrays.copyOfRange(rawBytes,
                                                  getOffset(),
                                                  getOffset() + lengthThisTime));
        long longValue = 0;
        if (trimmed.length() > 0) {
            if (getParseFormat() == null) {
                longValue = CalendarFactory.asDateLong(trimmed);
            } else {
                if (sdf == null) {
                    sdf = new SimpleDateFormat(getParseFormat());
                }
                final Date date = sdf.parse(trimmed);
                longValue = date.getTime();
            }
        }
        contents.setTimeInMillis(longValue);
        setCurrentLength(getLength());
    }

    @Override
    public void formatForOutput(ByteArrayOutputStream outputBytes,
                                int originalSize,
                                FormatPart outputPart) {
        if (outputPart.isDefaultFormattingOK()) {
            super.formatForOutput(outputBytes, originalSize, outputPart);
        } else {
            String result = new String(getUnformattedContents()).trim();
            final String sResult = outputPart.format(CalendarFactory.asZoned(result),
                                                     IsoFormatsForDates.ISO_DATE);
            outputPart.writeToOutput(outputBytes,
                                     sResult.getBytes(),
                                     sResult.length(),
                                     sResult.length());
        }
    }
}
