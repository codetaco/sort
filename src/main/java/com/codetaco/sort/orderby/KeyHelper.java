package com.codetaco.sort.orderby;

import com.codetaco.sort.columns.ColumnHelper;
import com.codetaco.sort.parameters.FunnelContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KeyHelper {

    public static final int MAX_KEY_SIZE = 255;
    private final KeyContext context;
    private final int maxKeyBytes;
    private KeyPart formatter;

    public KeyHelper() {
        this(MAX_KEY_SIZE);
    }

    KeyHelper(final int maxsize) {
        log.debug("maximum string key length is " + MAX_KEY_SIZE);

        maxKeyBytes = maxsize;
        context = new KeyContext();
    }

    /**
     * Add the key in sequence after all other keys that have already been defined. This is done through a linked list
     * of keys. Use the column helper to find the definition of the key if a column name was specified.
     *
     * @param formatter    the formatter
     * @param columnHelper the column helper
     */
    public void add(final KeyPart formatter,
                    final ColumnHelper columnHelper) {
        if (columnHelper != null && columnHelper.exists(formatter.getColumnName())) {
            final KeyPart colDef = columnHelper.get(formatter.getColumnName());
            formatter.defineFrom(colDef);
        }

        if (this.formatter == null) {
            this.formatter = formatter;
        } else {
            this.formatter.add(formatter);
        }
    }

    public KeyContext extractKey(final byte[] data,
                                 final long recordNumber) throws Exception {
        /*
         * It is likely that the provided data is a reusable buffer of bytes. So we can't just store these bytes for later
         * use.
         *
         * The extra byte is for a 0x00 character to be placed at the end of
         * String keys. This is important in order to handle keys where the user
         * specified the maximum length for a String key. Or took the default
         * sort, which is the maximum key.
         */
        context.setKey(new byte[maxKeyBytes + 1]);
        context.setKeyLength(0);
        context.setRawRecordBytes(new byte[1][]);
        context.getRawRecordBytes()[0] = data;
        context.setRecordNumber(recordNumber);

        formatter.pack(context);

        context.setRawRecordBytes(null);
        return context;
    }

    public KeyContext extractKey(final byte[][] data, final long recordNumber) throws Exception {
        /*
         * Call this method for csvIn files that break each row up into fields (byte arrays). [][].
         *
         * The extra byte is for a 0x00 character to be placed at the end of
         * String keys. This is important in order to handle keys where the user
         * specified the maximum length for a String key. Or took the default
         * sort, which is the maximum key.
         */
        context.setKey(new byte[maxKeyBytes + 1]);
        context.setKeyLength(0);
        context.setRawRecordBytes(data);
        context.setRecordNumber(recordNumber);

        formatter.pack(context);

        context.setRawRecordBytes(null);
        return context;
    }

    public void setUpAsCopy(final FunnelContext funnelContext) {
        switch (funnelContext.getCopyOrder()) {
            case ByKey:
                AlphaKey ak;
                if (funnelContext.getCsvDefIn() != null) {
                    add(ak = new AlphaKey(), null);
                    ak.setCsvFieldNumber(0);
                    ak.setOffset(0);
                    ak.setLength(MAX_KEY_SIZE);
                    ak.setDirection(KeyDirection.ASC);
                } else if (funnelContext.getJsonDefIn() != null) {
                    add(ak = new AlphaKey(), null);
                    ak.setJsonFieldPath(".*");
                    ak.setOffset(0);
                    ak.setLength(MAX_KEY_SIZE);
                    ak.setDirection(KeyDirection.ASC);
                } else {
                    add(ak = new AlphaKey(), null);
                    ak.setOffset(0);
                    ak.setLength(MAX_KEY_SIZE);
                    ak.setDirection(KeyDirection.ASC);
                }
                break;
            case Original:
                add(new RecordNumberKey(KeyDirection.ASC, null), null);
                break;
            case Reverse:
                add(new RecordNumberKey(KeyDirection.DESC, null), null);
                break;
        }
    }
}
