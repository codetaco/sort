package com.codetaco.sort.orderby;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.sort.columns.FormatPart;
import lombok.Getter;
import lombok.Setter;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

@Getter
@Setter
abstract public class KeyPart {
    private KeyPart nextPart;
    private byte[] unformattedContents;
    private int currentLength;

    /*
     * KeyPart is used as an internal representation of orderby. This most a
     * legacy issue. This direction is not used as an argument. Keytype is used
     * as an argument for several things like headers, columns and the like,
     * that have no direction associated with them. Bad design - but for now, it
     * is what it is.
     */
    private KeyDirection direction;

    @Arg(shortName = 'n', longName = "name", help = "The key name", caseSensitive = true)
    private String columnName;

    @Arg(shortName = 't',
      longName = "type",
      positional = true,
      help = "The data type of the key",
      required = true,
      caseSensitive = true)
    private KeyType typeName;

    @Arg(shortName = 'f',
      longName = "field",
      range = {"1"},
      help = "If this is a CSV file then use this instead of offset and length.  The first field is field #1 (not zero).")
    private int csvFieldNumber;

    @Arg(shortName = 'p',
      longName = "path",
      caseSensitive = true,
      help = "If this is a JSON file then use this instead of offset and length or field.  It is the path to the field")
    private String jsonFieldPath;
    private int jsonFieldNumber;

    @Arg(shortName = 'o',
      defaultValues = {"-1"},
      range = {"0"},
      help = "The zero relative offset from the beginning of a row.  This will be computed, if not specified, to be the location of the previous column plus the length of the previous column.  Most often this parameter is not needed.")
    private int offset;

    @Arg(shortName = 'l',
      defaultValues = {"255"},
      range = {"1", "255"},
      help = "The length of the key in bytes.")
    private int length;

    @Arg(shortName = 'd',
      longName = "formatC",
      caseSensitive = true,
      help = "The parsing format for converting the contents of the key in the file to an internal representation. Use Java SimpleDateFormat rules for making the format.")
    private String parseFormat;

    public KeyPart() {
        super();
        csvFieldNumber = -1;
        typeName = null;
        columnName = null;
        offset = -1;
        currentLength = 0;
    }

    protected void packObjectIntoKey(final KeyContext context, final Long _longValue) {
        Long longValue = _longValue;
        if (longValue < 0) {
            if (getDirection() == KeyDirection.AASC || getDirection() == KeyDirection.ADESC) {
                longValue = 0 - longValue;
            }
        }

        if (getDirection() == KeyDirection.DESC || getDirection() == KeyDirection.ADESC) {
            longValue = 0 - longValue;
        }

        final ByteBuffer bb = ByteBuffer.wrap(context.getKey(), context.getKeyLength(), 8);
        /*
         * Flip the sign bit so negatives are before positives in ascending
         * sorts.
         */
        bb.putLong(longValue ^ 0x8000000000000000L);
        context.setKeyLength(context.getKeyLength() + 8);
    }

    public void add(final KeyPart anotherFormatter) {
        if (nextPart == null) {
            nextPart = anotherFormatter;
        } else {
            nextPart.add(anotherFormatter);
        }
    }

    /**
     * Copy everything exception the key specific things like direction and nextPart. Even if this is the key that
     * caused the column to be defined we can still copy the values since they would be the same.
     */
    void defineFrom(final KeyPart colDef) {
        csvFieldNumber = colDef.csvFieldNumber;
        offset = colDef.offset;
        length = colDef.length;
        parseFormat = colDef.parseFormat;
        typeName = colDef.typeName;
        columnName = colDef.columnName;
        jsonFieldPath = colDef.jsonFieldPath;
        jsonFieldNumber = colDef.jsonFieldNumber;
        currentLength = 0;
    }

    abstract public Object getContents();

    public byte[] getContentsAsByteArray() {
        return unformattedContents;
    }

    abstract public double getContentsAsDouble();

    public void formatForOutput(ByteArrayOutputStream outputBytes,
                                int originalSize,
                                FormatPart outputPart) {
        outputPart.writeToOutput(outputBytes,
                                 getUnformattedContents(),
                                 getCurrentLength(),
                                 originalSize);
    }

    public boolean isCsv() {
        return csvFieldNumber >= 0;
    }

    public boolean isJson() {
        return jsonFieldPath != null;
    }

    public boolean isField() {
        return false;
    }

    public boolean isDate() {
        return false;
    }

    public boolean isString() {
        return false;
    }

    public boolean isFloat() {
        return false;
    }

    public boolean isInteger() {
        return false;
    }

    abstract public boolean isNumeric();

    public KeyPart newCopy() {
        KeyPart myCopy;
        try {
            myCopy = getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
        myCopy.csvFieldNumber = csvFieldNumber;
        myCopy.offset = offset;
        myCopy.length = length;
        myCopy.parseFormat = parseFormat;
        myCopy.typeName = typeName;
        myCopy.direction = direction;
        myCopy.columnName = columnName;
        myCopy.jsonFieldPath = jsonFieldPath;
        myCopy.jsonFieldNumber = jsonFieldNumber;
        myCopy.currentLength = currentLength;
        return myCopy;
    }

    public abstract void pack(final KeyContext context) throws Exception;

    public void parseObject(final KeyContext context) throws Exception {
        byte[] rawBytes = rawBytes(context);
        if (rawBytes != null) {
            parseObjectFromRawData(rawBytes);
        }
    }

    public abstract void parseObjectFromRawData(byte[] rawData) throws Exception;

    private byte[] rawBytes(final KeyContext context) {
        if (isCsv()) {
            return context.getRawRecordBytes()[csvFieldNumber];
        }
        if (isJson()) {
            return context.getRawRecordBytes()[jsonFieldNumber];
        }
        return context.getRawRecordBytes()[0];
    }
}
