package com.codetaco.sort.orderby;

import lombok.Getter;
import lombok.Setter;

import java.nio.ByteBuffer;

@Getter
@Setter
public class RecordNumberKey extends KeyPart {

    private Long contents;

    RecordNumberKey(final KeyDirection dir, final String _parseFormat) {
        super();
        setOffset(0);
        setLength(8);
        setDirection(dir);

        // assert parseFormat == null : "\"--format " + parseFormat +
        // "\" is not expected for \"RecordNumber\"";
    }

    private void formatObjectIntoKey(final KeyContext context, final Long _longValue) {
        Long longValue = _longValue;

        if (getDirection() == KeyDirection.DESC || getDirection() == KeyDirection.ADESC) {
            longValue = 0 - longValue;
        }

        final ByteBuffer bb = ByteBuffer.wrap(context.getKey(), context.getKeyLength(), 8);
        bb.putLong(longValue ^ 0x8000000000000000L);
        context.setKeyLength(context.getKeyLength() + 8);
    }

    @Override
    public Object getContents() {
        return contents;
    }

    @Override
    public double getContentsAsDouble() {
        return contents;
    }

    @Override
    public boolean isInteger() {
        return true;
    }

    @Override
    public boolean isNumeric() {
        return true;
    }

    @Override
    public void pack(final KeyContext context) throws Exception {
        parseObject(context);
        formatObjectIntoKey(context, contents);

        if (getNextPart() != null) {
            getNextPart().pack(context);
        }
    }

    @Override
    public void parseObject(final KeyContext context) {
        contents = context.getRecordNumber();
    }

    @Override
    public void parseObjectFromRawData(final byte[] rawBytes) {
        // not used since this is a system variable
    }
}
