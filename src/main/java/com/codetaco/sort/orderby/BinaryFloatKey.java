package com.codetaco.sort.orderby;

import com.codetaco.sort.columns.FormatPart;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

public class BinaryFloatKey extends KeyPart {

    private Double contents;

    public BinaryFloatKey() {
        super();
        // assert parseFormat == null : "\"--format " + parseFormat +
        // "\" is not expected for \"BFloat\"";
        // assert length == 4 || length == 8 :
        // "Binary Float lengths must be 4 or 8.";
    }

    private void packObjectIntoKey(final KeyContext context,
                                   final Double _doubleValue) {

        Double doubleValue = _doubleValue;

        if (doubleValue < 0) {
            if (getDirection() == KeyDirection.AASC || getDirection() == KeyDirection.ADESC) {
                doubleValue = 0 - doubleValue;
            }
        }

        if (getDirection() == KeyDirection.DESC || getDirection() == KeyDirection.ADESC) {
            doubleValue = 0 - doubleValue;
        }

        final ByteBuffer bb = ByteBuffer.wrap(context.getKey(), context.getKeyLength(), 8);

        switch (getLength()) {
            case 4:
                int intbits = (int) Double.doubleToRawLongBits(doubleValue);
                if (doubleValue < 0) {
                    intbits = ~intbits;
                } else {
                    intbits = intbits ^ 0x80000000;
                }

                bb.putLong(intbits);
                break;
            case 8:
                long longbits = Double.doubleToRawLongBits(doubleValue);
                if (doubleValue < 0) {
                    longbits = ~longbits;
                } else {
                    longbits = longbits ^ 0x8000000000000000L;
                }

                bb.putLong(longbits);
                break;
        }
        context.setKeyLength(context.getKeyLength() + getLength());
    }

    @Override
    public Object getContents() {
        return contents;
    }

    @Override
    public double getContentsAsDouble() {
        return contents;
    }

    @Override
    public boolean isFloat() {
        return true;
    }

    @Override
    public boolean isNumeric() {
        return true;
    }

    @Override
    public void pack(final KeyContext context) throws Exception {
        parseObject(context);
        packObjectIntoKey(context, contents);

        if (getNextPart() != null) {
            getNextPart().pack(context);
        }
    }

    @Override
    public void parseObjectFromRawData(final byte[] rawBytes) throws Exception {
        if (rawBytes.length < getOffset() + getLength()) {
            throw new Exception("index out of bounds: " + (getOffset() + getLength()));
        }

        final ByteBuffer bb = ByteBuffer.wrap(rawBytes, getOffset(), 8);
        setUnformattedContents(bb.array());
        setCurrentLength(getLength());

        switch (getLength()) {
            case 4:
                contents = new Double(bb.getFloat());
                return;
            case 8:
                contents = bb.getDouble();
                return;
        }
        throw new Exception("invalid length for a binary floating point field: " + getLength());
    }

    @Override
    public void formatForOutput(ByteArrayOutputStream outputBytes,
                                int originalSize,
                                FormatPart outputPart) {
        if (outputPart.getFormat() == null) {
            super.formatForOutput(outputBytes, originalSize, outputPart);
        } else {
            String result = new String(getUnformattedContents()).trim();
            final String sResult = outputPart.format(result);
            outputPart.writeToOutput(outputBytes,
                                     sResult.getBytes(),
                                     sResult.length(),
                                     sResult.length());
        }
    }
}
