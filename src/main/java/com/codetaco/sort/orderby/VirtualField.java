package com.codetaco.sort.orderby;

import com.codetaco.sort.columns.ColumnHelper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class VirtualField extends AlphaKey {

    private ColumnHelper columnHelper;

    @Override
    public boolean isField() {
        return true;
    }

    @Override
    public void parseObject(final KeyContext context) {
    }

    @Override
    public void parseObjectFromRawData(final byte[] bytes) {
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    @Override
    public int getCurrentLength() {
        return contents.length();
    }

    @Override
    public byte[] getUnformattedContents() {
        return contents.getBytes();
    }

    @Override
    public void pack(final KeyContext keyContext) throws Exception {
        contents = (String) columnHelper.get(getColumnName()).getContents();
        setLength(contents.length());
        super.pack(keyContext);
    }

    @Override
    void defineFrom(final KeyPart colDef) {
        super.defineFrom(colDef);
        VirtualField donor = (VirtualField) colDef;
        setColumnHelper(donor.columnHelper);
    }

    @Override
    public KeyPart newCopy() {
        VirtualField myCopy = (VirtualField) super.newCopy();
        myCopy.setColumnHelper(columnHelper);
        return myCopy;
    }
}
