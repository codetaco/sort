package com.codetaco.sort.orderby;

public class Filler extends KeyPart {

    @Override
    public Object getContents() {
        return null;
    }

    @Override
    public double getContentsAsDouble() {
        return 0;
    }

    @Override
    public boolean isNumeric() {
        return false;
    }

    @Override
    public void pack(final KeyContext context) {
        // n/a
    }

    @Override
    public void parseObjectFromRawData(final byte[] rawData) {
        // n/a
    }
}
