package com.codetaco.sort.orderby;

public enum KeyDirection {
    ASC,
    DESC,
    AASC,
    ADESC
}
