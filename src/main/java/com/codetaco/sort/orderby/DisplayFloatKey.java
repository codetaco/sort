package com.codetaco.sort.orderby;

import com.codetaco.sort.columns.FormatPart;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class DisplayFloatKey extends KeyPart {

    private Double contents;

    public DisplayFloatKey() {
        super();
    }

    private void formatObjectIntoKey(final KeyContext context, final Double _doubleValue) {
        Double doubleValue = _doubleValue;

        if (doubleValue < 0) {
            if (getDirection() == KeyDirection.AASC || getDirection() == KeyDirection.ADESC) {
                doubleValue = 0 - doubleValue;
            }
        }

        if (getDirection() == KeyDirection.DESC || getDirection() == KeyDirection.ADESC) {
            doubleValue = 0 - doubleValue;
        }

        final ByteBuffer bb = ByteBuffer.wrap(context.getKey(), context.getKeyLength(), 8);
        setUnformattedContents(bb.array());

        long longbits = Double.doubleToRawLongBits(doubleValue);
        if (doubleValue < 0) {
            longbits = ~longbits;
        } else {
            longbits = longbits ^ 0x8000000000000000L;
        }

        bb.putLong(longbits);
        context.setKeyLength(context.getKeyLength() + 8);
    }

    @Override
    public Object getContents() {
        return contents;
    }

    @Override
    public double getContentsAsDouble() {
        return contents;
    }

    @Override
    public boolean isFloat() {
        return true;
    }

    @Override
    public boolean isNumeric() {
        return true;
    }

    @Override
    public void pack(final KeyContext context) throws Exception {
        parseObject(context);
        formatObjectIntoKey(context, contents);

        if (getNextPart() != null) {
            getNextPart().pack(context);
        }
    }

    @Override
    public void parseObjectFromRawData(final byte[] rawBytes) {
        byte[] trimmed = new byte[getLength()];

        int lengthThisTime = getLength();
        if (rawBytes.length < getOffset() + getLength()) {
            lengthThisTime = rawBytes.length - getOffset();
        }

        setCurrentLength(lengthThisTime);
        setUnformattedContents(Arrays.copyOfRange(rawBytes,
                                                  getOffset(),
                                                  getOffset() + lengthThisTime));
        int t = 0;
        boolean minusSignFound = false;
        for (int b = 0; b < lengthThisTime; b++) {
            final byte bb = rawBytes[getOffset() + b];
            if (bb >= (byte) '0') {
                if (bb <= (byte) '9') {
                    trimmed[t++] = bb;
                    continue;
                }
            }
            if (bb == (byte) '$' || bb == (byte) ',') {
                continue;
            }
            if (bb == (byte) '-') {
                minusSignFound = true;
                continue;
            }
            if (bb == (byte) '.' || bb == (byte) 'e' || bb == (byte) 'E') {
                trimmed[t++] = bb;
                continue;
            }
            if (t > 0) {
                break;
            }
        }

        contents = Double.parseDouble(new String(trimmed, 0, t));
        if (minusSignFound) {
            contents = -contents;
        }
    }

    @Override
    public void formatForOutput(ByteArrayOutputStream outputBytes,
                                int originalSize,
                                FormatPart outputPart) {
        if (outputPart.getFormat() == null) {
            super.formatForOutput(outputBytes, originalSize, outputPart);
        } else {
            String result = new String(getUnformattedContents()).trim();
            final String sResult = outputPart.format(Float.parseFloat(result));
            outputPart.writeToOutput(outputBytes,
                          sResult.getBytes(),
                          sResult.length(),
                          sResult.length());
        }
    }
}
