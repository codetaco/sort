package com.codetaco.sort.orderby;

import com.codetaco.sort.columns.FormatPart;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class ByteKey extends KeyPart {

    private byte[] contents;

    private void formatObjectIntoKey(final KeyContext context, final byte[] rawBytes) {
        int lengthThisTime = getLength();
        if (rawBytes.length < getLength()) {
            lengthThisTime = rawBytes.length;
        }

        System.arraycopy(rawBytes, 0, context.getKey(), context.getKeyLength(), lengthThisTime);
        context.setKeyLength(context.getKeyLength() + lengthThisTime);

        if (getDirection() == KeyDirection.DESC || getDirection() == KeyDirection.ADESC) {
            for (int b = context.getKeyLength() - lengthThisTime; b < context.getKeyLength(); b++) {
                context.getKey()[b] = (byte) (context.getKey()[b] ^ 0xff);
            }
        }
    }

    @Override
    public Object getContents() {
        return contents;
    }

    @Override
    public byte[] getContentsAsByteArray() {
        return contents;
    }

    @Override
    public double getContentsAsDouble() {
        return 0;
    }

    @Override
    public boolean isNumeric() {
        return false;
    }

    @Override
    public void pack(final KeyContext context) throws Exception {
        parseObject(context);
        formatObjectIntoKey(context, contents);

        if (getNextPart() != null) {
            getNextPart().pack(context);
        }
    }

    @Override
    public void parseObjectFromRawData(final byte[] rawBytes) {
        contents = Arrays.copyOfRange(rawBytes,
                                      this.getOffset(),
                                      this.getOffset() + this.getLength());
        setUnformattedContents(contents);
        setCurrentLength(getLength());
    }

    @Override
    public void formatForOutput(ByteArrayOutputStream outputBytes,
                                int originalSize,
                                FormatPart outputPart) {
        if (outputPart.getFormat() == null) {
            super.formatForOutput(outputBytes, originalSize, outputPart);
        } else {
            String result = new String(getUnformattedContents()).trim();
            final String sResult = outputPart.format(result);
            outputPart.writeToOutput(outputBytes,
                                     sResult.getBytes(),
                                     sResult.length(),
                                     sResult.length());
        }
    }
}
