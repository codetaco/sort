package com.codetaco.sort.orderby;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeyContext {
    private long recordNumber;
    /*
     * Only the first occurrence is used for non-csvIn files. All occurrences are
     * for the fields in the csvIn row.
     */
    private byte[][] rawRecordBytes;
    private byte[] key;
    private int keyLength;
}
