package com.codetaco.sort;

import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.IOException;
import java.text.ParseException;

@Getter
@Setter
@ToString
public class FunnelItem {

    private FunnelDataProvider provider;
    private SourceProxyRecord data;
    private boolean endOfData;
    private long phase;

    public boolean next(final long _phase) throws IOException, ParseException {
        if (!provider.next(this, _phase)) {
            this.setEndOfData(true);
            return false;
        }
        return true;
    }

    public void reset() {
        data = null;
        endOfData = false;
        phase = -1;
    }
}
