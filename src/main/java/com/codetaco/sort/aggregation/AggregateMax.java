package com.codetaco.sort.aggregation;

import com.codetaco.date.CalendarFactory;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;

import java.time.ZonedDateTime;
import java.util.Calendar;

class AggregateMax extends Aggregate {
    private double maxDouble;
    private long maxLong;
    private Calendar maxCalendar;

    AggregateMax() {
        reset();
    }

    @Override
    Object getValueForEquations() {
        if (maxDouble != Double.MIN_VALUE) {
            return maxDouble;
        }
        if (maxLong != Long.MIN_VALUE) {
            return maxLong;
        }
        if (maxCalendar != null) {
            return maxCalendar;
        }
        return 0D;
    }

    @Override
    void reset() {
        maxDouble = Double.MIN_VALUE;
        maxLong = Long.MIN_VALUE;
        maxCalendar = null;
    }

    @Override
    void update(final FunnelContext context) {
        if (equation != null) {
            final Object unknownType = equation.evaluate();
            if (unknownType instanceof Double) {
                final double currentValue = ((Double) unknownType);
                if (currentValue > maxDouble) {
                    maxDouble = currentValue;
                }
                return;
            }
            if (unknownType instanceof Long) {
                final long currentValue = ((Long) unknownType);
                if (currentValue > maxLong) {
                    maxLong = currentValue;
                }
                return;
            }
            if (unknownType instanceof ZonedDateTime) {
                final Calendar currentValue = CalendarFactory.asCalendar((ZonedDateTime) unknownType);
                if (maxCalendar == null || currentValue.after(maxCalendar)) {
                    maxCalendar = currentValue;
                }
                return;
            }
            maxDouble = 0;
            return;
        }
        if (columnName != null) {
            final KeyPart col = context.getColumnHelper().get(columnName);
            if (col.isDate()) {
                final Calendar currentValue = (Calendar) col.getContents();
                if (maxCalendar == null || currentValue.after(maxCalendar)) {
                    maxCalendar = currentValue;
                }
                return;
            }
            if (col.isFloat()) {
                final double currentValue = col.getContentsAsDouble();
                if (currentValue > maxDouble) {
                    maxDouble = currentValue;
                }
                return;
            }
            if (col.isInteger()) {
                final long currentValue = (Long) col.getContents();
                if (currentValue > maxLong) {
                    maxLong = currentValue;
                }
                return;
            }
        }
        maxDouble = 0;
    }
}
