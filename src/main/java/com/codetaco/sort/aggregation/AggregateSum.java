package com.codetaco.sort.aggregation;

import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;

class AggregateSum extends Aggregate {
    private double sumDouble;
    private long sumLong;

    AggregateSum() {
        reset();
    }

    @Override
    Object getValueForEquations() {
        if (AggType.FLOAT == aggType) {
            return sumDouble;
        }
        if (AggType.INT == aggType) {
            return sumLong;
        }
        return 0D;
    }

    @Override
    void reset() {
        sumDouble = 0D;
        sumLong = 0L;
    }

    @Override
    void update(final FunnelContext context) {
        if (equation != null) {
            final Object unknownType = equation.evaluate();
            if (unknownType instanceof Double) {
                aggType = AggType.FLOAT;
                final double currentValue = ((Double) unknownType);
                sumDouble += currentValue;
                return;
            }
            if (unknownType instanceof Long) {
                aggType = AggType.INT;
                final long currentValue = ((Long) unknownType);
                sumLong += currentValue;
                return;
            }
            return;
        }
        if (columnName != null) {
            final KeyPart col = context.getColumnHelper().get(columnName);
            if (col.isFloat()) {
                aggType = AggType.FLOAT;
                final double currentValue = col.getContentsAsDouble();
                sumDouble += currentValue;
                return;
            }
            if (col.isInteger()) {
                aggType = AggType.INT;
                final long currentValue = (Long) col.getContents();
                sumLong += currentValue;
            }
        }
    }
}
