package com.codetaco.sort.aggregation;

import com.codetaco.sort.parameters.FunnelContext;

public class AggregateCount extends Aggregate {

    private long counter;

    @Override
    Object getValueForEquations() {
        return counter;
    }

    @Override
    void reset() {
        counter = 0;
    }

    @Override
    public boolean supportsDate() {
        return false;
    }

    @Override
    public boolean supportsNumber() {
        return false;
    }

    @Override
    void update(final FunnelContext context) {
        counter++;
    }

}
