package com.codetaco.sort.aggregation;

import com.codetaco.date.CalendarFactory;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;

import java.time.ZonedDateTime;
import java.util.Calendar;

class AggregateMin extends Aggregate {
    private double minDouble;
    private long minLong;
    private Calendar minCalendar;

    AggregateMin() {
        reset();
    }

    @Override
    Object getValueForEquations() {
        if (minDouble != Double.MAX_VALUE) {
            return minDouble;
        }
        if (minLong != Long.MAX_VALUE) {
            return minLong;
        }
        if (minCalendar != null) {
            return minCalendar;
        }
        return 0D;
    }

    @Override
    void reset() {
        minDouble = Double.MAX_VALUE;
        minLong = Long.MAX_VALUE;
        minCalendar = null;
    }

    @Override
    void update(final FunnelContext context) {
        if (equation != null) {
            final Object unknownType = equation.evaluate();
            if (unknownType instanceof Double) {
                final double currentValue = ((Double) unknownType);
                if (currentValue < minDouble) {
                    minDouble = currentValue;
                }
                return;
            }
            if (unknownType instanceof Long) {
                final long currentValue = ((Long) unknownType);
                if (currentValue < minLong) {
                    minLong = currentValue;
                }
                return;
            }
            if (unknownType instanceof ZonedDateTime) {
                final Calendar currentValue = CalendarFactory.asCalendar((ZonedDateTime) unknownType);
                if (minCalendar == null || currentValue.before(minCalendar)) {
                    minCalendar = currentValue;
                }
                return;
            }
            minDouble = 0;
            return;
        }
        if (columnName != null) {
            final KeyPart col = context.getColumnHelper().get(columnName);
            if (col.isDate()) {
                final Calendar currentValue = (Calendar) col.getContents();
                if (minCalendar == null || currentValue.before(minCalendar)) {
                    minCalendar = currentValue;
                }
                return;
            }
            if (col.isFloat()) {
                final double currentValue = col.getContentsAsDouble();
                if (currentValue < minDouble) {
                    minDouble = currentValue;
                }
                return;
            }
            if (col.isInteger()) {
                final long currentValue = (Long) col.getContents();
                if (currentValue < minLong) {
                    minLong = currentValue;
                }
                return;
            }
        }
        minDouble = 0;
    }
}
