package com.codetaco.sort.aggregation;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.math.Equ;
import com.codetaco.sort.Sort;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;

abstract public class Aggregate {

    /*
    These factory methods are actually used by CLI.
     */
    static public Aggregate newAvg() {
        return new AggregateAvg();
    }

    static public Aggregate newCount() {
        return new AggregateCount();
    }

    static public Aggregate newMax() {
        return new AggregateMax();
    }

    static public Aggregate newMin() {
        return new AggregateMin();
    }

    static public Aggregate newSum() {
        return new AggregateSum();
    }

    public static void aggregate(final FunnelContext context, final int originalRecordSize,
                                 final long originalRecordNumber) throws Exception {
        if (context.isAggregating()) {
            loadColumnsIntoAggregateEquations(context, originalRecordSize, originalRecordNumber);
            for (final Aggregate agg : context.getAggregates()) {
                agg.update(context);
            }
        }
    }

    private static void loadColumnsIntoAggregateEquations(final FunnelContext context,
                                                          final int originalRecordSize,
                                                          final long originalRecordNumber) throws Exception {
        for (final Aggregate agg : context.getAggregates()) {
            if (agg.equation != null) {
                for (final KeyPart col : context.getColumnHelper().getColumns()) {
                    agg.equation.variable(col.getColumnName(), col.getContents());
                }

                agg.equation.variable(Sort.SYS_RECORDNUMBER, originalRecordNumber);
                agg.equation.variable(Sort.SYS_RECORDSIZE, originalRecordSize);
            }
        }
    }

    public static void loadEquations(final FunnelContext context, final Equ[] referencesToAllOutputFormatEquations)
      throws Exception {
        if (context.isAggregating()) {
            for (final Aggregate agg : context.getAggregates()) {
                for (final Equ equ : referencesToAllOutputFormatEquations) {
                    equ.variable(agg.name, agg.getValueForEquations());
                }
            }
        }
    }

    public static void reset(final FunnelContext context) {
        if (context.isAggregating()) {
            for (final Aggregate agg : context.getAggregates()) {
                agg.reset();
            }
        }
    }

    @Arg(positional = true, help = "A previously defined column name.")
    public String columnName;

    @Arg(shortName = 'n', required = true, help = "A name for this aggregate so that it can be referenced.")
    public String name;

    @Arg(shortName = 'e', allowMetaphone = true, help = "Used instead of a column name.")
    public Equ equation;

    AggType aggType;

    abstract Object getValueForEquations();

    abstract void reset();

    public boolean supportsDate() {
        return true;
    }

    public boolean supportsNumber() {
        return true;
    }

    abstract void update(FunnelContext context) throws Exception;
}
