package com.codetaco.sort.columns;

import com.codetaco.math.Equ;
import com.codetaco.sort.aggregation.Aggregate;
import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@Setter
public class FormatOutHelper {
    private static final int MAX_OUTPUT_SIZE = 4096;

    public static int lengthToWrite(final byte[] data,
                                    final int offset,
                                    final int dataLength,
                                    final boolean rightTrim) {
        int lengthToWrite = 0;
        if (!rightTrim) {
            lengthToWrite = dataLength;
        } else {
            for (int i = offset + dataLength - 1; i >= offset; i--) {
                if (data[i] != ' ') {
                    lengthToWrite = (i - offset) + 1;
                    break;
                }
            }
        }
        return lengthToWrite;
    }

    private final KeyContext context;
    private final int maxRecordBytes;
    private final ColumnHelper columnHelper;
    private final HeaderHelper headerHelper;
    private FormatPart firstOutputFormatColumn;

    private List<FormatPart> columns;

    private Equ[] referencesToAllOutputFormatEquations;

    public FormatOutHelper(final ColumnHelper columnHelper,
                           final HeaderHelper headerHelper) {
        this(columnHelper, headerHelper, MAX_OUTPUT_SIZE);
    }

    private FormatOutHelper(final ColumnHelper columnHelper,
                            final HeaderHelper headerHelper,
                            final int maxsize) {
        log.debug("maximum output record length is {}", MAX_OUTPUT_SIZE);

        maxRecordBytes = maxsize;
        context = new KeyContext();
        columns = new ArrayList<>();
        this.columnHelper = columnHelper;
        this.headerHelper = headerHelper;
    }

    /**
     * Add the field in sequence after all other fields that have already been defined. This is done through a linked
     * list of fields. Use the column helper to find the definition of the key if a column name was specified.
     *
     * @param formatter the formatter
     */
    public void add(final FormatPart formatter) {
        if (columnHelper != null && columnHelper.exists(formatter.getColumnName())) {
            final KeyPart colDef = columnHelper.get(formatter.getColumnName());
            formatter.defineFrom(colDef);
        }
        if (headerHelper != null && headerHelper.exists(formatter.getColumnName())) {
            final KeyPart colDef = headerHelper.get(formatter.getColumnName());
            formatter.defineFrom(colDef);
        }

        if (this.firstOutputFormatColumn == null) {
            this.firstOutputFormatColumn = formatter;
        } else {
            this.firstOutputFormatColumn.add(formatter);
        }
    }

    /**
     * It is likely that the provided data is a reusable buffer of bytes. So we
     * can't just store these bytes for later use.
     */
    void formatRow(FunnelContext funnelContext,
                   byte[] data,
                   SourceProxyRecord proxyRecord,
                   ColumnConsumer columnConsumer) throws Exception {
        /*
         * The extra byte is for a 0x00 character to be placed at the end of
         * String keys. This is important in order to handle keys where the user
         * specified the maximum length for a String key. Or took the default
         * sort, which is the maximum key.
         */
        context.setKey(new byte[maxRecordBytes + 1]);
        context.setKeyLength(0);
        context.setRawRecordBytes(new byte[1][]);
        context.getRawRecordBytes()[0] = data;
        context.setRecordNumber(proxyRecord.getOriginalRecordNumber());

        /*
         * Use the output column definitions to format here. The real issue is
         * that the input computations and system variables are not available at
         * this point in the process. The rows from the original source were
         * read and a tag sort was performed. Now that the rows are ready for
         * writing it is necessary to recompute those fields.
         */
        prepareEquationsWithOriginalColumnData(funnelContext);

        final ByteArrayOutputStream formattedRow = new ByteArrayOutputStream(maxRecordBytes);
        final ByteArrayOutputStream formattedPart = new ByteArrayOutputStream(maxRecordBytes);
        FormatPart partToFormat = firstOutputFormatColumn;
        while (partToFormat != null) {
            partToFormat.formatInto(funnelContext, proxyRecord.getOriginalSize(), formattedPart);
            formattedPart.writeTo(formattedRow);
            columnConsumer.accept(partToFormat, formattedPart);
            formattedPart.reset();
            partToFormat = partToFormat.getNextPart();

        }

        context.setKey(formattedRow.toByteArray());
        context.setKeyLength(context.getKey().length);
        context.setRawRecordBytes(null);
    }

    public void format(byte[] originalData,
                       int dataSize,
                       SourceProxyRecord proxyRecord,
                       boolean rightTrim,
                       ColumnConsumer columnConsumer,
                       RowConsumer rowConsumer) throws Exception {
        if (firstOutputFormatColumn == null) {
            final int lengthToWrite = lengthToWrite(originalData, 0, dataSize, rightTrim);
            context.setKeyLength(lengthToWrite);
            ByteArrayOutputStream formattedRow = new ByteArrayOutputStream(lengthToWrite);
            formattedRow.write(originalData, 0, lengthToWrite);
            rowConsumer.accept(formattedRow);
            return;
        }
        formatRow(proxyRecord.getFunnelContext(),
                  originalData,
                  proxyRecord,
                  columnConsumer);

        final int lengthToWrite = lengthToWrite(context.getKey(),
                                                0,
                                                context.getKeyLength(),
                                                rightTrim);
        context.setKeyLength(lengthToWrite);
        ByteArrayOutputStream formattedRow = new ByteArrayOutputStream(lengthToWrite);
        formattedRow.write(context.getKey(), 0, lengthToWrite);
        rowConsumer.accept(formattedRow);
    }

    Equ[] getReferencesToAllOutputFormatEquations(final FunnelContext funnelContext) {
        if (referencesToAllOutputFormatEquations == null) {
            /*
             * First count all of the equations so we can make an array.
             */
            int equationCount = 0;
            if (funnelContext.isAggregating()) {
                /*
                 * Also count all of the aggregate equations because they are
                 * considered to be output functions.
                 */
                for (final Aggregate agg : funnelContext.getAggregates()) {
                    if (agg.equation != null) {
                        equationCount++;
                    }
                }
            }
            if (funnelContext.getFormatOutDefs() != null) {
                for (final FormatPart def : funnelContext.getFormatOutDefs()) {
                    if (def.getEquation() != null) {
                        equationCount++;
                    }
                }
            }
            if (funnelContext.getHeaderOutDefs() != null) {
                for (final FormatPart def : funnelContext.getHeaderOutDefs()) {
                    if (def.getEquation() != null) {
                        equationCount++;
                    }
                }
            }
            referencesToAllOutputFormatEquations = new Equ[equationCount];

            equationCount = 0;
            if (funnelContext.isAggregating()) {
                /*
                 * Also include all of the aggregate equations because they are
                 * considered to be output functions.
                 */
                for (final Aggregate agg : funnelContext.getAggregates()) {
                    if (agg.equation != null) {
                        referencesToAllOutputFormatEquations[equationCount++] = agg.equation;
                    }
                }
            }
            if (funnelContext.getFormatOutDefs() != null) {
                for (final FormatPart def : funnelContext.getFormatOutDefs()) {
                    if (def.getEquation() != null) {
                        referencesToAllOutputFormatEquations[equationCount++] = def.getEquation();
                    }
                }
            }
            if (funnelContext.getHeaderOutDefs() != null) {
                for (final FormatPart def : funnelContext.getHeaderOutDefs()) {
                    if (def.getEquation() != null) {
                        referencesToAllOutputFormatEquations[equationCount++] = def.getEquation();
                    }
                }
            }
        }
        return referencesToAllOutputFormatEquations;
    }

    private void prepareEquationsWithOriginalColumnData(final FunnelContext funnelContext) throws Exception {
        getReferencesToAllOutputFormatEquations(funnelContext);
        /*
         * Just to get the variables in the equation loaded from the original
         * record.
         */
        funnelContext.getColumnHelper().loadEquations(context.getRecordNumber(),
                                                      context.getRawRecordBytes()[0].length,
                                                      referencesToAllOutputFormatEquations);
        /*
         * In order to get the aggregate values into the format equations they
         * will also be needlessly loaded back into the aggregate equations too.
         */
        Aggregate.loadEquations(funnelContext, referencesToAllOutputFormatEquations);
    }

}
