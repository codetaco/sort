package com.codetaco.sort.columns;

import com.codetaco.math.Equ;
import com.codetaco.sort.Sort;
import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Slf4j
@Getter
public class ColumnHelper {

    private static final int MAX_COLUMN_SIZE = 255;
    private final KeyContext context;
    private final int maxKeyBytes;
    private List<KeyPart> columns;
    private FieldHelper fieldHelper;

    public ColumnHelper(FieldHelper fieldHelper) {
        this(MAX_COLUMN_SIZE);
        this.fieldHelper = fieldHelper;
    }

    private ColumnHelper(final int maxsize) {
        log.debug("maximum column length is {}", MAX_COLUMN_SIZE);

        maxKeyBytes = maxsize;
        context = new KeyContext();
        columns = new ArrayList<>();
    }

    public void add(final KeyPart formatter) throws ParseException {
        if (exists(formatter.getColumnName())) {
            throw new ParseException("Column already defined: " + formatter.getColumnName(), 0);
        }
        final KeyPart myCopy = formatter.newCopy();
        columns.add(myCopy);
    }

    public boolean exists(final String name) {
        for (final KeyPart col : columns) {
            if (name == null) {
                if (col.getColumnName() == null) {
                    return true;
                }
                continue;
            } else if (col.getColumnName() == null) {
                continue;
            }

            if (col.getColumnName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public KeyContext extract(final FunnelContext funnelContext,
                              final byte[] data,
                              final long recordNumber,
                              final int dataLength,
                              final Equ... equations) throws Exception {
        /*
         * It is likely that the provided data is a reusable buffer of bytes. So we
         * can't just store these bytes for later use.
         *
         * The extra byte is for a 0x00 character to be placed at the end of
         * String keys. This is important in order to handle keys where the user
         * specified the maximum length for a String key. Or took the default
         * sort, which is the maximum key.
         */
        context.setKey(new byte[maxKeyBytes + 1]);
        context.setKeyLength(0);
        context.setRawRecordBytes(new byte[1][]);
        context.getRawRecordBytes()[0] = data;
        context.setRecordNumber(recordNumber);

        extractColumnContentsFromRawData(funnelContext, recordNumber);
        evaluateFields();
        loadEquations(recordNumber, dataLength, equations);

        context.setRawRecordBytes(null);
        return context;
    }

    public void extract(final FunnelContext funnelContext,
                        final byte[][] data,
                        final long recordNumber,
                        final int dataLength,
                        final Equ... equations) {
        /*
         * Call this method for csvIn files that break each row up into fields (byte
         * arrays). [][].
         *
         * The extra byte is for a 0x00 character to be placed at the end of
         * String keys. This is important in order to handle keys where the user
         * specified the maximum length for a String key. Or took the default
         * sort, which is the maximum key.
         */
        context.setKey(new byte[maxKeyBytes + 1]);
        context.setKeyLength(0);
        context.setRawRecordBytes(data);
        context.setRecordNumber(recordNumber);

        extractColumnContentsFromRawData(funnelContext, recordNumber);
        loadEquations(recordNumber, dataLength, equations);
        evaluateFields();

        context.setRawRecordBytes(null);
    }

    public void evaluateFields() {
        fieldHelper.evaluteAllFields(this);
    }

    void loadEquations(Equ... equations) {
        loadEquations(context.getRecordNumber(), equations);
    }

    private void loadEquations(long recordNumber, Equ... equations) {
        columns.forEach(
          col ->
            Arrays.stream(equations)
              .filter(Objects::nonNull)
              .forEach(equ -> {
                  try {
                      equ.variable(col.getColumnName(), col.getContents());
                  } catch (final Exception e1) {
                      log.warn("\"{}\" {} {} on record number {}",
                               col.getColumnName(),
                               e1.getClass().getSimpleName(),
                               e1.getMessage(),
                               (recordNumber),
                               e1);
                  }
              }));
    }

    void loadEquations(long recordNumber,
                       int dataLength,
                       Equ... equations) {
        loadEquations(recordNumber, equations);
        final Long rn = recordNumber;
        final Long rs = (long) dataLength;
        Arrays.stream(equations)
          .filter(Objects::nonNull)
          .forEach(equ -> {
              try {
                  equ.variable(Sort.SYS_RECORDNUMBER, rn);
                  equ.variable(Sort.SYS_RECORDSIZE, rs);
              } catch (Exception e1) {
                  log.warn("{} {} on record number {}",
                           e1.getClass().getSimpleName(),
                           e1.getMessage(),
                           (recordNumber),
                           e1);
              }
          });
    }

    private void extractColumnContentsFromRawData(final FunnelContext funnelContext,
                                                  final long recordNumber) {
        if (funnelContext == null) {
            return;
        }
        columns.forEach(col -> {
            try {
                col.parseObject(context);
            } catch (final Exception e) {
                log.warn("\"{}\" {} {} on record number {}",
                         col.getColumnName(),
                         e.getClass().getSimpleName(),
                         e.getMessage(),
                         (recordNumber),
                         e);
            }
        });
    }

    public KeyPart get(final String name) {
        for (final KeyPart col : columns) {
            if (name == null) {
                if (col.getColumnName() == null) {
                    return col;
                }
                continue;
            } else if (col.getColumnName() == null) {
                continue;
            }

            if (col.getColumnName().equalsIgnoreCase(name)) {
                return col;
            }
        }
        return null;
    }

    public List<KeyPart> getColumns() {
        return columns;
    }

    public List<String> getNames() {
        final List<String> allNames = new ArrayList<>();
        for (final KeyPart col : columns) {
            allNames.add(col.getColumnName());
        }
//        allNames.addAll(fieldHelper.getNames());
        return allNames;
    }

    public void loadColumnsFromBytes(final byte[] data,
                                     final long recordNumber) {

        byte[][] expandedData = new byte[1][];
        expandedData[0] = data;
        loadColumnsFromBytes(expandedData, recordNumber);
    }

    public void loadColumnsFromBytes(final byte[][] data,
                                     final long recordNumber) {
        context.setKey(null);
        context.setKeyLength(0);
        context.setRawRecordBytes(data);
        context.setRecordNumber(recordNumber);

        for (final KeyPart col : columns) {
            try {
                col.parseObject(context);

            } catch (final Exception e) {
                log.warn("\"{}\" {} {} on record number {}",
                         col.getColumnName(),
                         e.getClass().getSimpleName(),
                         e.getMessage(),
                         recordNumber);
            }
        }
        evaluateFields();
    }
}
