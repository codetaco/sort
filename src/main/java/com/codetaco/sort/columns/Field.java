package com.codetaco.sort.columns;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.math.Equ;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Field {

    @Arg(shortName = 'n',
      required = true,
      caseSensitive = true,
      help = "A new field name that can be used as any column can be used.")
    private String columnName;

    @Arg(shortName = 'e',
      caseSensitive = true,
      required = true,
      allowMetaphone = true,
      help = "An equation that references other fields to create a new field")
    private Equ<String> equation;

    public Field() {
        super();
    }
}
