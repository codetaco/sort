package com.codetaco.sort.columns;

import com.codetaco.math.Equ;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.orderby.KeyType;
import com.codetaco.sort.orderby.VirtualField;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class FieldHelper {

    private final static Map<String, KeyPart> EmptyCurrentValues = new HashMap<>();

    private List<Field> inputFormatters = new ArrayList<>();
    private Equ<String>[] referencesToAllMyEquations;

    public void add(Field field, ColumnHelper columnHelper) {
        inputFormatters.add(field);

        VirtualField virtualColumn = (VirtualField)KeyType.Virtual.create();
        virtualColumn.setTypeName(KeyType.Virtual);
        virtualColumn.setColumnName(field.getColumnName());
        virtualColumn.setOffset(0);
        virtualColumn.setLength(0);
        virtualColumn.setColumnHelper(columnHelper);
        try {
            columnHelper.add(virtualColumn);
        } catch (Exception e) {
            log.warn("", e);
        }
    }

    void evaluteAllFields(ColumnHelper columnHelper) {
        if (inputFormatters == null) {
            return;
        }
        loadEquations(columnHelper);
        inputFormatters.forEach(field -> {

            VirtualField virtualColumn = (VirtualField)columnHelper.get(field.getColumnName());
            String value = field.getEquation().evaluate();
            virtualColumn.setOffset(0);
            virtualColumn.setLength(value.length());
            try {
                virtualColumn.setContents(value);
            } catch (Exception e) {
                log.warn("", e);
            }
        });
    }

    private void loadEquations(ColumnHelper columnHelper) {
        if (referencesToAllMyEquations == null) {
            referencesToAllMyEquations = inputFormatters.stream()
                                           .map(Field::getEquation)
                                           .collect(Collectors.toList())
                                           .toArray(new Equ[]{});
        }
        columnHelper.loadEquations(referencesToAllMyEquations);
    }
}
