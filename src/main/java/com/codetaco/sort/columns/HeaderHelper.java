package com.codetaco.sort.columns;

import com.codetaco.math.Equ;
import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;

@Getter
@Setter
public class HeaderHelper extends ColumnHelper {

    private boolean waitingForInput = true;
    private byte[] originalHeaderRow;

    public HeaderHelper(FieldHelper fieldHelper) {
        super(fieldHelper);
    }

    @Override
    public KeyContext extract(final FunnelContext funnelContext,
                              final byte[] data,
                              final long recordNumber,
                              final int dataLength,
                              final Equ... equations) throws Exception {

        originalHeaderRow = Arrays.copyOf(data, dataLength);
        /*
         * Add some more equations just for headerIn fields.
         */
        Equ[] cachedEquations;

        int ceSize = equations.length;
        ceSize += funnelContext.getFormatOutHelper().getReferencesToAllOutputFormatEquations(funnelContext).length;
        cachedEquations = new Equ[ceSize];

        int ce = 0;
        for (final Equ equ : equations) {
            cachedEquations[ce++] = equ;
        }
        for (final Equ equ : funnelContext.getFormatOutHelper().getReferencesToAllOutputFormatEquations(funnelContext)) {
            cachedEquations[ce++] = equ;
        }
        return super.extract(funnelContext, data, recordNumber, dataLength, cachedEquations);
    }

    public byte[] getContents(final KeyPart headerCol) throws Exception {
        headerCol.parseObjectFromRawData(originalHeaderRow);
        return headerCol.getContentsAsByteArray();
    }

    public boolean isWaitingForInput() {
        if (waitingForInput) {
            waitingForInput = false;
            // if (columns != null && columns.size() > 0)
            return true;
        }
        return false;
    }

    public void setWaitingForInput(final boolean shouldReadHeader) {
        waitingForInput = shouldReadHeader;
    }
}
