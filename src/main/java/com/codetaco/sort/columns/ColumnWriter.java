package com.codetaco.sort.columns;

import java.io.IOException;

public interface ColumnWriter {
    void write(byte[] sourceBytes, int off, int len) throws IOException;
}
