package com.codetaco.sort.columns;

import java.io.ByteArrayOutputStream;

@FunctionalInterface
public interface RowConsumer {
    void accept(ByteArrayOutputStream formattedRow);
}
