package com.codetaco.sort.columns;

import com.codetaco.cli.annotation.Arg;
import com.codetaco.math.Equ;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.orderby.KeyType;
import com.codetaco.sort.parameters.FunnelContext;
import lombok.Getter;
import lombok.Setter;

import java.io.ByteArrayOutputStream;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Formatter;

@Getter
@Setter
public class FormatPart {

    @Arg(positional = true, caseSensitive = true, help = "A previously defined column name.")
    private String columnName;

    @Arg(shortName = 't',
      longName = "type",
      positional = true,
      caseSensitive = true,
      help = "The data type to be written.  Defaults to the columnIn data type.")
    private KeyType typeName;

    @Arg(shortName = 'e',
      allowMetaphone = true,
      help = "Used instead of a column name, this will be evaluated with the result written to the output.")
    private Equ equation;

    @Arg(shortName = 'd',
      caseSensitive = true,
      help = "The format for converting the contents of the data to be written. Use java.util.Formatter rules for making the format.  The format must match the type of the data.")
    private String format;

    @Arg(caseSensitive = true,
      longName = "formatISO",
      help = "The format for converting the contents of the data to be written. Use Java Formatter rules for making the format.  The format must match the type of the data.")
    private IsoFormatsForDates formatIso;

    @Arg(help = "Change the zone of this date / time.",
      shortName = 'z',
      caseSensitive = true)
    private String zoneId;

    @Arg(shortName = 'o',
      defaultValues = {"-1"},
      range = {"0"},
      help = "The zero relative offset from the beginning of a row.  This will be computed, if not specified, to be the location of the previous column plus the length of the previous column.  Most often this parameter is not needed.")
    private int offset;

    @Arg(shortName = 'l', defaultValues = {"255"}, range = {"1", "255"}, help = "The length of the key in bytes.")
    private int length;

    @Arg(shortName = 's',
      defaultValues = {"255"},
      range = {"1", "255"},
      help = "The number of characters this field will use on output.")
    private int size;

    @Arg(shortName = 'f', defaultValues = {" "}, help = "The trailing filler character to use for a short field.")
    private byte filler;

    private FormatPart nextPart;
    private KeyPart column;

    public FormatPart() {
        super();
        columnName = null;
        filler = ' ';
    }

    public void add(final FormatPart anotherFormatter) {
        if (nextPart == null) {
            nextPart = anotherFormatter;
        } else {
            nextPart.add(anotherFormatter);
        }
    }

    void defineFrom(final KeyPart colDef) {
        column = colDef;
    }

    void formatInto(final FunnelContext funnelContext,
                    final int originalSize,
                    final ByteArrayOutputStream outputBytes) throws Exception {

        if (column != null) {
            column.formatForOutput(outputBytes, originalSize, this);

        } else if (equation != null) {
            formatEquResults(outputBytes);

        } else if (funnelContext.getHeaderHelper().exists(columnName)) {
            formatHeaderColumn(funnelContext, outputBytes);

        } else {
            formatFiller(outputBytes);
        }
    }

    private void formatFiller(ByteArrayOutputStream outputBytes) {
        writeToOutput(outputBytes, null, 0, 0);
    }

    private void formatHeaderColumn(FunnelContext funnelContext, ByteArrayOutputStream outputBytes) throws Exception {
        final KeyPart headerCol = funnelContext.getHeaderHelper().get(columnName);
        final byte[] result = funnelContext.getHeaderHelper().getContents(headerCol);
        writeToOutput(outputBytes,
                      result,
                      result.length,
                      result.length);
    }

    private void formatEquResults(ByteArrayOutputStream outputBytes) throws Exception {
        Object result = null;
        try {
            result = equation.evaluate();
        } catch (Exception e) {
            throw new Exception(e.getMessage() + ": --format(" + equation.toString() + ")");
        }

        if (typeName == null || KeyType.String == typeName) {
            if (result instanceof String) {
                if (isDefaultFormattingOK()) {
                    final String sResult = (String) result;
                    writeToOutput(outputBytes,
                                  sResult.getBytes(),
                                  sResult.length(),
                                  sResult.length());
                } else {
                    final String sResult = format(result);
                    writeToOutput(outputBytes,
                                  sResult.getBytes(),
                                  sResult.length(),
                                  sResult.length());
                }
            } else {
                if (result instanceof ZonedDateTime) {
                    final String sResult = format((ZonedDateTime) result,
                                                  IsoFormatsForDates.ISO_ZONED_DATE_TIME);
                    writeToOutput(outputBytes,
                                  sResult.getBytes(),
                                  sResult.length(),
                                  sResult.length());
                } else {
                    if (isDefaultFormattingOK()) {
                        final String sResult = result.toString();
                        writeToOutput(outputBytes,
                                      sResult.getBytes(),
                                      sResult.length(),
                                      sResult.length());
                    } else {
                        final String sResult = format(result);
                        writeToOutput(outputBytes,
                                      sResult.getBytes(),
                                      sResult.length(),
                                      sResult.length());
                    }
                }
            }
        }
    }

    public boolean isDefaultFormattingOK() {
        return format == null && formatIso == null && zoneId == null;
    }

    public String format(Object unformatted) {
        if (format == null) {
            return unformatted.toString();
        }
        try (Formatter formatter = new Formatter()) {
            return formatter.format(format, unformatted).out().toString();
        }
    }

    public String format(ZonedDateTime zonedDateTime, IsoFormatsForDates defaultFormatter) {

        ZonedDateTime rezoned = zonedDateTime;
        if (zoneId != null) {
            rezoned = zonedDateTime.withZoneSameInstant(ZoneId.of(zoneId));
        }

        if (formatIso == null || formatIso == IsoFormatsForDates.DEFAULT) {
            if (format != null) {
                return format(rezoned);
            }
            return defaultFormatter.formatter().format(rezoned);
        }
        return formatIso.formatter().format(rezoned);
    }

    public void writeToOutput(final ByteArrayOutputStream outputBytes,
                              final byte[] rawBytes,
                              final int columnLength,
                              final int originalSize) {

        int dataLength = columnLength;
        if (originalSize < offset + dataLength) {
            dataLength = originalSize - offset;
        }
        if (length < dataLength) {
            dataLength = length;
        }
        if (rawBytes != null) {
            outputBytes.write(rawBytes, offset, dataLength);
        }

        int lengthWithFiller = columnLength;
        if (length != 255) // 255 means not specified
        {
            lengthWithFiller = length;
        }
        if (size != 255) // 255 means not specified
        {
            lengthWithFiller = size;
        }
        if (lengthWithFiller == 255) {
            lengthWithFiller = dataLength;
        }
        if (lengthWithFiller > dataLength) {
            for (int x = 0; x < lengthWithFiller - dataLength; x++) {
                outputBytes.write(filler);
            }
        }
    }
}
