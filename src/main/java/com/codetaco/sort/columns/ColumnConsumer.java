package com.codetaco.sort.columns;

import java.io.ByteArrayOutputStream;

@FunctionalInterface
public interface ColumnConsumer {
    void accept(FormatPart formatPart, ByteArrayOutputStream formattedContents);
}
