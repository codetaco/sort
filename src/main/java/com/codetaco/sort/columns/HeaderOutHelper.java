package com.codetaco.sort.columns;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Getter
@Setter
@Slf4j
public class HeaderOutHelper extends FormatOutHelper {
    private boolean waitingToWrite = false;

    public HeaderOutHelper(final HeaderHelper headerHelper) {
        super(null, headerHelper);
    }

    public void format(final FunnelContext funnelContext,
                       final ColumnWriter writer,
                       ColumnConsumer columnConsumer,
                       RowConsumer rowConsumer) throws Exception {
        if (getFirstOutputFormatColumn() == null) {
            final int lengthToWrite = lengthToWrite(getHeaderHelper().getOriginalHeaderRow(),
                                                    0,
                                                    getHeaderHelper().getOriginalHeaderRow().length,
                                                    false);
            writer.write(getHeaderHelper().getOriginalHeaderRow(), 0, lengthToWrite);
            return;
        }
        try {
            final SourceProxyRecord dummyProxy = SourceProxyRecord.getInstance(funnelContext);
            dummyProxy.setOriginalRecordNumber(0);
            byte[] data = getHeaderHelper().getOriginalHeaderRow();
            if (data == null) {
                data = new byte[0];
            }
            dummyProxy.setOriginalSize(data.length);
            formatRow(funnelContext,
                      data,
                      dummyProxy,
                      columnConsumer);
        } catch (final Exception e) {
            throw new IOException(e.getMessage(), e);
        }

        final int lengthToWrite = lengthToWrite(getContext().getKey(),
                                                0,
                                                getContext().getKeyLength(),
                                                false);
        writer.write(getContext().getKey(), 0, lengthToWrite);
    }

    public boolean isWaitingToWrite() {
        if (waitingToWrite) {
            waitingToWrite = false;
            return true;
        }
        return false;
    }

    public void setWaitingToWrite(final boolean needsToBeWritten) {
        waitingToWrite = needsToBeWritten;
    }
}
