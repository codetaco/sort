package com.codetaco.sort;

import com.codetaco.math.MathException;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

public class App {

    static public void abort(final int code, final Exception ex) {
        ex.printStackTrace();
        System.exit(code);
    }

    public static void main(final String... args) throws Throwable {
        final AppContext cfg = new AppContext(workDir());

        LogManager.resetConfiguration();
        DOMConfigurator.configure(cfg.log4jConfigFileName);

        try {
            Sort.sort(cfg, args);
        } catch (final MathException e) {
            System.out.println(e.getMessage());
        }
        System.exit(0);
    }

    static private String workDir() {
        return System.getProperty("user.dir");
    }
}
