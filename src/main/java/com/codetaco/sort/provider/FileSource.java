package com.codetaco.sort.provider;

import com.codetaco.sort.parameters.FunnelContext;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;

@Slf4j
public class FileSource implements RandomAccessInputSource {

    private final FunnelContext context;
    private RandomAccessFile[] raf;

    public FileSource(final FunnelContext context) throws ParseException, IOException {
        this.context = context;
        raf = new RandomAccessFile[context.inputFileCount()];
    }

    @Override
    public void close() throws IOException, ParseException {
        for (int i = 0; i < context.inputFileCount(); i++) {
            raf[i].close();
            log.debug("releasing original input source {}", context.getInputFile(i).getAbsolutePath());
        }
    }

    @Override
    public void open() throws IOException, ParseException {
        for (int i = 0; i < context.inputFileCount(); i++) {
            raf[i] = new RandomAccessFile(context.getInputFile(i), "r");
            log.debug("rereading original input source {}", context.getInputFile(i).getAbsolutePath());
        }
    }

    @Override
    public int read(final int originalInputFileIndex,
                    final byte[] originalBytes,
                    final long originalLocation,
                    final int originalSize) throws IOException {

        raf[originalInputFileIndex].seek(originalLocation);
        int readSize = originalSize;
        if (originalBytes.length < originalSize) {
            readSize = originalBytes.length;
        }
        return raf[originalInputFileIndex].read(originalBytes, 0, readSize);
    }
}
