package com.codetaco.sort.provider;

import com.codetaco.math.Equ;
import com.codetaco.sort.FunnelDataProvider;
import com.codetaco.sort.FunnelItem;
import com.codetaco.sort.Sort;
import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.parameters.DuplicateDisposition;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.segment.SourceProxyRecord;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
@Getter
@Setter
public abstract class AbstractProvider implements FunnelDataProvider {

    final FunnelContext context;
    InputReader reader;
    private long thisFileRecordNumber;
    private long continuousRecordNumber;
    byte row[];
    int unselectedCount;
    private Equ[] cachedEquations;

    protected AbstractProvider(final FunnelContext _context) throws IOException, ParseException {
        context = _context;
        /*
         * This is really to handle test cases. It should never be null.
         */
        if (_context == null) {
            row = null;
            return;
        }
        initialize();
    }


    @Override
    public void attachTo(final FunnelItem item) {
        item.setProvider(this);
    }


    @Override
    public void close() throws IOException, ParseException {
        if (reader == null) {
            return;
        }
        reader.close();
        reader = null;
    }

    protected Equ[] getCachedEquations() {
        if (cachedEquations == null) {
            int ceSize = 0;
            if (context.getWhereEqu() != null) {
                ceSize += context.getWhereEqu().size();
            }
            if (context.getStopEqu() != null) {
                ceSize += context.getStopEqu().size();
            }
            cachedEquations = new Equ[ceSize];

            int ce = 0;
            if (context.getWhereEqu() != null) {
                for (final Equ equ : context.getWhereEqu()) {
                    cachedEquations[ce++] = equ;
                }
            }
            if (context.getStopEqu() != null) {
                for (final Equ equ : context.getStopEqu()) {
                    cachedEquations[ce++] = equ;
                }
            }
        }
        return cachedEquations;
    }

    protected abstract void initialize() throws IOException, ParseException;

    protected boolean isRowSelected(final int byteCount) {
        return true;
    }

    private void logStatistics(final int fileIndex) throws ParseException, IOException {
        final StringBuilder sb = new StringBuilder();

        sb.append(Sort.ByteFormatter.format(getThisFileRecordNumber()));
        sb.append(" rows obtained from ");
        if (context.isSysin()) {
            sb.append("SYSIN");
        } else if (context.isURLInput()) {
            sb.append(context.getInputURLs());
        } else {
            sb.append(context.getInputFile(fileIndex).getName());
        }
        if (unselectedCount > 0) {
            sb.append(", ");
            sb.append(Sort.ByteFormatter.format(unselectedCount));
            sb.append(" filtered out by where clause");
        }

        context.inputCounters(unselectedCount, getThisFileRecordNumber());

        log.debug(sb.toString());
    }

    @Override
    public boolean next(final FunnelItem item, final long phase) throws IOException, ParseException {
        /*
         * Only return 1 row per phase per item.
         */
        if (item.getPhase() == phase || reader == null) {
            item.setEndOfData(true);
            return false;
        }
        item.setPhase(phase);

        boolean earlyEnd = false;
        int byteCount;
        long startPosition;
        try {
            while (true) {
                startPosition = reader.position();
                byteCount = reader.read(row);

                if (byteCount == -1) {
                    /*
                     * See if there are more files to be read.
                     */
                    if (context.startNextInput()) {
                        logStatistics(context.inputFileIndex() - 1);
                        if (context.isInPlaceSort()) {
                            setContinuousRecordNumber(0);
                        }
                        setThisFileRecordNumber(0);
                        unselectedCount = 0;
                        reader.close();
                        reader.open(context.getInputFile(context.inputFileIndex()));
                        continue;
                    }
                    break;
                }
                if (context.getHeaderHelper().isWaitingForInput()) {
                    context.getHeaderHelper()
                      .extract(context, row, getContinuousRecordNumber(), byteCount, getCachedEquations());
                    continue;
                }
                /*
                 * Putting this incrementer here causes the record number to be
                 * 1 relative.
                 */
                setThisFileRecordNumber(getThisFileRecordNumber() + 1);
                setContinuousRecordNumber(getContinuousRecordNumber() + 1);

                if (!recordLengthOK(byteCount)) {
                    continue;
                }

                if (!isRowSelected(byteCount)) {
                    continue;
                }

                preSelectionExtract(byteCount);

                if (context.stopIsTrue()) {
                    earlyEnd = true;
                    /*
                     * Uncount the termination record.
                     */
                    setThisFileRecordNumber(getThisFileRecordNumber() - 1);
                    setContinuousRecordNumber(getContinuousRecordNumber() - 1);

                    log.debug("stopWhen triggered at row " + getContinuousRecordNumber());
                    break;
                }

                if (!context.whereIsTrue()) {
                    unselectedCount++;
                    continue;
                }
                break;
            }

        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            throw new IOException(e.getMessage(), e);
        }
        if (byteCount < 1 || earlyEnd) {
            item.setEndOfData(true);
            try {
                logStatistics(context.inputFileIndex());
                close();
            } catch (final IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        final KeyContext kContext = postReadKeyProcessing(byteCount);

        final SourceProxyRecord wrapped = SourceProxyRecord.getInstance(context);
        wrapped.setOriginalInputFileIndex(context.inputFileIndex());

        wrapped.setSize(kContext.getKeyLength());
        wrapped.setSortKey(kContext.getKey());
        wrapped.setOriginalSize(byteCount);
        wrapped.setOriginalLocation(startPosition);

        if (DuplicateDisposition.LastOnly == context.getDuplicateDisposition()
          || DuplicateDisposition.Reverse == context.getDuplicateDisposition()) {
            wrapped.setOriginalRecordNumber(-getContinuousRecordNumber());
        } else {
            wrapped.setOriginalRecordNumber(getContinuousRecordNumber());
        }

        item.setData(wrapped);
        return true;
    }

    protected KeyContext postReadKeyProcessing(final int byteCount) throws IOException {
        KeyContext kContext;
        try {
            kContext = context.getKeyHelper().extractKey(row, getContinuousRecordNumber());
        } catch (final Exception e) {
            throw new IOException(e);
        }
        return kContext;
    }

    @Override
    public void loadColumns(byte[] originalData,
                            long originalRecordNumber) throws IOException {
        context.getColumnHelper().loadColumnsFromBytes(originalData, originalRecordNumber);
    }

    protected void preSelectionExtract(final int byteCount) throws Exception {
        context.getColumnHelper().extract(context,
                                          row,
                                          getContinuousRecordNumber(),
                                          byteCount,
                                          getCachedEquations());
    }

    protected boolean recordLengthOK(final int byteCount) {
        return true;
    }

    @Override
    public void reset() throws IOException, ParseException {
        initialize();
    }

}
