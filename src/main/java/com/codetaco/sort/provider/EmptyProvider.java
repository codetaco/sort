package com.codetaco.sort.provider;

import com.codetaco.sort.FunnelDataProvider;
import com.codetaco.sort.FunnelItem;

import java.io.IOException;

public class EmptyProvider implements FunnelDataProvider {
    @Override
    public long actualNumberOfRows() {
        return 0;
    }

    @Override
    public void attachTo(final FunnelItem item) {
        // intentionally empty
    }

    @Override
    public void close() throws IOException {
        // intentionally empty
    }

    @Override
    public void loadColumns(byte[] originalData,
                            long originalRecordNumber) {
        // intentionally empty
    }

    @Override
    public long maximumNumberOfRows() {
        return Long.MAX_VALUE;
    }

    @Override
    public boolean next(final FunnelItem item, final long phase) {
        item.setData(null);
        item.setEndOfData(true);
        return false;
    }

    @Override
    public void reset() {
        // Intentionally empty
    }
}
