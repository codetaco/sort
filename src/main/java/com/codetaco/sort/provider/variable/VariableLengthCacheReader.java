package com.codetaco.sort.provider.variable;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.InputReader;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;

@Slf4j
@Getter
public class VariableLengthCacheReader implements InputReader {

    final FunnelContext context;

    protected VariableLengthCacheReader(final FunnelContext context) throws IOException, ParseException {
        this.context = context;
        log.debug("variable length cache reader activated");
        loadDataToCache();
    }

    @Override
    public void close() {
        // intentionally. Cached input is opened and closed when the instance is
        // made.
    }

    @Override
    public long length() {
        return context.getInputCache().length();
    }

    protected void loadDataToCache() throws IOException, ParseException {
        try (final FileInputStream inputStream = new FileInputStream(context.getInputFile(context.inputFileIndex()))) {
            context.setInputCache(new VariableLengthInputCache(context, inputStream));
            log.debug("loaded {}", context.getInputFile(context.inputFileIndex()).getAbsolutePath());
        }
    }

    @Override
    public void open(final File inputFile) throws IOException {
        throw new IOException("the cacheInput option is not allowed with multiple input files");
    }

    @Override
    public long position() {
        return context.getInputCache().position();
    }

    @Override
    public int read(final byte[] row) {
        if (context.getInputCache().eof()) {
            return -1;
        }
        /*
         * Clear the row before reading.
         */
        for (int b = 0; b < row.length; b++) {
            row[b] = 0x00;
        }

        int rowNextPointer = 0;
        int sepNextPointer = 0;
        while (!context.getInputCache().eof()) {
            final byte b = context.getInputCache().readNextByte();
            /*
             * Then see of this character is the next expected character in the
             * end of row sequence.
             */
            if (b == context.getEndOfRecordDelimiterIn()[sepNextPointer]) {
                if (sepNextPointer == context.getEndOfRecordDelimiterIn().length - 1) {
                    return rowNextPointer;
                }
                sepNextPointer++;
                continue;
            }
            /*
             * Something that started to look like the end of row turned out not
             * to be. So give it to the caller.
             */
            if (sepNextPointer > 0) {
                for (int sp = 0; sp < sepNextPointer; sp++) {
                    row[rowNextPointer++] = context.getEndOfRecordDelimiterIn()[sp];
                }
                sepNextPointer = 0;
            }
            /*
             * transfer the byte to the row.
             */
            row[rowNextPointer++] = b;
        }
        if (context.getInputCache().eof())
            /*
             * end of file without end of record, this is ok.
             */ {
            if (rowNextPointer == 0) {
                return -1;
            }
        }
        /*
         * We have hit the end of the file and did not find an end of line for
         * the last bytes we did find. Return the row for what is there.
         */
        log.warn("assuming a line terminator at end of file where {} unterminated bytes were found", rowNextPointer);
        return rowNextPointer;
    }
}
