package com.codetaco.sort.provider.variable.json;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthSysinReader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
class JsonSysinReader extends VariableLengthSysinReader {

    JsonSysinReader(final FunnelContext context) throws IOException, ParseException {
        super(context);
        log.debug("JSON sysin reader activated");
    }

    @Override
    protected void loadDataToCache() throws IOException {
        getContext().setInputCache(new JsonInputCache(getContext(), System.in));
        try {
            System.in.close();
            log.debug("loaded SYSIN");
        } catch (Exception e) {
            log.debug("load SYSIN failed");
        }
    }
}
