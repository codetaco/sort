package com.codetaco.sort.provider.variable.csv;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthSysinReader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
class CsvSysinReader extends VariableLengthSysinReader {

    CsvSysinReader(final FunnelContext _context) throws IOException, ParseException {
        super(_context);
        log.debug("csvIn sysin reader activated");
    }
}
