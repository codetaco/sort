package com.codetaco.sort.provider.variable;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.AbstractProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class VariableLengthProvider extends AbstractProvider {
    public static final int MAX_VARIABLE_LENGTH_RECORD_SIZE = 1 << 16;

    public VariableLengthProvider(final FunnelContext _context) throws IOException, ParseException {
        super(_context);
    }

    @Override
    public long actualNumberOfRows() {
        return getContinuousRecordNumber();
    }

    protected void assignReaderInstance() throws IOException, ParseException {
        if (getContext().isURLInput()) {
            setReader(new VariableLengthURLReader(getContext()));
        } else if (getContext().isSysin()) {
            setReader(new VariableLengthSysinReader(getContext()));
        } else if (getContext().isCacheInput()) {
            setReader(new VariableLengthCacheReader(getContext()));
        } else {
            setReader(new VariableLengthFileReader(getContext()));
        }
    }

    @Override
    protected void initialize() throws IOException, ParseException {
        setRow(new byte[MAX_VARIABLE_LENGTH_RECORD_SIZE]);

        log.debug("{} byte array size for rows.  This is an arbitrary upper limit.", getRow().length);

        int optimalFunnelDepth = 2;
        long pow2 = getContext().getMaximumNumberOfRows();
        while (pow2 >= 2) {
            pow2 /= 2;
            optimalFunnelDepth++;
        }
        if (getContext().getDepth() > optimalFunnelDepth) {
            log.debug("overriding power from {} to {}", getContext().getDepth(), optimalFunnelDepth);

            getContext().setDepth(optimalFunnelDepth);
        }

        setUnselectedCount(0);
        setThisFileRecordNumber(0);

        assignReaderInstance();
    }

    @Override
    public long maximumNumberOfRows() {
        return getContext().getMaximumNumberOfRows();
    }

}
