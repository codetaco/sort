package com.codetaco.sort.provider.variable.json;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.AbstractInputCache;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

@Slf4j
class JsonInputCache extends AbstractInputCache {

    JsonInputCache(final FunnelContext context,
                   final InputStream source) throws IOException {
        super(context, source);
    }

    @Override
    protected void loadUntilSourceIsDepleted() throws IOException {
        try {
            if (getSource() == null || getSource().available() <= 0) {
                log.warn("input source is not available");
                return;
            }
        } catch (final IOException e) {
            log.warn("input source is not available, {}", e.getMessage());
            return;
        }

//        Scanner scanner = new Scanner(source);
//        String jsonString = scanner.nextLine().trim();
//        System.out.println(jsonString);

        Object readObject = null;
        try {
            ReadContext ctx = JsonPath.parse(getSource(), getContext().getJsonDefIn().getJsonConfiguration());
            readObject = ctx.read(getContext().getJsonDefIn().getPath());
        } catch (Exception e) {
            log.warn("while reading json: {}", e.getMessage());
            return;
        }

        if (!(readObject instanceof ArrayNode)) {
            throw new IOException("jsonIn(--path) must be an array");
        }

        ArrayNode rows = (ArrayNode) readObject;
        rows.forEach(row -> {
            byte[] jsonRow = row.toString().getBytes();
            ByteBuffer byteBuffer = ByteBuffer.allocate(jsonRow.length + getContext().getEndOfRecordDelimiterIn().length);
            byteBuffer.put(jsonRow);
            byteBuffer.put(getContext().getEndOfRecordDelimiterIn());
            byteBuffer.flip();
            getSourceBuffers().add(byteBuffer);
        });
    }

    @Override
    protected void postOpenVerification() {
        // nothing to do here
    }
}
