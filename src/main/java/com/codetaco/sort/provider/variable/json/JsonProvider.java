package com.codetaco.sort.provider.variable.json;

import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthProvider;
import com.fasterxml.jackson.databind.JsonNode;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class JsonProvider extends VariableLengthProvider {

    public JsonProvider(final FunnelContext context) throws IOException, ParseException {
        super(context);
        log.debug("JSON file provider activated");
    }

    @Override
    protected void assignReaderInstance() throws IOException, ParseException {
        if (getContext().isSysin()) {
            setReader(new JsonSysinReader(getContext()));
        } else if (getContext().isURLInput()) {
            setReader(new JsonURLReader(getContext()));
        } else if (getContext().isCacheInput()) {
            setReader(new JsonCacheReader(getContext()));
        } else {
            setReader(new JsonFileReader(getContext()));
        }
    }

    private byte[][] decodeJson(final byte[] input) {

        String jsonFormattedRow = new String(input);
        final byte[][] field = new byte[getContext().getInputColumnDefs().size()][];

        for (int colIndex = 0; colIndex < getContext().getInputColumnDefs().size(); ++colIndex) {
            KeyPart keyPart = getContext().getInputColumnDefs().get(colIndex);

            ReadContext ctx = JsonPath.parse(jsonFormattedRow, getContext().getJsonDefIn().getJsonConfiguration());
            JsonNode readObject = ctx.read(keyPart.getJsonFieldPath());
            if (readObject == null) {
                field[colIndex] = null;
            } else {
                field[colIndex] = readObject.asText().getBytes();
            }
        }
        return field;
    }

    @Override
    protected void preSelectionExtract(final int byteCount) throws Exception {
        final byte[][] data = decodeJson(getRow());
        getContext().getColumnHelper().extract(getContext(),
                                               data,
                                               getContinuousRecordNumber(),
                                               byteCount,
                                               getCachedEquations());
    }

    @Override
    protected KeyContext postReadKeyProcessing(final int byteCount) throws IOException {
        KeyContext kContext;
        try {
            final byte[][] data = decodeJson(getRow());
            kContext = getContext().getKeyHelper().extractKey(data, getContinuousRecordNumber());

        } catch (final Exception e) {
            throw new IOException(e);
        }
        return kContext;
    }

    @Override
    public void loadColumns(byte[] originalData,
                            long originalRecordNumber) {
        final byte[][] data = decodeJson(originalData);
        getContext().getColumnHelper().loadColumnsFromBytes(data, originalRecordNumber);
    }

}
