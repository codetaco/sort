package com.codetaco.sort.provider.variable;

import com.codetaco.sort.parameters.FunnelContext;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class VariableLengthSysinReader extends VariableLengthCacheReader {

    protected VariableLengthSysinReader(final FunnelContext context) throws IOException, ParseException {
        super(context);
        log.debug("variable length sysin reader activated");
    }

    @Override
    protected void loadDataToCache() throws IOException {
        context.setInputCache(new VariableLengthInputCache(context, System.in));
        try {
            System.in.close();
            log.debug("loaded SYSIN");
        } catch (Exception e) {
            log.debug("load SYSIN failed");
        }
    }
}
