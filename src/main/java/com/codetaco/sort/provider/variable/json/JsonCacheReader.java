package com.codetaco.sort.provider.variable.json;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthCacheReader;
import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class JsonCacheReader extends VariableLengthCacheReader {

    JsonCacheReader(final FunnelContext context) throws IOException, ParseException {
        super(context);
        log.debug("JSON cache reader activated");
    }

    @Override
    protected void loadDataToCache() throws IOException, ParseException {
        try (final FileInputStream inputStream =
               new FileInputStream(getContext().getInputFile(getContext().inputFileIndex()))) {
            getContext().setInputCache(new JsonInputCache(getContext(), inputStream));

            log.debug("loaded {}",
                      getContext().getInputFile(getContext().inputFileIndex()).getAbsolutePath());
        }
    }
}
