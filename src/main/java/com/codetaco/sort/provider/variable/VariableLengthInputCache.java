package com.codetaco.sort.provider.variable;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.AbstractInputCache;

import java.io.IOException;
import java.io.InputStream;

class VariableLengthInputCache extends AbstractInputCache {

    VariableLengthInputCache(final FunnelContext _context,
                             final InputStream _source) throws IOException {
        super(_context, _source);
    }

    @Override
    protected void postOpenVerification() {
        // nothing to do here
    }
}
