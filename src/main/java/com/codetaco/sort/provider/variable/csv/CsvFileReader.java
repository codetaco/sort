package com.codetaco.sort.provider.variable.csv;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthFileReader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
class CsvFileReader extends VariableLengthFileReader {

    CsvFileReader(final FunnelContext _context) throws IOException, ParseException {
        this(_context, defaultCharBufferSize);
        log.debug("csvIn file reader activated");
    }

    private CsvFileReader(final FunnelContext _context,
                          final int sz) throws IOException, ParseException {
        super(_context, sz);
    }

}
