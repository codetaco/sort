package com.codetaco.sort.provider.variable.json;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthURLReader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
class JsonURLReader extends VariableLengthURLReader {

    JsonURLReader(final FunnelContext context) throws IOException, ParseException {
        super(context);
        log.debug("JSON file reader activated");
    }

    @Override
    protected void loadDataToCache() throws IOException {
        getContext().setInputCache(
          new JsonInputCache(getContext(),
                             getContext().getInputURLs().get(getContext().inputFileIndex()).openStream()));
        log.debug("loaded URL body");
    }
}
