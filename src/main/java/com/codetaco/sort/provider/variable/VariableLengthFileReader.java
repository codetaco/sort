package com.codetaco.sort.provider.variable;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.InputReader;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;

@Slf4j
@Getter
public class VariableLengthFileReader implements InputReader {

    protected static int defaultCharBufferSize = 2048;
    private final FunnelContext context;
    private File inFile;
    private RandomAccessFile raf;
    private long startPosition;
    private final byte bb[];
    private int bbInUse;
    private final byte[] separator;
    private int bbNextPointer;
    private boolean eof;

    VariableLengthFileReader(final FunnelContext context) throws IOException, ParseException {
        this(context, defaultCharBufferSize);
        log.debug("variable length file reader activated");
    }

    protected VariableLengthFileReader(final FunnelContext context,
                                       final int sz) throws IOException, ParseException {
        assert sz > 0 : "Buffer size <= 0";
        this.context = context;
        bb = new byte[sz];
        separator = context.getEndOfRecordDelimiterIn();

        open(context.getInputFile(context.inputFileIndex()));
    }

    @Override
    public void close() throws IOException {
        raf.close();
        log.debug("loaded {}", inFile.getAbsolutePath());
    }

    private int fillBB() throws IOException {
        startPosition = raf.getFilePointer();
        bbInUse = raf.read(bb);
        bbNextPointer = 0;
        if (bbInUse == -1) {
            eof = true;
        }
        return bbInUse;
    }

    @Override
    public long length() throws IOException {
        return raf.length();
    }

    @Override
    public void open(final File inputFile) throws IOException, ParseException {
        bbNextPointer = 0;
        inFile = context.getInputFile(context.inputFileIndex());
        raf = new RandomAccessFile(inFile, "r");
        eof = false;
        fillBB();
    }

    @Override
    public long position() {
        return startPosition + bbNextPointer;
    }

    @Override
    public int read(final byte[] row) throws IOException {
        if (eof) {
            return -1;
        }

        int sepNextPointer = 0;

        /*
         * Clear the row before reading.
         */
        for (int b = 0; b < row.length; b++) {
            row[b] = 0x00;
        }

        for (int rowNextPointer = 0; ; bbNextPointer++) {
            /*
             * First see of the buffer is empty - refill it.
             */
            if (bbNextPointer >= bbInUse) {
                if (fillBB() <= 0) {
                    /*
                     * end of file without end of record, this is ok.
                     */
                    if (rowNextPointer == 0) {
                        return -1;
                    }
                    /*
                     * We have hit the end of the file and did not find an end
                     * of line for the last bytes we did find. Return the row
                     * for what is there.
                     */
                    log.warn("assuming a line terminator at end of file where {} unterminated bytes were found",
                             rowNextPointer);
                    return rowNextPointer;
                }
            }
            /*
             * Then see of this character is the next expected character in the
             * end of row sequence.
             */
            if (bb[bbNextPointer] == separator[sepNextPointer]) {
                if (sepNextPointer == separator.length - 1) {
                    bbNextPointer++;
                    return rowNextPointer;
                }
                sepNextPointer++;
                continue;
            }
            /*
             * Something that started to look like the end of row turned out not
             * to be. So give it to the caller.
             */
            if (sepNextPointer > 0) {
                for (int sp = 0; sp < sepNextPointer; sp++) {
                    row[rowNextPointer++] = separator[sp];
                }
                sepNextPointer = 0;
            }
            /*
             * transfer the byte to the row.
             */
            row[rowNextPointer++] = bb[bbNextPointer];
        }
    }
}
