package com.codetaco.sort.provider.variable.csv;

import com.codetaco.sort.AppContext;
import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

@Slf4j
public class CsvProvider extends VariableLengthProvider {

    private boolean includeColumn[];

    public CsvProvider(final boolean _includeColumn[]) throws Exception {
        this(new FunnelContext(new AppContext()));
        includeColumn = _includeColumn;
    }

    public CsvProvider(final FunnelContext _context) throws IOException, ParseException {
        super(_context);
        log.debug("CSV file provider activated");
        if (_context == null || _context.getInputColumnDefs() == null) {
            return;
        }
        /*
         * Find out which fields we really care about. No sense in moving around
         * bytes or analyzing fields we ultimately won't care about.
         */
        int highestKeyedColumnNumber = -1;

        for (final KeyPart kdef : _context.getInputColumnDefs()) {
            if (kdef.getCsvFieldNumber() > highestKeyedColumnNumber) {
                highestKeyedColumnNumber = kdef.getCsvFieldNumber();
            }
        }
        includeColumn = new boolean[highestKeyedColumnNumber + 1];
        for (final KeyPart kdef : _context.getInputColumnDefs()) {
            includeColumn[kdef.getCsvFieldNumber()] = true;
        }
    }

    @Override
    public long actualNumberOfRows() {
        return super.actualNumberOfRows()
          - (getContext().getCsvDefIn().header
          ? 1
          : 0);
    }

    @Override
    protected void assignReaderInstance() throws IOException, ParseException {
        if (getContext().isSysin()) {
            setReader(new CsvSysinReader(getContext()));
        } else if (getContext().isCacheInput()) {
            setReader(new CsvCacheReader(getContext()));
        } else {
            setReader(new CsvFileReader(getContext()));
        }
    }

    public byte[][] decodeCsv(final byte[] input,
                              final int inputLength,
                              final CSVFormat csvFormat) throws IOException {
        final byte[][] field = new byte[includeColumn.length][];

        if (inputLength > 0) {
            try (final CSVParser csvparser = CSVParser.parse(new String(input, 0, inputLength), csvFormat)) {
                final CSVRecord csvrecord = csvparser.getRecords().get(0);
                final Iterator<String> fields = csvrecord.iterator();
                for (int fNum = 0; fields.hasNext(); fNum++) {
                    final String fieldAsString = fields.next().trim();

                    if (fNum >= includeColumn.length) {
                        return field;
                    }

                    if (fNum < includeColumn.length) {
                        if (includeColumn[fNum]) {
                            field[fNum] = fieldAsString.getBytes();
                        }
                    }
                }
            }
        }
        return field;
    }

    /**
     * A special case of not selecting the row is when a header is expected on
     * the csvIn file. This is one line and is to be kept for writing but ignored
     * for sorting. The publisher needs to be sensitive to the csvIn header
     * possibility and force the header out at the front of the file.
     */
    @Override
    protected boolean isRowSelected(final int byteCount) {
        if (getContext().getCsvDefIn().header && getContext().getCsvDefIn().headerContents == null) {
            getContext().getCsvDefIn().headerContents = new byte[byteCount];
            System.arraycopy(getRow(),
                             0, getContext().getCsvDefIn().headerContents,
                             0, byteCount);
            return false;
        }
        return true;
    }

    @Override
    protected KeyContext postReadKeyProcessing(final int byteCount) throws IOException {
        KeyContext kContext;
        try {
            final byte[][] data = decodeCsv(getRow(), byteCount, getContext().getCsvDefIn().format);
            kContext = getContext().getKeyHelper().extractKey(data, getContinuousRecordNumber());

        } catch (final Exception e) {
            throw new IOException(e);
        }
        return kContext;
    }

    @Override
    protected void preSelectionExtract(final int byteCount) throws Exception {
        final byte[][] data = decodeCsv(getRow(), byteCount, getContext().getCsvDefIn().format);
        getContext().getColumnHelper().extract(getContext(),
                                               data,
                                               getContinuousRecordNumber(),
                                               byteCount,
                                               getCachedEquations());
    }

    @Override
    public void loadColumns(byte[] originalData,
                            long originalRecordNumber) throws IOException {
        final byte[][] data = decodeCsv(originalData, originalData.length, getContext().getCsvDefIn().format);
        getContext().getColumnHelper().loadColumnsFromBytes(data, originalRecordNumber);
    }

}
