package com.codetaco.sort.provider.variable.csv;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthCacheReader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
class CsvCacheReader extends VariableLengthCacheReader {

    CsvCacheReader(final FunnelContext context) throws IOException, ParseException {
        super(context);
        log.debug("csvIn cache reader activated");
    }
}
