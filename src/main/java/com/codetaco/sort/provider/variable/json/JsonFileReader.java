package com.codetaco.sort.provider.variable.json;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.VariableLengthFileReader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
class JsonFileReader extends VariableLengthFileReader {

    JsonFileReader(final FunnelContext context) throws IOException, ParseException {
        this(context, defaultCharBufferSize);
        log.debug("JSON file reader activated");
    }

    private JsonFileReader(final FunnelContext context,
                           final int sz) throws IOException, ParseException {
        super(context, sz);
    }

}
