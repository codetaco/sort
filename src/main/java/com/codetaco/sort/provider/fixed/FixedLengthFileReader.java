package com.codetaco.sort.provider.fixed;

import com.codetaco.sort.provider.InputReader;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

@Slf4j
public class FixedLengthFileReader implements InputReader {

    private File inputFile;
    private RandomAccessFile reader;

    FixedLengthFileReader(final File inputFile) throws IOException {
        open(inputFile);
    }

    @Override
    public void close() throws IOException {
        reader.close();
        log.debug("loaded {}", inputFile.getAbsolutePath());
    }

    @Override
    public long length() throws IOException {
        return reader.length();
    }

    @Override
    public void open(final File _inputFile) throws IOException {
        inputFile = _inputFile;
        reader = new RandomAccessFile(_inputFile, "r");
    }

    @Override
    public long position() throws IOException {
        return reader.getFilePointer();
    }

    @Override
    public int read(final byte[] row) throws IOException {
        return reader.read(row);
    }
}
