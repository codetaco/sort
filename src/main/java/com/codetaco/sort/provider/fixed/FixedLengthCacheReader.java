package com.codetaco.sort.provider.fixed;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.InputReader;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class FixedLengthCacheReader implements InputReader {
    final FunnelContext context;
    private long currentPosition;

    FixedLengthCacheReader(final FunnelContext context) throws IOException, ParseException {
        this.context = context;
        log.debug("fixed length cache provider activated");
        loadDataToCache();
        currentPosition = 0;
    }

    @Override
    public void close() {
        // intentionally empty
    }

    @Override
    public long length() {
        return context.getInputCache().length();
    }

    protected void loadDataToCache() throws IOException, ParseException {
        try (final FileInputStream inputStream = new FileInputStream(context.getInputFile(context.inputFileIndex()))) {
            context.setInputCache(new FixedLengthInputCache(context, inputStream));
            log.debug("loaded {}", context.getInputFile(context.inputFileIndex()).getAbsolutePath());
        }
    }

    @Override
    public void open(final File inputFile) {
        // intentionally empty
    }

    @Override
    public long position() {
        return context.getInputCache().position();
    }

    @Override
    public int read(final byte[] row) {
        if (context.getInputCache().eof()) {
            return -1;
        }

        final int count = context.getInputCache().read(context.inputFileIndex(), row, currentPosition, row.length);
        currentPosition += count;
        return count;
    }

    public void reset() {
        // Intentionally empty
    }
}
