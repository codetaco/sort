package com.codetaco.sort.provider.fixed;

import com.codetaco.sort.parameters.FunnelContext;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class FixedLengthSysinReader extends FixedLengthCacheReader {

    FixedLengthSysinReader(final FunnelContext context) throws IOException, ParseException {
        super(context);
        log.debug("fixed length sysin provider activated");
    }

    @Override
    protected void loadDataToCache() throws IOException {
        context.setInputCache(new FixedLengthInputCache(context, System.in));
        try {
            System.in.close();
            log.debug("loaded SYSIN");
        } catch (Exception e) {
            log.debug("load SYSIN failed");
        }
    }

}
