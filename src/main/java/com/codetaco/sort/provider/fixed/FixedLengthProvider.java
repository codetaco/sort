package com.codetaco.sort.provider.fixed;

import com.codetaco.sort.App;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.AbstractProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;

@Slf4j
public class FixedLengthProvider extends AbstractProvider {
    private long size;

    public FixedLengthProvider(final FunnelContext _context) throws IOException, ParseException {
        super(_context);
        log.debug("fixed length file provider activated");
    }

    @Override
    public long actualNumberOfRows() {
        return maximumNumberOfRows();
    }

    @Override
    protected void initialize() throws IOException, ParseException {
        initializeReader();
        try {
            size = getReader().length() / getContext().getFixedRecordLengthIn();
        } catch (final IOException e) {
            App.abort(-1, e);
        }
        setRow(new byte[getContext().getFixedRecordLengthIn()]);

        int optimalFunnelDepth = 2;
        long pow2 = size;

        /*
         * If the user specific a max rows expected then make sure to use that.
         * It might be the case that there are more than this single file being
         * sorted. And at this point we only know about the first one.
         */
        if (getContext().getMaximumNumberOfRows() > 0) {
            pow2 = getContext().getMaximumNumberOfRows();
        }

        while (pow2 >= 2) {
            pow2 /= 2;
            optimalFunnelDepth++;
        }
        if (getContext().getDepth() > optimalFunnelDepth) {
            log.debug("overriding power from {} to {}", getContext().getDepth(), optimalFunnelDepth);
            getContext().setDepth(optimalFunnelDepth);
        }
    }

    private void initializeReader() throws IOException, ParseException {
        if (getContext().isSysin()) {
            setReader(new FixedLengthSysinReader(getContext()));
        } else if (getContext().isCacheInput()) {
            setReader(new FixedLengthCacheReader(getContext()));
        } else {
            setReader(new FixedLengthFileReader(getContext().getInputFile(getContext().inputFileIndex())));
        }
    }

    @Override
    public long maximumNumberOfRows() {
        return size;
    }

    @Override
    protected boolean recordLengthOK(final int byteCount) {
        if (byteCount != -1 && byteCount != getContext().getFixedRecordLengthIn()) {
            log.warn("Record truncated at EOF, bytes read = {}, bytes expected = {}",
                     byteCount,
                     getContext().getFixedRecordLengthIn());
            return false;
        }
        return true;
    }

    private void setMaximumNumberOfRows(final long max) {
        size = max;
    }
}
