package com.codetaco.sort.provider.fixed;

import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.AbstractInputCache;

import java.io.IOException;
import java.io.InputStream;

public class FixedLengthInputCache extends AbstractInputCache {

    FixedLengthInputCache(final FunnelContext _context, final InputStream _source) throws IOException {
        super(_context, _source);
    }

    @Override
    protected void postOpenVerification() throws IOException {
        if (length() % getContext().getFixedRecordLengthIn() != 0) {
            throw new IOException("file size ("
                                    + length()
                                    + ") not even multiple of record size ("
                                    + getContext().getFixedRecordLengthIn()
                                    + ")");
        }
    }
}
