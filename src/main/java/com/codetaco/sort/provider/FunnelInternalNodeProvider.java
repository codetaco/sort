package com.codetaco.sort.provider;

import com.codetaco.sort.FunnelDataProvider;
import com.codetaco.sort.FunnelItem;
import com.codetaco.sort.Sort;

import java.io.IOException;
import java.text.ParseException;

public class FunnelInternalNodeProvider implements FunnelDataProvider {
    private final Sort funnel;
    private final FunnelItem left;
    private final FunnelItem right;

    public FunnelInternalNodeProvider(final Sort funnel,
                                      final FunnelItem leftContestantIndex,
                                      final FunnelItem rightContestantIndex) {
        this.funnel = funnel;
        left = leftContestantIndex;
        right = rightContestantIndex;
    }

    @Override
    public void loadColumns(byte[] originalData,
                            long originalRecordNumber) {
        funnel.getContext().getColumnHelper().loadColumnsFromBytes(originalData, originalRecordNumber);
    }

    @Override
    public long actualNumberOfRows() {
        return maximumNumberOfRows();
    }

    @Override
    public void attachTo(final FunnelItem item) {
        item.setProvider(this);
    }

    @Override
    public void close() {
        // intentionally empty
    }

    @Override
    public long maximumNumberOfRows() {
        return Long.MAX_VALUE;
    }

    @Override
    public boolean next(final FunnelItem item,
                        final long phase) throws IOException, ParseException {
        if (!left.isEndOfData() && left.getData() == null) {
            left.next(phase);
        }
        if (!right.isEndOfData() && right.getData() == null) {
            right.next(phase);
        }

        if (left.isEndOfData()) {
            if (right.isEndOfData()) {
                item.setEndOfData(true);
                return false;
            }
            item.setData(right.getData());
            item.setPhase(phase);
            right.next(phase);
            return true;
        } else if (right.isEndOfData()) {
            item.setData(left.getData());
            item.setPhase(phase);
            left.next(phase);
            return true;
        }
        /*
         * compare right and left nodes and use the one that is less
         */
        if (left.getData().compareTo(right.getData()) > 0) {
            item.setData(right.getData());
            item.setPhase(phase);
            right.next(phase);
        } else {
            item.setData(left.getData());
            item.setPhase(phase);
            left.next(phase);
        }

        return true;
    }

    @Override
    public void reset() {
        // Intentionally empty
    }
}
