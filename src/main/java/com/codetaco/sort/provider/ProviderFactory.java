package com.codetaco.sort.provider;

import com.codetaco.sort.FunnelDataProvider;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.fixed.FixedLengthProvider;
import com.codetaco.sort.provider.variable.VariableLengthProvider;
import com.codetaco.sort.provider.variable.csv.CsvProvider;
import com.codetaco.sort.provider.variable.json.JsonProvider;

import java.io.IOException;
import java.text.ParseException;

public class ProviderFactory {
    static public FunnelDataProvider create(final FunnelContext context) throws IOException, ParseException {
        if (context.getFixedRecordLengthIn() > 0) {
            return new FixedLengthProvider(context);
        }
        if (context.isURLInput() || context.getJsonDefIn() != null) {
            return new JsonProvider(context);
        }
        if (context.getCsvDefIn() == null) {
            return new VariableLengthProvider(context);
        }
        return new CsvProvider(context);
    }
}
