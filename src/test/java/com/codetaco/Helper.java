package com.codetaco;

import com.codetaco.sort.AppContext;
import com.codetaco.sort.columns.FormatOutHelper;
import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.parameters.FunnelContext;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class Helper {

    public static final String DOS_EOL = new String(new byte[]{0x0d, 0x0a});
    private static final File workDir = new File("target/junit");

    static {
        try {
            Files.createDirectory(Paths.get(workDir.getAbsolutePath()));
        } catch (Exception e) {
            //
        }
    }

    static public void compare(File file, List<String> expectedLines) {
        compare(file, expectedLines, false);
    }

    static public void compare(File file, List<String> expectedLines, boolean show) {
        int rowsReadFromFile = 0;
        try (BufferedReader sorted = new BufferedReader(new FileReader(file))) {
            String actual;
            while ((actual = sorted.readLine()) != null) {
                if (show) {
                    System.out.println(actual);
                }
                if (rowsReadFromFile < expectedLines.size()) {
                    Assertions.assertEquals(expectedLines.get(rowsReadFromFile),
                                            actual);
                }
                rowsReadFromFile++;
            }
        } catch (Exception e) {
            Assertions.fail("compare " + e.getMessage());
        }
        Assertions.assertEquals(expectedLines.size(), rowsReadFromFile);
    }

    static public void compareJson(File file, String paths) {
        try (BufferedReader sorted = new BufferedReader(new FileReader(file))) {
            String actual = sorted.readLine();
//            System.out.println(actual);
            JSONAssert.assertEquals(paths, actual, JSONCompareMode.STRICT_ORDER);
        } catch (Exception e) {
            Assertions.fail("compare " + e.getMessage());
        }
        Assertions.assertTrue(file.delete());
    }

    static public void compareFixed(File file, List<String> expectedData) {
        try (BufferedReader sorted = new BufferedReader(new FileReader(file))) {
            char[] foundChar = new char[1];
            int row = 0;
            for (String aRow : expectedData) {
                int col = 0;
                for (byte expected : aRow.getBytes()) {
                    Assertions.assertEquals(//"row " + row + " col " + col + " byte count",
                                            1,
                                            sorted.read(foundChar));
                    Assertions.assertEquals(//"row " + row + " col " + col,
                                            expected,
                                            (byte) foundChar[0]);
                    col++;
                }
                row++;
            }
        } catch (IOException e) {
            Assertions.fail("compare " + e.getMessage());
        }
    }

    static public void compareFixed(File file, String expectedData)
      throws IOException {
        try (BufferedReader sorted = new BufferedReader(new FileReader(file))) {
            char[] foundChar = new char[1];
            for (byte expected : expectedData.getBytes()) {
                Assertions.assertEquals(1, sorted.read(foundChar));
                Assertions.assertEquals(expected, (byte) foundChar[0]);
            }
        }
    }

    @NotNull
    static public AppContext config() {
        try {
            AppContext cxt = new AppContext(System.getProperty("user.dir"));
            cxt.specPath = new String[]{"src/test/resources/examples"};
            return cxt;
        } catch (Exception e) {
            Assertions.fail("HelperConfig " + e.getMessage());
            return null;
        }
    }

    static public CloseableFile nothingToDoSort() {
        PrintStream oldSysout = null;
        try {
            List<String> list = new ArrayList<>();
            File file = Helper.outFileWhenInIsSysin();
            oldSysout = Helper.createSysout(file);
            File nothingToDoFile = createUnsortedFile("nothingToDo", "", list, false);
            return CloseableFile.fromFile(nothingToDoFile, oldSysout);
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
            if (oldSysout != null) {
                System.setOut(oldSysout);
            }
            return null;
        }
    }

    public static class CloseableFile extends File implements Closeable {

        private static CloseableFile fromFile(File file) {
            return new CloseableFile(file.getAbsolutePath());
        }

        private static CloseableFile fromFile(File file, PrintStream oldSysout) {
            return new CloseableFile(file.getAbsolutePath(), oldSysout);
        }

        PrintStream oldSysout;

        private CloseableFile(@NotNull String pathname) {
            super(pathname);
        }

        private CloseableFile(@NotNull String pathname, PrintStream oldSysout) {
            super(pathname);
            this.oldSysout = oldSysout;
        }

        @NotNull
        public String forSort() {
            return Helper.quotedFileName(this);
        }

        @Override
        public void close() {
            try {
                if (!super.delete()) {
                    Assertions.fail("could not delete: " + getAbsolutePath());
                }
                if (oldSysout != null) {
                    System.setOut(oldSysout);
                }
            } catch (Exception e) {
                Assertions.fail(e.getMessage());
            }
        }
    }

    static public File createFixedUnsortedFile(String prefix,
                                               List<String> lines,
                                               int rowLength) throws IOException {
        File file = File.createTempFile(prefix + ".", ".in", workDir);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {

            lines.forEach(line -> {
                try {
                    out.write(line);
                    for (int fill = line.length(); fill < rowLength; fill++) {
                        out.write(" ");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        return file;
    }

    @NonNull
    static public File createUnsortedFile(List<String> lines) throws IOException {
        return createUnsortedFile("sort", System.lineSeparator(), lines, true);
    }

    @NonNull
    static public File createUnsortedFile(String prefix, List<String> lines) {
        try {
            return createUnsortedFile(prefix, System.lineSeparator(), lines);
        } catch (Exception e) {
            Assertions.fail("createUnsortedFile " + e.getMessage());
            return null;
        }
    }

    static public InputStream createSysin(String classpathFilename) {
        InputStream inputStream = ClassLoader.getSystemClassLoader().getResourceAsStream(classpathFilename);
        Assertions.assertNotNull(//classpathFilename + " does not exist in the test resources directory",
                                 inputStream);
        return createSysin(inputStream);
    }

    static public InputStream createSysin(InputStream inputStream) {
        InputStream oldSysin = System.in;
        System.setIn(inputStream);
        return oldSysin;
    }

    static public InputStream createSysin(List<String> lines) {
        return createSysin(lines, true);
    }

    static public InputStream createSysin(List<String> lines, boolean withLineFeeds) {
        StringBuilder sb = new StringBuilder();
        lines.forEach(line -> {
            sb.append(line.trim());
            if (withLineFeeds) {
                sb.append(System.lineSeparator());
            }
        });

//        System.setIn(new ByteArrayInputStream(sb.toString().getBytes()));
//        Scanner scanner = new Scanner(System.in);
//        System.out.println(scanner.nextLine());

        InputStream oldSysin = System.in;
        System.setIn(new ByteArrayInputStream(sb.toString().getBytes()));
        return oldSysin;
    }

    @NonNull
    static public File createUnsortedFile(String prefix,
                                          List<String> lines,
                                          boolean includeTrailingLineTerminator) {
        try {
            return createUnsortedFile(prefix, System.lineSeparator(), lines, includeTrailingLineTerminator);
        } catch (Exception e) {
            Assertions.fail("createUnsortedFile " + e.getMessage());
            return null;
        }

    }

    @NonNull
    static public File createUnsortedFile(String prefix, String eol, List<String> lines)
      throws IOException {
        return createUnsortedFile(prefix, eol, lines, true);
    }

    @NonNull
    static public File createUnsortedFile(String prefix,
                                          String eol,
                                          List<String> lines,
                                          boolean includeTrailingLineTerminator)
      throws IOException {
        File file = File.createTempFile(prefix + ".", ".in", workDir);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
            for (int idx = 0; idx < lines.size(); idx++) {
                int lengthToWrite = FormatOutHelper.lengthToWrite(lines.get(idx).getBytes(), 0,
                                                                  lines.get(idx).length(), true);

                String line = lines.get(idx);
                if (idx > 0) {
                    out.write(eol);
                }
                out.write(line, 0, lengthToWrite);
            }
            if (includeTrailingLineTerminator) {
                out.write(eol);
            }
        }
        return file;
    }

    static public KeyContext dummyKeyContext(String rawBytes) {
        KeyContext kx = new KeyContext();
        kx.setKey(new byte[255]);
        kx.setKeyLength(0);
        kx.setRawRecordBytes(new byte[1][]);
        kx.getRawRecordBytes()[0] = rawBytes.getBytes();
        kx.setRecordNumber(0);
        return kx;
    }

    public static void initializeFor(String testCaseName) {
//        System.out.println();
//        System.out.println(testCaseName);
    }

    @NotNull
    public static String quotedFileName(File file) {
        return "'" + file.getAbsolutePath() + "'";
    }

    public static File outFile(String testName) {
        return new File(outFileName(testName));
    }

    private static String outFileName(String jUnitTestName) {
        return workDir + "\\" + jUnitTestName + ".out";
    }

    static public File outFileWhenInIsSysin() {
        try {
            return File.createTempFile("sort.", ".out", workDir);
        } catch (Exception e) {
            Assertions.fail("compare " + e.getMessage());
            return null;
        }
    }

    static public String testName() {
        StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        return ste[2].getMethodName();
    }

    public static PrintStream createSysout(File file) {
        try {
            PrintStream outputStream = new PrintStream(new FileOutputStream(file));
            PrintStream oldSysout = System.out;
            System.setOut(outputStream);
            return oldSysout;
        } catch (Exception e) {
            Assertions.fail("sysout accumulator file: " + e.getMessage());
            return null;
        }
    }

    public static File captureSysinSysoutWhileRunning(String classpathSysin,
                                                      Supplier<FunnelContext> supplier) {
        File file = Helper.outFileWhenInIsSysin();
        InputStream oldSysin = Helper.createSysin(classpathSysin);
        PrintStream oldSysout = Helper.createSysout(file);
        FunnelContext context = supplier.get();
        System.setIn(oldSysin);
        System.setOut(oldSysout);
        return file;
    }

    public static FunnelContext captureSysoutWhileRunning(Supplier<FunnelContext> supplier) {
        File file = Helper.outFileWhenInIsSysin();
        PrintStream oldSysout = Helper.createSysout(file);
        FunnelContext context = supplier.get();
        System.setOut(oldSysout);
        return context;
    }
}
