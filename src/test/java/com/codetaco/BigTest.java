package com.codetaco;

import com.codetaco.cli.type.WildPath;
import com.codetaco.sort.Sort;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BigTest {

    @Test
    public void multipleInputFiles()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 1; x <= 1000000; x++) {
            in1.add("" + x);
        }

        List<String> expectedOut = new ArrayList<>();
        for (int x = 1000000; x >= 1; x--) {
            expectedOut.add("" + x);
            expectedOut.add("" + x);
            expectedOut.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName + "1", in1);
        File file2 = Helper.createUnsortedFile(testName + "2", in1);
        File file3 = Helper.createUnsortedFile(testName + "3", in1);

        WildPath wildPath = new WildPath("multipleInputFiles*");
        for (File fileToBeRemoved : wildPath.files()) {
            fileToBeRemoved.delete();
        }

        FunnelContext context = Sort.sort(Helper.config(),
                                          "'" + file.getParentFile().getAbsolutePath() + "/multipleInputFiles*' "
                                            + " --nocache"
                                            + " --col(int -o0 -l7 -n col1)"
                                            + " --orderby(col1 desc)"
                                            + " -o " + Helper.quotedFileName(output)
                                            + " --row 5000000 "
                                            + " --pow 16");

        Assertions.assertEquals(3000000L, context.getRecordCount());
        Assertions.assertEquals(3000000L, context.getWriteCount());
        Helper.compare(output, expectedOut);

        Assertions.assertTrue(file.delete());
        Assertions.assertTrue(file2.delete());
        Assertions.assertTrue(file3.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void oneBigFile() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 1; x <= 1000000; x++) {
            in1.add("" + x);
        }

        List<String> expectedOut = new ArrayList<>();
        for (int x = 1000000; x >= 1; x--) {
            expectedOut.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " -o " + Helper.quotedFileName(output)
                                                             + " --col(Int -o0 -l7 -n col1)"
                                                             + " --orderby(col1 desc)"
                                                             + " --pow 8");

        Assertions.assertEquals(1000000L, context.getRecordCount());
        Assertions.assertEquals(1000000L, context.getWriteCount());
        Helper.compare(output, expectedOut);

        Assertions.assertTrue(file.delete());
        Assertions.assertTrue(output.delete());
    }
}
