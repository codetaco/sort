package com.codetaco.sort.parameters;

import com.codetaco.Helper;
import com.codetaco.sort.Sort;
import com.codetaco.sort.SortException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class InputFormatDefaultTests {

    @Test
    public void defaultVariableOutput() {
        try (Helper.CloseableFile file = Helper.nothingToDoSort()) {
            FunnelContext context = Sort.sort(Helper.config(), file.forSort()
              + " --vI SP --col(in -o 0 -l 10)");
            Assertions.assertTrue(context.isVariableLengthOutputFormat());
            Assertions.assertEquals(0, context.getRecordCount());
            Assertions.assertEquals(0, context.getWriteCount());
        }
    }

    @Test
    public void defaultFixedOutput() {
        try (Helper.CloseableFile file = Helper.nothingToDoSort()) {
            FunnelContext context = Sort.sort(Helper.config(), file.forSort()
              + " --fixedI 10 --col(Integer -n aNumber -o 0 -l 10)");
            Assertions.assertTrue(context.isFixedOutputFormat());
            Assertions.assertEquals(0, context.getRecordCount());
            Assertions.assertEquals(0, context.getWriteCount());
        }
    }

    @Test
    public void defaultCsvOutput() {
        try (Helper.CloseableFile file = Helper.nothingToDoSort()) {
            FunnelContext context = Sort.sort(Helper.config(), file.forSort()
              + " --csvIn() --col(Integer --field 1 -n aNumber)");
            Assertions.assertTrue(context.isCsvOutputFormat());
            Assertions.assertEquals(0, context.getRecordCount());
            Assertions.assertEquals(0, context.getWriteCount());
        }
    }


    @Test
    public void defaultJsonOutput() {
        try (Helper.CloseableFile file = Helper.nothingToDoSort()) {
            FunnelContext context = Sort.sort(Helper.config(), file.forSort()
              + " --jsonIn(--path 123) --col(Integer --path 456 -n aNumber)");
            Assertions.assertTrue(context.isJsonOutputFormat());
            Assertions.assertEquals(0, context.getRecordCount());
            Assertions.assertEquals(0, context.getWriteCount());
        }
    }


    @Test
    public void fixedVarError() {
        try {
            Sort.sort(Helper.config(), "--vI X --fixedIn 10");
            Assertions.fail("should have failed");
        } catch (SortException e) {
            Assertions.assertEquals("--fixedIn and --variableIn are mutually exclusive parameters", e.getMessage());
        }
    }

    @Test
    public void fixedCsvError() {
        try {
            Sort.sort(Helper.config(), "--csvI () --fixedIn 10");
            Assertions.fail("should have failed");
        } catch (SortException e) {
            Assertions.assertEquals("--fixedIn and --csvIn are mutually exclusive parameters", e.getMessage());
        }
    }

    @Test
    public void fixedJsonError() {
        try {
            Sort.sort(Helper.config(), "--jsonI () --fixedIn 10");
            Assertions.fail("should have failed");
        } catch (SortException e) {
            Assertions.assertEquals("--fixedIn and --jsonIn are mutually exclusive parameters", e.getMessage());
        }
    }

    @Test
    public void variableCsvError() {
        try {
            Sort.sort(Helper.config(), "--csvI () --vI X");
            Assertions.fail("should have failed");
        } catch (SortException e) {
            Assertions.assertEquals("--variableIn and --csvIn are mutually exclusive parameters", e.getMessage());
        }
    }

    @Test
    public void variableJsonError() {
        try {
            Sort.sort(Helper.config(), "--jsonI () --vI X");
            Assertions.fail("should have failed");
        } catch (SortException e) {
            Assertions.assertEquals("--variableIn and --jsonIn are mutually exclusive parameters", e.getMessage());
        }
    }

    @Test
    public void csvJsonError() {
        try {
            Sort.sort(Helper.config(), "--jsonI () --csvI ()");
            Assertions.fail("should have failed");
        } catch (SortException e) {
            Assertions.assertEquals("--csvIn and --jsonIn are mutually exclusive parameters", e.getMessage());
        }
    }
}
