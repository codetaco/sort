package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DuplicateTest {

    @Test
    public void dupsFirst() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 130; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        out.add(in.get(0));

        int rowSize = System.lineSeparator().length() + 8;

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 130 --fixedIn " + rowSize
                                            + " --col(String -o0,-l3 -n col1)"
                                            + "--orderby(col1) "
                                            + "--dup firstonly");

        Assertions.assertEquals(130L, context.getRecordCount());
        Assertions.assertEquals(129L, context.getDuplicateCount());
        Assertions.assertEquals(1L, context.getWriteCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void dupsLast() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        int rowSize = System.lineSeparator().length() + 8;

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 130; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        out.add(in.get(129));

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 130 --fixedIn " + rowSize
                                            + " --col(String -o0,-l3 -n col1)"
                                            + "--orderby(col1) "
                                            + "--dup lastonly");

        Assertions.assertEquals(130L, context.getRecordCount());
        Assertions.assertEquals(129L, context.getDuplicateCount());
        Assertions.assertEquals(1L, context.getWriteCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void dupsReverse() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 130; r++) {
            in.add("row " + (r + 1000));
        }

        List<String> out = new ArrayList<>();
        for (int r = 129; r >= 0; r--) {
            out.add("row " + (r + 1000));
        }

        int rowSize = System.lineSeparator().length() + 8;

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 130 --fixedIn " + rowSize
                                            + " --col(String -o0,-l3 -n col1)"
                                            + "--orderby(col1) "
                                            + "--dup reverse");

        Assertions.assertEquals(130L, context.getRecordCount());
        Assertions.assertEquals(129L, context.getDuplicateCount());
        Assertions.assertEquals(130L, context.getWriteCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }
}
