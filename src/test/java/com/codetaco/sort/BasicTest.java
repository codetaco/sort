package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BasicTest {
    @Test
    public void integerSort() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("1");
        in1.add("111");
        in1.add("11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                                   + " -o " + Helper.quotedFileName(output)
                                                                   + " --col(int -o0 -l7 -n col1)"
                                                                   + " --orderby(col1 asc)"
                                                                   + " --pow 2"
                                                                   + " --row 3");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(3L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("1");
        expectedOut.add("11");
        expectedOut.add("111");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void multifileInputIntegerSort() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("1");
        in1.add("111");
        in1.add("21");

        File file1 = Helper.createUnsortedFile(testName + "1", in1);
        File file2 = Helper.createUnsortedFile(testName + "2", in1);
        FunnelContext context = Sort.sort(Helper.config(),
                                                Helper.quotedFileName(file1) + " " + Helper.quotedFileName(file2)
                                                  + " --nocache"
                                                  + " --col(int -o0 -l3 -n col1)"
                                                  + " --orderby(col1 desc)"
                                                  + " --pow 2"
                                                  + " --row 6"
                                                  + " -o " + Helper.quotedFileName(output));

        Assertions.assertEquals(6L, context.getRecordCount());
        Assertions.assertEquals(6L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("111");
        expectedOut.add("111");
        expectedOut.add("21");
        expectedOut.add("21");
        expectedOut.add("1");
        expectedOut.add("1");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(file2.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void multifileInputStringSort() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("1");
        in1.add("111");
        in1.add("21");

        File file1 = Helper.createUnsortedFile(testName + "1", in1);
        File file2 = Helper.createUnsortedFile(testName + "2", in1);
        FunnelContext context = Sort.sort(Helper.config(),
                                                Helper.quotedFileName(file1) + " " + Helper.quotedFileName(file2)
                                                  + " --nocache"
                                                  + " --col(String -o0 -l3 -n col1)"
                                                  + " --orderby(col1 desc)"
                                                  + " --pow 2"
                                                  + " --row 6"
                                                  + " -o " + Helper.quotedFileName(output));

        Assertions.assertEquals(6L, context.getRecordCount());
        Assertions.assertEquals(6L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("21");
        expectedOut.add("21");
        expectedOut.add("111");
        expectedOut.add("111");
        expectedOut.add("1");
        expectedOut.add("1");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(file2.delete());
        Assertions.assertTrue(output.delete());
    }
}
