package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.date.CalendarFactory;
import com.codetaco.sort.parameters.FunnelContext;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class DateKeyTest {

    @BeforeEach
    public void setZoneToLocalTime() {
        CalendarFactory.setZoneId(ZoneOffset.ofHours(-5));
    }

    @AfterEach
    public void setZoneToUTC() {
        CalendarFactory.setZoneId(ZoneOffset.UTC);
    }

    @NotNull
    private List<String> createInputList(SimpleDateFormat sdf, String... mods) {
        List<String> in = new ArrayList<>();
        ZonedDateTime localDateTime = CalendarFactory.asZoned("2018-10-05T09:25:43.123-05:00");
        for (int r = 0; r < 100; r++) {
            in.add(sdf.format(CalendarFactory.asLong(localDateTime, mods)));
        }
        return in;
    }

    @NotNull
    private List<String> createInputList(String... mods) {
        List<String> in = new ArrayList<>();
        ZonedDateTime localDateTime = CalendarFactory.asZoned("2018-10-05T09:25:43.123-05:00");
        for (int r = 0; r < 100; r++) {
            in.add(CalendarFactory.asJSON(localDateTime, mods));
        }
        return in;
    }

    @Test
    public void sortDate() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String format = "yyyyMMdd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        List<String> in = createInputList(sdf, "+1Day");
        List<String> out = new ArrayList<>();
        for (int r = 99; r >= 0; r--) {
            out.add(in.get(r));
        }

        int rowSize = 8;

        File file = Helper.createFixedUnsortedFile(testName, in, rowSize);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 100 --fixedIn " + rowSize
                                            + "--col(-ndate Date -o0 --fo '" + format + "')"
                                            + "--formatOut(date)"
                                            + "--o(date desc)");

        Assertions.assertEquals(100L, context.getRecordCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortDateImpliedFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String format = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        List<String> in = createInputList(sdf, "+1Day");
        List<String> out = new ArrayList<>();
        for (int r = 99; r >= 0; r--) {
            out.add(in.get(r));
        }

        int rowSize = 10;

        File file = Helper.createFixedUnsortedFile(testName, in, rowSize);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 100 --fixedIn " + rowSize
                                            + "--col(-ndate Date -o0)"
                                            + "--formatOut(date)"
                                            + "--o(date desc)");

        Assertions.assertEquals(100L, context.getRecordCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortTime() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String format = "HHmmss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        List<String> in = createInputList(sdf, "+1min");
        List<String> out = new ArrayList<>();
        for (int r = 99; r >= 0; r--) {
            out.add(in.get(r));
        }

        int rowSize = 6;

        File file = Helper.createFixedUnsortedFile(testName, in, rowSize);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 100 --fixedIn " + rowSize
                                            + "--col(-ntime Time -o0 --fo '" + format + "')"
                                            + "--formatOut(time)"
                                            + "--o(time desc)");

        Assertions.assertEquals(100L, context.getRecordCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortTimeImpliedFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String format = "HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        List<String> in = createInputList(sdf, "+1min");
        List<String> out = new ArrayList<>();
        for (int r = 99; r >= 0; r--) {
            out.add(in.get(r));
        }

        int rowSize = 8;

        File file = Helper.createFixedUnsortedFile(testName, in, rowSize);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 100 --fixedIn " + rowSize
                                            + "--col(-ntime Time -o0)"
                                            + "--formatOut(time)"
                                            + "--o(time desc) --vOut cr lf");

        Assertions.assertEquals(100L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortDateTime() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String format = "yyyyMMddHHmmss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        List<String> in = createInputList(sdf, "+1day");
        List<String> out = new ArrayList<>();
        for (int r = 99; r >= 0; r--) {
            out.add(in.get(r));
        }

        int rowSize = 14;

        File file = Helper.createFixedUnsortedFile(testName, in, rowSize);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 100 --fixedIn " + rowSize
                                            + "--col(-ndatetime DateTime -o0 --fo '" + format + "')"
                                            + "--formatOut(datetime)"
                                            + "--o(datetime desc)");

        Assertions.assertEquals(100L, context.getRecordCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortDateTimeImpliedFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = createInputList("+1day");
        List<String> out = new ArrayList<>();
        for (int r = 99; r >= 0; r--) {
            out.add(in.get(r));
        }

        int rowSize = 29;

        File file = Helper.createFixedUnsortedFile(testName, in, rowSize);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 100 --fixedIn " + rowSize
                                            + "--col(-ndatetime DateTime -o0)"
                                            + "--formatOut(datetime)"
                                            + "--o(datetime desc)");

        Assertions.assertEquals(100L, context.getRecordCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());
    }

}
