package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.date.CalendarFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class FormatOutputTest {

    @BeforeAll
    static public void setZoneToUTC() {
        CalendarFactory.setZoneId(ZoneOffset.UTC);
    }

    @Test
    public void defaultFiller() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(comments -l10)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(6) + "    " + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void formatInt() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l20 -n zipCode)"
                                     + " --fOut(zipCode --format '%8d')"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add("   12345");
        }
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void formatFloat() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("345.98765");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(float -o0 -l20 -n zipCode)"
                                     + " --fOut(zipCode --format '%13.4f')"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add("     345.9876");
        }
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void equColumnReferenceToString() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12346 line 1");
        out.add("54322 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(--equ 'toString(zipCode/2)' -l 10)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        exp.add("6173.0    12346");
        exp.add("27161.0   54322");

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void equColumnReferenceWithFormat() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12346 line 1");
        out.add("54322 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(--equ 'zipCode/2' -l 10 -d'%5.0f')(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        exp.add(" 6173     12346");
        exp.add("27161     54322");

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void equFormatDate() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12346 line 1");
        out.add("54322 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(zipCode)(--equ \"todate('20160510', 'yyyyMMdd')\" -l 19 -d'%tF')"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        exp.add("123462016-05-10");
        exp.add("543222016-05-10");

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void formatDate() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-09   line 1");
        out.add("Apr 10, 1960 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                             + " --col(date -l12 -n birthdate)(string -o13 -n comments)"
                                             + " --fOut(comments)(-s1)(birthdate -l 19 -d'%tF')"
                                             + " --orderby(birthdate)"
                                             + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 1 1960-04-09");
        exp.add("line 2 1960-04-10");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void formatDateTime() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-09T12:14Z         line 1");
        out.add("1960-04-10T01:02:03.456Z  line 2");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(),
                          Helper.quotedFileName(file)
                            + " --col(datetime -l26 -n birthdate)(string -o26 -n comments)"
                            + " --fOut(comments)(-s1)(birthdate -l 40 -d'%1$ta %1$tb %1$td, %1$tY %1$tT.%1$tL')"
                            + " --orderby(birthdate)"
                            + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 1 Sat Apr 09, 1960 12:14:00.000");
        exp.add("line 2 Sun Apr 10, 1960 01:02:03.456");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void formatTime() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-09   12:14        line 1");
        out.add("Apr 10, 1960 01:02:03.456 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(),
                          Helper.quotedFileName(file)
                            + " --col(time -o 13 -l13 -n birthtime)(string -o26 -n comments)"
                            + " --fOut(comments)(-s1)(birthtime -l 23 -d'%tT')"
                            + " --orderby(birthtime)"
                            + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 2 01:02:03");
        exp.add("line 1 12:14:00");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void equSimpleString() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(--equ \"'WHAT'\" -l 4 -s 5)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add("WHAT " + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void largerOutputArea() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(comments -l2 -f' ' -s3)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(6, 8) + " " + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void offset() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(comments -o1 -l2)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(7, 9) + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void offsetNoLength() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2a");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(comments -o1 -l5)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(7, 12) + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void oneColumnVariableLength() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)"
                                     + " --fOut(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void onlyFiller() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12346 line 1");
        out.add("54322 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(zipCode)(-s3)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        exp.add("12346   12346");
        exp.add("54322   54322");

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void truncate() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(comments -l2)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(6, 8) + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void twoColumnCSV() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345,line 1");
        out.add("54321,line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        try {
            Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                         + " --col(int -f1 -l5 -n zipCode)(string -f2 -n comments)"
                                         + " --formatOut(comments)(zipCode)"
                                         + " -r --csvIn() ");
        } catch (SortException e) {
            Assertions.assertEquals("--csvIn and --format are mutually exclusive parameters", e.getMessage());
        } finally {
            Assertions.assertTrue(file.delete());
        }
    }

    @Test
    public void twoColumnVariableLength() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -l6 -n comments)"
                                     + " --fOut(comments)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(6) + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void twoColumnVariableLengthImpliedFieldLength() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(comments)(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(6) + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void xFiller() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("12345 line 1");
        out.add("54321 line 2");

        File file = Helper.createUnsortedFile(testName, out, true);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " --col(int -o0 -l5 -n zipCode)(string -o6 -n comments)"
                                     + " --fOut(comments -l10 -f'x')(zipCode)"
                                     + " -r ");

        List<String> exp = new ArrayList<>();
        for (String expLine : out) {
            exp.add(expLine.substring(6) + "xxxx" + expLine.substring(0, 5));
        }

        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void noFormatDateTimes() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-09T21:33:01.543Z line 1");
        out.add("1960-04-09T21:33:01.543  line 2");
        out.add("1960-04-09               line 3");
        out.add("21:33:01.543             line 4");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                             + " --col(date -l25 -n date)(string -l6 -n comments)"
                                             + " --fOut(comments)"
                                             + "(-s1)(date -l 25 )"
                                             + "(-s1)(date -l 25 )"
                                             + "(-s1)(date -l 25 )"
                                             + "(-s1)(date -l 25 )"
                                             + " --orderby(date)"
                                             + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 1 1960-04-09T21:33:01.543Z  1960-04-09T21:33:01.543Z  1960-04-09T21:33:01.543Z  1960-04-09T21:33:01.543Z");
        exp.add("line 2 1960-04-09T21:33:01.543   1960-04-09T21:33:01.543   1960-04-09T21:33:01.543   1960-04-09T21:33:01.543");
        exp.add("line 3 1960-04-09                1960-04-09                1960-04-09                1960-04-09");
        exp.add("line 4 21:33:01.543              21:33:01.543              21:33:01.543              21:33:01.543");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void rezoned() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-07T21:33:01.543Z      line 1");
        out.add("1960-04-08T21:33:01.543+05:00 line 2");
        out.add("1960-04-09                    line 3");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                             + " --col(datetime -l30 -n date)(string -l6 -n comments)"
                                             + " --fOut(comments)"
                                             + "(-s1)(date -l 50 -z 'America/Chicago')"
                                             + "(-s1)(date -l 50 -z '+0500')"
                                             + " --orderby(date)"
                                             + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 1 1960-04-07T15:33:01.543-06:00[America/Chicago]     1960-04-08T02:33:01.543+05:00");
        exp.add("line 2 1960-04-08T10:33:01.543-06:00[America/Chicago]     1960-04-08T21:33:01.543+05:00");
        exp.add("line 3 1960-04-08T18:00:00-06:00[America/Chicago]         1960-04-09T05:00:00+05:00");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void rezonedTime() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("21:33:01.543                  line 4");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                             + " --col(time -l30 -n date)(string -l6 -n comments)"
                                             + " --fOut(comments)"
                                             + "(-s1)(date -l 20 -z '-0600')"
                                             + "(-s1)(date -l 20 -z '+0530')"
                                             + "(-s1)(date -l 20 --formatISO ISO_TIME )"
                                             + " --orderby(date)"
                                             + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 4 15:33:01.543-06:00   03:03:01.543+05:30   21:33:01.543Z");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void defaultDateTimeFormatters() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-09T21:33:01.543Z line 1");
        out.add("1960-04-09T21:33:01.543  line 2");
        out.add("1960-04-09               line 3");
        out.add("21:33:01.543             line 4");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                             + " --col(date -l25 -n date)(string -l6 -n comments)"
                                             + " --fOut(comments)"
                                             + "(-s1)(date -l 25 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 25 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 25 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 25 --formatISO DEFAULT)"
                                             + " --orderby(date)"
                                             + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 1 1960-04-09Z               1960-04-09Z               1960-04-09Z               1960-04-09Z");
        exp.add("line 2 1960-04-09Z               1960-04-09Z               1960-04-09Z               1960-04-09Z");
        exp.add("line 3 1960-04-09Z               1960-04-09Z               1960-04-09Z               1960-04-09Z");
        exp.add("line 4 -999999999-01-01Z         -999999999-01-01Z         -999999999-01-01Z         -999999999-01-01Z");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void defaultDates() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-09T21:33:01.543Z line 1");
        out.add("1960-04-09T21:33:01.543  line 2");
        out.add("1960-04-09               line 3");
        out.add("21:33:01.543             line 4");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                             + " --col(time -l25 -n date)(string -l6 -n comments)"
                                             + " --fOut(comments)"
                                             + "(-s1)(date -l 25 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 25 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 25 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 25 --formatISO DEFAULT)"
                                             + " --orderby(date)"
                                             + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 3 00:00:00Z                 00:00:00Z                 00:00:00Z                 00:00:00Z");
        exp.add("line 1 21:33:01.543Z             21:33:01.543Z             21:33:01.543Z             21:33:01.543Z");
        exp.add("line 2 21:33:01.543Z             21:33:01.543Z             21:33:01.543Z             21:33:01.543Z");
        exp.add("line 4 21:33:01.543Z             21:33:01.543Z             21:33:01.543Z             21:33:01.543Z");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void defaultDateTimes() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-09T21:33:01.543Z line 1");
        out.add("1960-04-09T21:33:01.543  line 2");
        out.add("1960-04-09               line 3");
        out.add("21:33:01.543             line 4");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                             + " --col(datetime -l25 -n date)(string -l6 -n comments)"
                                             + " --fOut(comments)"
                                             + "(-s1)(date -l 30 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 30 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 30 --formatISO DEFAULT)"
                                             + "(-s1)(date -l 30 --formatISO DEFAULT)"
                                             + " --orderby(date)"
                                             + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 3 1960-04-09T00:00:00Z           1960-04-09T00:00:00Z           1960-04-09T00:00:00Z           1960-04-09T00:00:00Z");
        exp.add("line 1 1960-04-09T21:33:01.543Z       1960-04-09T21:33:01.543Z       1960-04-09T21:33:01.543Z       1960-04-09T21:33:01.543Z");
        exp.add("line 2 1960-04-09T21:33:01.543Z       1960-04-09T21:33:01.543Z       1960-04-09T21:33:01.543Z       1960-04-09T21:33:01.543Z");
        exp.add("line 4 -999999999-01-01T21:33:01.543Z -999999999-01-01T21:33:01.543Z -999999999-01-01T21:33:01.543Z -999999999-01-01T21:33:01.543Z");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void defaultISO() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("1960-04-09T21:33:01.543Z line 1");
        out.add("1960-04-09T21:33:01.543  line 2");
        out.add("1960-04-09               line 3");

        File file = Helper.createUnsortedFile(testName, out, true);
        Helper.captureSysoutWhileRunning(
          () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                             + " --col(datetime -l25 -n date)(string -l6 -n comments)"
                                             + " --fOut(comments)"
                                             + "(-s1)(date -l 30 --formatISO RFC_1123_DATE_TIME)"
                                             + "(-s1)(date -l 30 --formatISO ISO_WEEK_DATE)"
                                             + "(-s1)(date -l 30 --formatISO ISO_INSTANT)"
                                             + "(-s1)(date -l 30 --formatISO ISO_ORDINAL_DATE)"
                                             + " --orderby(date)"
                                             + " --replace"));
        List<String> exp = new ArrayList<>();
        exp.add("line 3 Sat, 9 Apr 1960 00:00:00 GMT   1960-W14-6Z                    1960-04-09T00:00:00Z           1960-100Z");
        exp.add("line 1 Sat, 9 Apr 1960 21:33:01 GMT   1960-W14-6Z                    1960-04-09T21:33:01.543Z       1960-100Z");
        exp.add("line 2 Sat, 9 Apr 1960 21:33:01 GMT   1960-W14-6Z                    1960-04-09T21:33:01.543Z       1960-100Z");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

}
