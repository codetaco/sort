package com.codetaco.sort;

import com.codetaco.Helper;
import org.junit.jupiter.api.Test;

public class AbbreviationTests {
    @Test
    public void abbreviation1() {
        final String testName = Helper.testName();
        Helper.initializeFor(testName);
        Sort.sort(Helper.config(), " --syntaxOnly --outFN what --colsIn (s -o0 -l1 -n key)(i -o2 -l3 -n myNumber)"
                                     + " --oBy(key Asc)"
                                     + " --avg(-n avgNumber --equation 'myNumber')"
                                     + " --FOut(key)(-s1)(-eavgNumber -l4)");
    }
}
