package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.orderby.DateKey;
import com.codetaco.sort.orderby.DisplayFloatKey;
import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.orderby.KeyDirection;
import com.codetaco.sort.segment.SourceProxyRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CompareTests {
    static private SourceProxyRecord dummySourceProxyRecord(KeyContext kx1) {

        SourceProxyRecord spr1 = SourceProxyRecord.getInstance(null);
        spr1.setOriginalLocation(0);
        spr1.setOriginalRecordNumber(0);
        spr1.setOriginalSize(0);
        spr1.setSize(8);
        spr1.setSortKey(kx1.getKey());
        return spr1;
    }

    @Test
    public void compareDates() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DateKey key1 = new DateKey();
        key1.setOffset(0);
        key1.setLength(20);
        key1.setDirection(KeyDirection.ASC);
        key1.setParseFormat("yyyy-MM-dd");
        KeyContext kx1 = Helper.dummyKeyContext(" 1960-04-09");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DateKey key2 = new DateKey();
        key2.setOffset(0);
        key2.setLength(20);
        key2.setDirection(KeyDirection.ASC);
        key2.setParseFormat("yyyy-MM-dd");
        KeyContext kx2 = Helper.dummyKeyContext(" 1960-04-10");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(-1, spr1.compareTo(spr2));
    }

    @Test
    public void compareDatesShortFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DateKey key1 = new DateKey();
        key1.setOffset(0);
        key1.setLength(20);
        key1.setDirection(KeyDirection.ASC);
        key1.setParseFormat("y-M-d");
        KeyContext kx1 = Helper.dummyKeyContext(" 1960-04-09");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DateKey key2 = new DateKey();
        key2.setOffset(0);
        key2.setLength(20);
        key2.setDirection(KeyDirection.ASC);
        key2.setParseFormat("y-M-d");
        KeyContext kx2 = Helper.dummyKeyContext(" 1960-04-10");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(-1, spr1.compareTo(spr2));
    }

    @Test
    public void compareDatesWrapping() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DateKey key1 = new DateKey();
        key1.setOffset(0);
        key1.setLength(20);
        key1.setDirection(KeyDirection.ASC);
        key1.setParseFormat("y-M-d");
        KeyContext kx1 = Helper.dummyKeyContext(" 1962-04-9");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DateKey key2 = new DateKey();
        key2.setOffset(0);
        key2.setLength(20);
        key2.setDirection(KeyDirection.ASC);
        key2.setParseFormat("y-M-d");
        KeyContext kx2 = Helper.dummyKeyContext(" 91-3-40");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(-1, spr1.compareTo(spr2));
    }

    @Test
    public void compareDatesY2K() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DateKey key1 = new DateKey();
        key1.setOffset(0);
        key1.setLength(20);
        key1.setDirection(KeyDirection.ASC);
        key1.setParseFormat("y-M-d");
        KeyContext kx1 = Helper.dummyKeyContext(" 1962-04-9");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DateKey key2 = new DateKey();
        key2.setOffset(0);
        key2.setLength(20);
        key2.setDirection(KeyDirection.ASC);
        key2.setParseFormat("y-M-d");
        KeyContext kx2 = Helper.dummyKeyContext(" 91-4-10");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(-1, spr1.compareTo(spr2));
    }

    @Test
    public void compareDouble() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayFloatKey key1 = new DisplayFloatKey();
        key1.setOffset(0);
        key1.setLength(20);
        key1.setDirection(KeyDirection.ASC);
        KeyContext kx1 = Helper.dummyKeyContext("123.456");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DisplayFloatKey key2 = new DisplayFloatKey();
        key2.setOffset(0);
        key2.setLength(20);
        key2.setDirection(KeyDirection.ASC);
        KeyContext kx2 = Helper.dummyKeyContext("123.5");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(-1, spr1.compareTo(spr2));
    }

    @Test
    public void compareDoubleDec() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayFloatKey key1 = new DisplayFloatKey();
        key1.setOffset(0);
        key1.setLength(20);
        key1.setDirection(KeyDirection.DESC);
        KeyContext kx1 = Helper.dummyKeyContext("123456789012345.11");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DisplayFloatKey key2 = new DisplayFloatKey();
        key2.setOffset(0);
        key2.setLength(20);
        key2.setDirection(KeyDirection.DESC);
        KeyContext kx2 = Helper.dummyKeyContext("123456789012345.12");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(1, spr1.compareTo(spr2));
    }

    @Test
    public void compareDoubleMax16Whole() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayFloatKey key1 = new DisplayFloatKey();
        key1.setOffset(0);
        key1.setLength(20);
        key1.setDirection(KeyDirection.ASC);
        KeyContext kx1 = Helper.dummyKeyContext("1234567890123450");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DisplayFloatKey key2 = new DisplayFloatKey();
        key2.setOffset(0);
        key2.setLength(20);
        key2.setDirection(KeyDirection.ASC);
        KeyContext kx2 = Helper.dummyKeyContext("1234567890123451");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(-1, spr1.compareTo(spr2));
    }

    @Test
    public void compareDoubleMax18Decimals() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayFloatKey key1 = new DisplayFloatKey();
        key1.setOffset(0);
        key1.setLength(20);
        key1.setDirection(KeyDirection.ASC);
        KeyContext kx1 = Helper.dummyKeyContext("0.123456789012345670");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DisplayFloatKey key2 = new DisplayFloatKey();
        key2.setOffset(0);
        key2.setLength(20);
        key2.setDirection(KeyDirection.ASC);
        KeyContext kx2 = Helper.dummyKeyContext("0.123456789012345671");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(-1, spr1.compareTo(spr2));
    }

    @Test
    public void compareDoubleMaxRatio17Total() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayFloatKey key1 = new DisplayFloatKey();
        key1.setOffset(0);
        key1.setLength(18);
        key1.setDirection(KeyDirection.ASC);
        KeyContext kx1 = Helper.dummyKeyContext("123456789012345.11");
        key1.pack(kx1);
        SourceProxyRecord spr1 = dummySourceProxyRecord(kx1);

        DisplayFloatKey key2 = new DisplayFloatKey();
        key2.setOffset(0);
        key2.setLength(18);
        key2.setDirection(KeyDirection.ASC);
        KeyContext kx2 = Helper.dummyKeyContext("123456789012345.12");
        key2.pack(kx2);
        SourceProxyRecord spr2 = dummySourceProxyRecord(kx2);

        Assertions.assertEquals(-1, spr1.compareTo(spr2));
    }
}
