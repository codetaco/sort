package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ColumnTests {

    @Test
    public void notLastColumnInVLR() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("  FATALX");
        logFile.add("  INFO X");
        logFile.add("  WARN X");

        File file = Helper.createUnsortedFile(testName, logFile);
        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                + " --col(string -o2 -l5 -n level)"
                                                + " --where \"(rtrim(level) = 'INFO')\"");
        });
        Assertions.assertEquals(1L, context.getWriteCount());
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void notStartingInFirstColumn() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("  FATAL");
        logFile.add("  INFO ");
        logFile.add("  WARN ");

        File file = Helper.createUnsortedFile(testName, logFile);
        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                + " --col(string -o2 -l5 -n level)"
                                                + " --where \"(level = 'FATAL')\""
                                                + " -r ");
        });
        Assertions.assertEquals(1L, context.getWriteCount());
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void shortValue() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("  FATAL");
        logFile.add("  INFO ");
        logFile.add("  WARN ");

        File file = Helper.createUnsortedFile(testName, logFile);
        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                + " --col(string -o2 -l5 -n level)"
                                                + " --where \"(rtrim(level) = 'INFO')\"");
        });
        Assertions.assertEquals(1L, context.getWriteCount());
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void startingInFirstColumn() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("FATAL");
        logFile.add("INFO ");
        logFile.add("WARN ");

        File file = Helper.createUnsortedFile(testName, logFile);
        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                + " --col(string -o0 -l5 -n level)"
                                                + " --where \"(level = 'FATAL')\""
                                                + " -r ");
        });
        Assertions.assertEquals(1L, context.getWriteCount());
        Assertions.assertTrue(file.delete());
    }
}
