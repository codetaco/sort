package com.codetaco.sort;

import com.codetaco.Helper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class JsonTest {

    @Test
    public void sortByFirstColumn() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(String, --path age -n age)"
                            + "--orderBy (age)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{age:25,name:{first:Edwina}}").append(',');
        expectedResults.append("{age:27,name:{first:Garrett}}").append(',');
        expectedResults.append("{age:27,name:{first:Harrell}}").append(',');
        expectedResults.append("{age:34,name:{first:Carey}}").append(',');
        expectedResults.append("{age:36,name:{first:Albert}}").append(',');
        expectedResults.append("{age:39,name:{first:Maggie}}").append(',');
        expectedResults.append("{age:40,name:{first:Stout}}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void allowDefOfUnknownAttributes() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]' --options SUPPRESS_EXCEPTIONS)"
                            + " --jsonout()"
                            + "--col(String, --path age -n age)"
                            + "     (String, --path NotInJsonFile -n unknown)"
                            + "--orderBy (age)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{age:25,name:{first:Edwina}}").append(',');
        expectedResults.append("{age:27,name:{first:Garrett}}").append(',');
        expectedResults.append("{age:27,name:{first:Harrell}}").append(',');
        expectedResults.append("{age:34,name:{first:Carey}}").append(',');
        expectedResults.append("{age:36,name:{first:Albert}}").append(',');
        expectedResults.append("{age:39,name:{first:Maggie}}").append(',');
        expectedResults.append("{age:40,name:{first:Stout}}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void outputParameters() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]' --options SUPPRESS_EXCEPTIONS)"
                            + "--col(String, --path age -n age)"
                            + "     (String, --path NotInJsonFile -n unknown)"
                            + "--orderBy (age)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{age:25,name:{first:Edwina}}").append(',');
        expectedResults.append("{age:27,name:{first:Garrett}}").append(',');
        expectedResults.append("{age:27,name:{first:Harrell}}").append(',');
        expectedResults.append("{age:34,name:{first:Carey}}").append(',');
        expectedResults.append("{age:36,name:{first:Albert}}").append(',');
        expectedResults.append("{age:39,name:{first:Maggie}}").append(',');
        expectedResults.append("{age:40,name:{first:Stout}}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void sortBySecondColumn() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(I, --path age -n age)(S, --path name.first -n firstName)"
                            + "--orderBy (firstName)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{age:36,name:{first:Albert}}").append(',');
        expectedResults.append("{age:34,name:{first:Carey}}").append(',');
        expectedResults.append("{age:25,name:{first:Edwina}}").append(',');
        expectedResults.append("{age:27,name:{first:Garrett}}").append(',');
        expectedResults.append("{age:27,name:{first:Harrell}}").append(',');
        expectedResults.append("{age:39,name:{first:Maggie}}").append(',');
        expectedResults.append("{age:40,name:{first:Stout}}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());

    }

    @Test
    public void selected() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(Int, --path age -n age)"
//                            + " (String, --path isactive -n active)"
                            + " (String, --path name.first -n firstName)"
                            + "--orderBy (firstName)"
                            + "--where 'age>30'"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{age:36,name:{first:Albert}}").append(',');
        expectedResults.append("{age:34,name:{first:Carey}}").append(',');
        expectedResults.append("{age:39,name:{first:Maggie}}").append(',');
        expectedResults.append("{age:40,name:{first:Stout}}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());

    }

    @Test
    public void stop() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(Int, --path age -n age)"
//                            + " (String, --path isactive -n active)"
                            + " (String, --path name.first -n firstName)"
                            + "--orderBy (firstName)"
                            + "--stop 'firstName = \"Garrett\"'"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{name:{first:Carey}},");
        expectedResults.append("{name:{first:Edwina}},");
        expectedResults.append("{name:{first:Stout}}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void accessToMixedCaseFieldName() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(String, --path name.first -n firstName)"
                            + "--col(String, --path isActive -n active)"
                            + "--orderBy (firstName desc)"
                            + "--where 'active = \"true\"'"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{name:{first:Carey}},");
        expectedResults.append("{name:{first:Albert}}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void dollars() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(String, --path name.first -n firstName)"
                            + "--col(Float, --path balance -n balance)"
                            + "--orderBy (balance desc)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{balance:'$3,940.64'},");
        expectedResults.append("{balance:'$3,797.04'},");
        expectedResults.append("{balance:'$3,389.97'},");
        expectedResults.append("{balance:'$2,999.46'},");
        expectedResults.append("{balance:'$2,340.57'},");
        expectedResults.append("{balance:'$1,446.30'},");
        expectedResults.append("{balance:'$1,130.35'}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());

    }

    @Test
    public void variableOut() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--vOut cr,lf"
                            + "--col(String, --path name.first -n firstName)"
                            + "--col(Float, --path balance -n balance)"
                            + "--formatOut(firstName -s15)(balance)"
                            + "--orderBy (balance desc)"));

        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("Maggie         $3,940.64");
        expectedResults.add("Harrell        $3,797.04");
        expectedResults.add("Carey          $3,389.97");
        expectedResults.add("Edwina         $2,999.46");
        expectedResults.add("Stout          $2,340.57");
        expectedResults.add("Albert         $1,446.30");
        expectedResults.add("Garrett        $1,130.35");
        Helper.compare(outfile, expectedResults);

    }

    @Test
    public void fixedOut() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--fixedOut 24"
                            + "--col(String, --path name.first -n firstName)"
                            + "--col(Float, --path balance -n balance)"
                            + "--formatOut(firstName -s15)(balance)"
                            + "--orderBy (balance desc)"));

        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("Maggie         $3,940.64");
        expectedResults.add("Harrell        $3,797.04");
        expectedResults.add("Carey          $3,389.97");
        expectedResults.add("Edwina         $2,999.46");
        expectedResults.add("Stout          $2,340.57");
        expectedResults.add("Albert         $1,446.30");
        expectedResults.add("Garrett        $1,130.35");
        Helper.compareFixed(outfile, expectedResults);

    }

    @Test
    public void csvOut() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--csvOut (-s -h)"
                            + "--col(String, --path name.first -n firstName)"
                            + "--col(Float, --path balance -n balance)"
                            + "--formatOut(firstName)(balance)(modbal --equ 'balance%1000' --format '%.02f' --length 11)"
                            + "--orderBy (balance desc)"));

        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("firstName,balance,modbal");
        expectedResults.add("Maggie,\"$3,940.64\",940.64");
        expectedResults.add("Harrell,\"$3,797.04\",797.04");
        expectedResults.add("Carey,\"$3,389.97\",389.97");
        expectedResults.add("Edwina,\"$2,999.46\",999.46");
        expectedResults.add("Stout,\"$2,340.57\",340.57");
        expectedResults.add("Albert,\"$1,446.30\",446.30");
        expectedResults.add("Garrett,\"$1,130.35\",130.35");
        Helper.compare(outfile, expectedResults);

    }

    @Test
    public void jsonOutNoFormat() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(String, --path name.first -n firstName)"
                            + "--col(Float, --path balance -n balance)"
                            + "--orderBy (balance desc)"));

        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("[{\"_id\":\"5b8abc1cd714a345071a8c7a\",\"index\":5,\"guid\":\"874ab27e-5f7c-4564-a2c0-713ac210126b\",\"isActive\":false,\"balance\":\"$3,940.64\",\"picture\":\"http://placehold.it/32x32\",\"age\":39,\"eyeColor\":\"blue\",\"name\":{\"first\":\"Maggie\",\"last\":\"Carrillo\"},\"company\":\"ORBOID\",\"email\":\"maggie.carrillo@orboid.name\",\"phone\":\"+1 (824) 567-2812\",\"address\":\"448 Herkimer Street, Retsof, Colorado, 2166\",\"about\":\"Irure excepteur duis nisi voluptate voluptate veniam aute excepteur. Tempor duis laboris anim magna. Ex ad voluptate proident voluptate quis aliqua ut incididunt voluptate. Sunt culpa reprehenderit id mollit laborum aliqua laboris ea Lorem proident et. Anim dolore culpa nisi reprehenderit enim exercitation. Incididunt aliqua reprehenderit consequat non ea.\",\"registered\":\"Wednesday, February 22, 2017 4:06 AM\",\"latitude\":\"-20.082773\",\"longitude\":\"56.848537\",\"tags\":[\"do\",\"ad\",\"sit\",\"ipsum\",\"esse\"],\"range\":[0,1,2,3,4,5,6,7,8,9],\"friends\":[{\"id\":0,\"name\":\"Phelps Middleton\"},{\"id\":1,\"name\":\"Abigail Velez\"},{\"id\":2,\"name\":\"Howe Green\"}],\"greeting\":\"Hello, Maggie! You have 5 unread messages.\",\"favoriteFruit\":\"apple\"},{\"_id\":\"5b8abc1cd9d47f8610153343\",\"index\":4,\"guid\":\"1ab83f7e-a54e-4d7b-9ddf-596369997cbf\",\"isActive\":false,\"balance\":\"$3,797.04\",\"picture\":\"http://placehold.it/32x32\",\"age\":27,\"eyeColor\":\"green\",\"name\":{\"first\":\"Harrell\",\"last\":\"Holder\"},\"company\":\"UNIWORLD\",\"email\":\"harrell.holder@uniworld.co.uk\",\"phone\":\"+1 (958) 549-2576\",\"address\":\"714 Oakland Place, Malott, Connecticut, 6489\",\"about\":\"Fugiat aliqua labore nisi non incididunt id. Eu nostrud enim deserunt do exercitation ut ad excepteur ex amet cupidatat in. Irure in sint labore et et sit ipsum sunt duis sit aliquip reprehenderit excepteur. Reprehenderit dolore duis excepteur consequat voluptate adipisicing esse consectetur commodo nostrud minim. Dolore commodo nostrud quis consectetur voluptate amet laborum ex esse.\",\"registered\":\"Tuesday, March 25, 2014 5:25 AM\",\"latitude\":\"3.801338\",\"longitude\":\"-102.088813\",\"tags\":[\"labore\",\"dolore\",\"fugiat\",\"qui\",\"ad\"],\"range\":[0,1,2,3,4,5,6,7,8,9],\"friends\":[{\"id\":0,\"name\":\"Kane Monroe\"},{\"id\":1,\"name\":\"Della Rojas\"},{\"id\":2,\"name\":\"Queen Freeman\"}],\"greeting\":\"Hello, Harrell! You have 7 unread messages.\",\"favoriteFruit\":\"apple\"},{\"_id\":\"5b8abc1c5295ec74df72835e\",\"index\":0,\"guid\":\"945e9d01-55c0-4065-bdfa-71a54e2ba9d8\",\"isActive\":true,\"balance\":\"$3,389.97\",\"picture\":\"http://placehold.it/32x32\",\"age\":34,\"eyeColor\":\"brown\",\"name\":{\"first\":\"Carey\",\"last\":\"Cobb\"},\"company\":\"PORTALIS\",\"email\":\"carey.cobb@portalis.me\",\"phone\":\"+1 (807) 562-2205\",\"address\":\"338 Battery Avenue, Titanic, New Mexico, 507\",\"about\":\"Esse incididunt quis aliquip id qui. Irure in officia ad consectetur consectetur fugiat aute est exercitation aliquip nisi consequat exercitation. Laborum id sint adipisicing in sint est incididunt dolor in. Irure duis dolor eu tempor eu ullamco et in ullamco veniam voluptate in. Nulla est proident occaecat labore Lorem sit eu deserunt consequat sit minim.\",\"registered\":\"Monday, July 17, 2017 12:58 PM\",\"latitude\":\"-57.521799\",\"longitude\":\"87.430673\",\"tags\":[\"officia\",\"adipisicing\",\"fugiat\",\"labore\",\"sint\"],\"range\":[0,1,2,3,4,5,6,7,8,9],\"friends\":[{\"id\":0,\"name\":\"Gale Spears\"},{\"id\":1,\"name\":\"Patrica Short\"},{\"id\":2,\"name\":\"Rhoda Cooley\"}],\"greeting\":\"Hello, Carey! You have 7 unread messages.\",\"favoriteFruit\":\"apple\"},{\"_id\":\"5b8abc1ca53f5e7fa00e50f3\",\"index\":2,\"guid\":\"94aeba55-c06e-44e2-be78-a5d9fef96989\",\"isActive\":false,\"balance\":\"$2,999.46\",\"picture\":\"http://placehold.it/32x32\",\"age\":25,\"eyeColor\":\"green\",\"name\":{\"first\":\"Edwina\",\"last\":\"Neal\"},\"company\":\"CODAX\",\"email\":\"edwina.neal@codax.io\",\"phone\":\"+1 (905) 409-2204\",\"address\":\"419 Bay Parkway, Calverton, Ohio, 4348\",\"about\":\"Consequat velit duis ipsum commodo exercitation laborum ea commodo consectetur sit officia. Magna cupidatat duis incididunt commodo consequat id velit reprehenderit sit. Ex magna duis aliqua proident incididunt deserunt dolore velit. Mollit reprehenderit irure sit mollit occaecat id Lorem fugiat aute. Lorem mollit cupidatat cillum sint reprehenderit mollit eiusmod officia cillum. Minim non labore anim esse cillum esse do laborum quis culpa officia do aliqua sunt. Magna excepteur Lorem aliquip ea dolore nulla nulla sint.\",\"registered\":\"Monday, December 19, 2016 1:06 PM\",\"latitude\":\"-40.108735\",\"longitude\":\"-34.09153\",\"tags\":[\"consequat\",\"ea\",\"aliqua\",\"laborum\",\"tempor\"],\"range\":[0,1,2,3,4,5,6,7,8,9],\"friends\":[{\"id\":0,\"name\":\"Pollard Goff\"},{\"id\":1,\"name\":\"Stefanie Hutchinson\"},{\"id\":2,\"name\":\"Yesenia Allen\"}],\"greeting\":\"Hello, Edwina! You have 10 unread messages.\",\"favoriteFruit\":\"strawberry\"},{\"_id\":\"5b8abc1cfef2417d3fe286dd\",\"index\":1,\"guid\":\"ad2d3736-e903-4ad7-b7d5-02a833dfe29a\",\"isActive\":false,\"balance\":\"$2,340.57\",\"picture\":\"http://placehold.it/32x32\",\"age\":40,\"eyeColor\":\"brown\",\"name\":{\"first\":\"Stout\",\"last\":\"Delacruz\"},\"company\":\"MENBRAIN\",\"email\":\"stout.delacruz@menbrain.biz\",\"phone\":\"+1 (942) 535-3282\",\"address\":\"602 Reeve Place, Tetherow, Utah, 8525\",\"about\":\"Voluptate irure et eiusmod ut aliqua ex adipisicing commodo aliqua nostrud. Id quis ad est ut qui non est magna cupidatat fugiat aliqua consequat. Dolore est nostrud qui aute sint ut irure cupidatat veniam excepteur. Non ullamco nostrud cupidatat elit.\",\"registered\":\"Tuesday, July 14, 2015 5:46 AM\",\"latitude\":\"8.409979\",\"longitude\":\"-72.185114\",\"tags\":[\"ullamco\",\"do\",\"irure\",\"velit\",\"esse\"],\"range\":[0,1,2,3,4,5,6,7,8,9],\"friends\":[{\"id\":0,\"name\":\"Molina Kinney\"},{\"id\":1,\"name\":\"Cara Chambers\"},{\"id\":2,\"name\":\"Bates Lynch\"}],\"greeting\":\"Hello, Stout! You have 5 unread messages.\",\"favoriteFruit\":\"banana\"},{\"_id\":\"5b8abc1c398de3679359e154\",\"index\":6,\"guid\":\"9987e891-14af-4d35-9d57-acba846f22f9\",\"isActive\":true,\"balance\":\"$1,446.30\",\"picture\":\"http://placehold.it/32x32\",\"age\":36,\"eyeColor\":\"green\",\"name\":{\"first\":\"Albert\",\"last\":\"Irwin\"},\"company\":\"XYMONK\",\"email\":\"albert.irwin@xymonk.net\",\"phone\":\"+1 (809) 587-3473\",\"address\":\"914 Tompkins Avenue, Muir, Maryland, 7272\",\"about\":\"Labore quis nisi aliqua proident tempor non duis. Aliquip aute sunt amet reprehenderit mollit aliquip. Dolore occaecat duis et amet sunt ut ut reprehenderit laboris labore et dolore. Dolor cupidatat cupidatat anim proident fugiat deserunt enim aliquip voluptate duis sint ea. Aute pariatur proident anim eiusmod mollit laborum non anim minim consectetur elit eu excepteur. Sit ad do ea proident occaecat quis sint proident et velit.\",\"registered\":\"Friday, April 24, 2015 12:44 PM\",\"latitude\":\"44.220304\",\"longitude\":\"-50.641627\",\"tags\":[\"consequat\",\"pariatur\",\"eu\",\"nulla\",\"enim\"],\"range\":[0,1,2,3,4,5,6,7,8,9],\"friends\":[{\"id\":0,\"name\":\"Sears Carter\"},{\"id\":1,\"name\":\"Griffin Lowe\"},{\"id\":2,\"name\":\"Velazquez Dunn\"}],\"greeting\":\"Hello, Albert! You have 6 unread messages.\",\"favoriteFruit\":\"strawberry\"},{\"_id\":\"5b8abc1c00812a43d688be3d\",\"index\":3,\"guid\":\"14547751-1842-4bc5-bba1-f670fc9e0aea\",\"isActive\":false,\"balance\":\"$1,130.35\",\"picture\":\"http://placehold.it/32x32\",\"age\":27,\"eyeColor\":\"blue\",\"name\":{\"first\":\"Garrett\",\"last\":\"Dudley\"},\"company\":\"BEADZZA\",\"email\":\"garrett.dudley@beadzza.org\",\"phone\":\"+1 (853) 489-3904\",\"address\":\"980 Colonial Road, Muse, Missouri, 3282\",\"about\":\"Cupidatat excepteur aliquip cupidatat sint consequat aliqua ipsum est minim est Lorem fugiat sunt. Cupidatat consequat reprehenderit dolor nulla officia labore do minim nisi consectetur esse elit et. Reprehenderit id dolor exercitation consequat reprehenderit anim ea. Ut amet enim ut tempor duis. Sint sit ex esse non fugiat ex nulla deserunt fugiat ut Lorem.\",\"registered\":\"Saturday, August 11, 2018 12:06 PM\",\"latitude\":\"-80.496241\",\"longitude\":\"91.894162\",\"tags\":[\"quis\",\"consequat\",\"id\",\"adipisicing\",\"ut\"],\"range\":[0,1,2,3,4,5,6,7,8,9],\"friends\":[{\"id\":0,\"name\":\"Olive Duffy\"},{\"id\":1,\"name\":\"Bobbi Harrison\"},{\"id\":2,\"name\":\"Jacquelyn Conley\"}],\"greeting\":\"Hello, Garrett! You have 8 unread messages.\",\"favoriteFruit\":\"apple\"}]");
        Helper.compare(outfile, expectedResults);

    }

    @Test
    public void jsonOutFormat() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(String, --path name.first -n firstName)"
                            + "--col(Float, --path balance -n balance)"
                            + "--formatout (firstName)(balance)"
                            + "--orderBy (balance desc)"));

        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("[{\"firstName\":\"Maggie\",\"balance\":\"$3,940.64\"},{\"firstName\":\"Harrell\",\"balance\":\"$3,797.04\"},{\"firstName\":\"Carey\",\"balance\":\"$3,389.97\"},{\"firstName\":\"Edwina\",\"balance\":\"$2,999.46\"},{\"firstName\":\"Stout\",\"balance\":\"$2,340.57\"},{\"firstName\":\"Albert\",\"balance\":\"$1,446.30\"},{\"firstName\":\"Garrett\",\"balance\":\"$1,130.35\"}]");
        Helper.compare(outfile, expectedResults);

    }

    @Test
    public void jsonWithFieldAsKey() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "JsonTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$[*]')"
                            + "--col(String, --path name.first -n firstName)"
                            + "--col(Float, --path balance -n balance)"
                            + "--field (-n aField -e 'firstName')"
                            + "--formatout (aField)"
                            + "--orderBy (balance desc)(aField)"));

        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("[{\"aField\":\"Maggie\"},{\"aField\":\"Harrell\"},{\"aField\":\"Carey\"},{\"aField\":\"Edwina\"},{\"aField\":\"Stout\"},{\"aField\":\"Albert\"},{\"aField\":\"Garrett\"}]");
        Helper.compare(outfile, expectedResults);

    }
}
