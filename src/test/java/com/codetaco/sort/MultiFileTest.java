package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.cli.type.WildPath;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MultiFileTest {
    @Test
    public void twoInputFilesMerged()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> expectedOutput = new ArrayList<>();

        List<String> in1 = new ArrayList<>();
        in1.add("line 1");
        in1.add("line 3");

        List<String> in2 = new ArrayList<>();
        in2.add("line 2");
        in2.add("line 4");

        expectedOutput.add(in1.get(0));
        expectedOutput.add(in2.get(0));
        expectedOutput.add(in1.get(1));
        expectedOutput.add(in2.get(1));

        File file = Helper.createUnsortedFile(testName, in1);
        File file2 = Helper.createUnsortedFile(testName, in2);

        WildPath wildPath = new WildPath(testName + "*");
        for (File fileToBeRemoved : wildPath.files()) {
            fileToBeRemoved.delete();
        }

        FunnelContext context = Sort.sort(Helper.config(),
                                                "'" + file.getParentFile().getAbsolutePath() + "/" + testName + "*'"
                                                  + " --nocache"
                                                  + " -o " + Helper.quotedFileName(output)
                                                  + " --row 4 --variableOut LF");

        Assertions.assertEquals(4L, context.getRecordCount());
        Assertions.assertEquals(4L, context.getWriteCount());
        Helper.compare(output, expectedOutput);

        Assertions.assertTrue(file.delete());
        Assertions.assertTrue(file2.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void twoInputFilesWithReplace()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("line 1");
        out.add("line 2");

        File file = Helper.createUnsortedFile(testName, out);
        File file2 = Helper.createUnsortedFile(testName, out);

        FunnelContext context = Sort.sort(Helper.config(),
                                                Helper.quotedFileName(file)
                                                  + ","
                                                  + Helper.quotedFileName(file2)
                                                  + " --replace --row 2 -c original --variableOut LF");

        Assertions.assertEquals(4L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
        Assertions.assertTrue(file2.delete());
    }
}
