package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AggregateTest {

    @BeforeEach
    public void setZoneToLocalTime() {
//        CalendarFactory.setZoneId(ZoneOffset.ofHours(-5));
    }

    @AfterEach
    public void setZoneToUTC() {
//        CalendarFactory.setZoneId(ZoneOffset.UTC);
    }

    @Test
    public void avgColAndEquError()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        try {
            Sort.sort(Helper.config(), " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                         + " --orderby(key asc)"
                                         + " --avg(myNumber -n avgNumber --equ 'myNumber')"
                                         + " --FO(key)(-s1)(avgNumber)");
            Assertions.fail("Exception expected");
        } catch (SortException e) {
            Assertions.assertEquals(
              "aggregate \"avgnumber\" columnName and --equ are mutually exclusive",
              e.getMessage());
        }
    }

    @Test
    public void avgColDate()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 20160409");
        in1.add("a 20160411");
        in1.add("a 20160410");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(date -o2 -l8 -n myDate -d 'yyyyMMdd')"
                                                             + " --orderby(key asc)"
                                                             + " --avg(myDate -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l10 -d '%1$tm/%<td/%<tY')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 04/10/2016");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgColFloat()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(myNumber -n avg)"
                                                             + " --FO(key)(-s1)(-eavg -l3 -d '%03.0f')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 041");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgColHugeFloats()
      throws Throwable {
        int rows = 50;
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            in1.add("a " + Long.MAX_VALUE);
        }

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l20 -n myHugeNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(myHugeNumber -n myAvgHugeNumber)"
                                                             + " --FO(key)(-s1)(-e myAvgHugeNumber -l20 -d '%020.0f')");

        Assertions.assertEquals(rows, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 09223372036854776000");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgColHugeIntegers()
      throws Throwable {
        int rows = 50;
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            in1.add("a " + Long.MAX_VALUE);
        }

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l20 -n myHugeNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(myHugeNumber -n myAvgHugeNumber)"
                                                             + " --FO(key)(-s1)(-e myAvgHugeNumber -l20 -d '%020d')");

        Assertions.assertEquals(rows, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 09223372036854775807");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgColInt()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(myNumber -n avg)"
                                                             + " --FO(key)(-s1)(-eavg -l3 -d '%03d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 041");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgColIntWithAllNegatives()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a -1");
        in1.add("a -111");
        in1.add("a -11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l4 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(myNumber -n avg)"
                                                             + " --FO(key)(-s1)(-eavg -l3 -d '%03d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a -41");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgColIntWithHugeNegatives()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a " + (0 - Long.MAX_VALUE));
        in1.add("a " + (0 - Long.MAX_VALUE));
        in1.add("a " + (0 - Long.MAX_VALUE));

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l20 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(myNumber -n avg)"
                                                             + " --FO(key)(-s1)(-eavg -l20 -d '%020d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a -9223372036854775807");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgColIntWithMixedSigns()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a " + (0 - Long.MAX_VALUE));
        in1.add("a " + (0 - Long.MAX_VALUE));
        in1.add("a " + (0 + Long.MAX_VALUE));

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l20 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(myNumber -n avg)"
                                                             + " --FO(key)(-s1)(-eavg -l20 -d '%020d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a -3074457345618258602");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgEquDate()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 20160409");
        in1.add("a 20160411");
        in1.add("a 20160410");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context =
          Sort.sort(Helper.config(),
                    Helper.quotedFileName(file1)
                      + " -o "
                      + Helper.quotedFileName(output)
                      + " --col(string -o0 -l1 -n key)(date -o2 -l8 -n myDate -d 'yyyyMMdd')"
                      + " --orderby(key asc)"
                      + " --avg(-emyDate -n myMin)"
                      + " --FO(key)(-s1)(-e 'toDate(myMin)' -l10 -d '%1$tm/%<td/%<tY')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 04/10/2016");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgEquFloat()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(-emyNumber -n avg)"
                                                             + " --FO(key)(-s1)(-eavg -l3 -d '%03.0f')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 041");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgEquHugeFloats()
      throws Throwable {
        int rows = 50;
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            in1.add("a " + Long.MAX_VALUE);
        }

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l20 -n myHugeNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(-emyHugeNumber -n myAvgHugeNumber)"
                                                             + " --FO(key)(-s1)(-e myAvgHugeNumber -l20 -d '%020.0f')");

        Assertions.assertEquals(rows, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 09223372036854776000");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgEquHugeIntegers()
      throws Throwable {
        int rows = 50;
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            in1.add("a " + Long.MAX_VALUE);
        }

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l20 -n myHugeNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(-emyHugeNumber -n myAvgHugeNumber)"
                                                             + " --FO(key)(-s1)(-e myAvgHugeNumber -l20 -d '%020d')");

        Assertions.assertEquals(rows, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 09223372036854775807");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgEquInt()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --avg(-emyNumber -n avg)"
                                                             + " --FO(key)(-s1)(-eavg -l3 -d '%03d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 041");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void avgStringError()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        try {
            Sort.sort(Helper.config(), " --col(String -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                         + " --orderby(key asc)"
                                         + " --avg(key -n avgNumber)"
                                         + " --FO(key)(-s1)(avgNumber)");
            Assertions.fail("Exception expected");
        } catch (SortException e) {
            Assertions.assertEquals(
              "aggregate \"avgnumber\" must reference a numeric or date column: key (String)",
              e.getMessage());
        }
    }

    @Test
    public void counter()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");
        in1.add("z 22");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n col1)"
                                                             + " --orderby(key asc)"
                                                             + " --count(-n count)"
                                                             + " --FO(key)(-s1)(-ecount -l 2 -d '%02d')");

        Assertions.assertEquals(4L, context.getRecordCount());
        Assertions.assertEquals(2L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 03");
        expectedOut.add("z 01");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void counterOfNothing()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");
        in1.add("z 22");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --stopWhen 'recordNumber = 1'"
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n col1)"
                                                             + " --orderby(key asc)"
                                                             + " --count(-n count)"
                                                             + " --FO(key)(-s1)(-ecount -l 2 -d '%02d')");

        Assertions.assertEquals(0L, context.getRecordCount());
        Assertions.assertEquals(0L, context.getWriteCount());

        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void duplicateAggregateName()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        try {
            Sort.sort(Helper.config(), " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                         + " --orderby(key asc)"
                                         + " --count(-n badName)"
                                         + " --avg(myNumber -n badName)"
                                         + " --FO(key)(-s1)(badName)");
            Assertions.fail("Exception expected");
        } catch (SortException e) {
            Assertions.assertEquals("aggregate \"badname\" must have a unique name", e.getMessage());
        }
    }

    @Test
    public void everythingVariable()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 20160409 20170411");
        in1.add("a 20160411 20170410");
        in1.add("a 20160410 20160410");
        in1.add("c 20110409 20130411");
        in1.add("c 20110411 20120410");
        in1.add("b 20110410 20140410");
        in1.add("b 20120409 20110411");
        in1.add("b 20120411 20110410");
        in1.add("b 20120410 20100410");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort
                                  .sort(Helper.config(), Helper.quotedFileName(file1)
                                                           + " -o "
                                                           + Helper.quotedFileName(output)
                                                           + " --col(string -o0 -l1 -n key)(date -o2 -l8 -n myDate -d 'yyyyMMdd')(date -o11 -l8 -n myDate2 -d 'yyyyMMdd')"
                                                           + " --orderby(key asc)"
                                                           + " --avg(myDate -n myAvg)(myDate2 -n myAvg2)"
                                                           + " --min(myDate -n myMin)(myDate2 -n myMin2)"
                                                           + " --max(myDate -n myMax)(myDate2 -n myMax2)"
                                                           + " --count(-n myCount)"
                                                           + " --FO(key)"
                                                           + " (-s1)(-e myCount -l4 -d '%04d')"
                                                           + " (-s1)(-e myMin  -l10 -d '%1$tm/%<td/%<tY')"
                                                           + " (-s1)(-e myMin2 -l10 -d '%1$tm/%<td/%<tY')"
                                                           + " (-s1)(-e myMax  -l10 -d '%1$tm/%<td/%<tY')"
                                                           + " (-s1)(-e myMax2 -l10 -d '%1$tm/%<td/%<tY')"
                                                           + " (-s1)(-e myAvg  -l10 -d '%1$tm/%<td/%<tY')"
                                                           + " (-s1)(-e myAvg2 -l10 -d '%1$tm/%<td/%<tY')");

        Assertions.assertEquals(9L, context.getRecordCount());
        Assertions.assertEquals(3L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 0003 04/09/2016 04/10/2016 04/11/2016 04/11/2017 04/10/2016 12/09/2016");
        expectedOut.add("b 0004 04/10/2011 04/10/2010 04/11/2012 04/10/2014 01/09/2012 10/10/2011");
        expectedOut.add("c 0002 04/09/2011 04/10/2012 04/11/2011 04/11/2013 04/10/2011 10/10/2012");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void maxColDate()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 20160409");
        in1.add("a 20160411");
        in1.add("a 20160410");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(date -o2 -l8 -n myDate -d 'yyyyMMdd')"
                                                             + " --orderby(key asc)"
                                                             + " --max(myDate -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l10 -d '%1$tm/%<td/%<tY')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 04/11/2016");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void maxColFloat()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --max(myNumber -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l3 -d '%3.0f')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 111");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void maxColInt()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --max(myNumber -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l3 -d '%3d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 111");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void maxEquDate()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 20160409");
        in1.add("a 20160411");
        in1.add("a 20160410");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(date -o2 -l8 -n myDate -d 'yyyyMMdd')"
                                                             + " --orderby(key asc)"
                                                             + " --max(-emyDate -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l10 -d '%1$tm/%<td/%<tY')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 04/11/2016");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void maxEquFloat()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --max(-emyNumber -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l3 -d '%3.0f')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 111");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void maxEquInt()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --max(-emyNumber -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l3 -d '%3d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 111");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void minColDate()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 20160409");
        in1.add("a 20160411");
        in1.add("a 20160410");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(date -o2 -l8 -n myDate -d 'yyyyMMdd')"
                                                             + " --orderby(key asc)"
                                                             + " --min(myDate -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l10 -d '%1$tm/%<td/%<tY')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 04/09/2016");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void minColFloat()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --min(myNumber -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l3 -d '%3.0f')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a   1");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void minColInt()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --min(myNumber -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l3 -d '%3d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a   1");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void minEquDate()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 20160409");
        in1.add("a 20160411");
        in1.add("a 20160410");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(date -o2 -l8 -n myDate -d 'yyyyMMdd')"
                                                             + " --orderby(key asc)"
                                                             + " --min(-emyDate -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l10 -d '%1$tm/%<td/%<tY')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 04/09/2016");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void minEquFloat()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --min(-emyNumber -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l3 -d '%3.0f')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a   1");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void minEquInt()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --min(-emyNumber -n myMin)"
                                                             + " --FO(key)(-s1)(-e myMin -l3 -d '%3d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a   1");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void sumColFloat() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1.1");
        in1.add("a 111.1");
        in1.add("a 11.1");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l5 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --sum(myNumber -n sum)"
                                                             + " --FO(key)(-s1)(-esum -l7 -d '%03.3f')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 123.300");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void sumColInt()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --sum(myNumber -n sum)"
                                                             + " --FO(key)(-s1)(-esum -l5 -d '%03d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 123");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void sumEquFloat()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1.1");
        in1.add("a 111.1");
        in1.add("a 11.1");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(float -o2 -l5 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --sum(-emyNumber -n sum)"
                                                             + " --FO(key)(-s1)(-esum -l7 -d '%03.3f')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 123.300");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void sumEquInt()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("a 1");
        in1.add("a 111");
        in1.add("a 11");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                             + " -o "
                                                             + Helper.quotedFileName(output)
                                                             + " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                                             + " --orderby(key asc)"
                                                             + " --sum(-emyNumber -n sum)"
                                                             + " --FO(key)(-s1)(-esum -l5 -d '%03d')");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a 123");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void unknownColumn()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        try {
            Sort.sort(Helper.config(), " --col(string -o0 -l1 -n key)(int -o2 -l3 -n myNumber)"
                                         + " --orderby(key asc)"
                                         + " --avg(unknownColumnName -n avgNumber)"
                                         + " --FO(key)(-s1)(avgNumber)");
            Assertions.fail("Exception expected");
        } catch (SortException e) {
            Assertions.assertEquals(
              "aggregate \"avgnumber\" must reference a defined column: unknowncolumnname",
              e.getMessage());
        }
    }
}
