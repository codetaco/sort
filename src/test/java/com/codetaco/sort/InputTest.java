package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.List;

public class InputTest {
    @Test
    public void dos2unix() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("line 1");
        out.add("line 2");

        StringBuilder sb = new StringBuilder();
        for (String outline : out) {
            sb.append(outline).append(System.getProperty("line.separator"));
        }

        File file = Helper.createUnsortedFile(testName, out);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --replace --row 2 -c original --variableOut LF");

        Assertions.assertEquals(2L, context.getRecordCount());
        Helper.compare(file, out);
        file.delete();
    }

    @Test
    public void emptySysin() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String out = "";

        InputStream inputStream = new StringBufferInputStream(out);
        System.setIn(inputStream);

        File file = Helper.outFileWhenInIsSysin();
        try (PrintStream outputStream = new PrintStream(new FileOutputStream(file))) {
            System.setOut(outputStream);

            FunnelContext context = Sort.sort(Helper.config(), "--fixedIn 10 --row 2 ");
            Assertions.assertEquals(0L, context.getRecordCount());
        }
        try {
            file.delete();
        } catch (Exception e) {//
        }
    }

    @Test
    public void emptySysinNoMax() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String out = "";

        InputStream inputStream = new StringBufferInputStream(out);
        System.setIn(inputStream);

        File file = Helper.outFileWhenInIsSysin();
        try (PrintStream outputStream = new PrintStream(new FileOutputStream(file))) {
            System.setOut(outputStream);

            FunnelContext context = Sort.sort(Helper.config(), "");
            Assertions.assertEquals(0L, context.getRecordCount());
        }
        try {
            file.delete();
        } catch (Exception e) {//
        }
    }

    @Test
    public void everythingDefaults() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("line 1");
        out.add("line 2");

        StringBuilder sb = new StringBuilder();
        for (String outline : out) {
            sb.append(outline).append(System.getProperty("line.separator"));
        }
        InputStream inputStream = new StringBufferInputStream(sb.toString());
        System.setIn(inputStream);

        File file = Helper.outFileWhenInIsSysin();
        try (PrintStream outputStream = new PrintStream(new FileOutputStream(file))) {
            System.setOut(outputStream);
            FunnelContext context = Sort.sort(Helper.config());
            Assertions.assertEquals(2L, context.getRecordCount());
        }
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void fixedSysinSysout() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String out = "line 1line 2";

        InputStream inputStream = new StringBufferInputStream(out);
        System.setIn(inputStream);

        File file = Helper.outFileWhenInIsSysin();
        PrintStream outputStream = new PrintStream(new FileOutputStream(file));
        System.setOut(outputStream);

        FunnelContext context = Sort.sort(Helper.config(), "--fixedIn 6--row 2 -c original");
        Assertions.assertEquals(2L, context.getRecordCount());
        Helper.compareFixed(file, out);
        try {
            outputStream.close();
            Assertions.assertTrue(file.delete());
        } catch (Exception e) {
            //
        }
    }

    @Test
    public void fixInFixOut() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("10       x");
        out.add("5   X     ");
        out.add("8      X  ");

        File file = Helper.createFixedUnsortedFile(testName, out, 10);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --fixedIn 10 -r -c original");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(3L, context.getWriteCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());

    }

    @Test
    public void fixInFixOutLong() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("10       x");
        out.add("5   X     ");
        out.add("8      X  ");

        File file = Helper.createFixedUnsortedFile(testName, out, 10);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --fixedIn 10 --fixedOut 20 -c original");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(3L, context.getWriteCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());

    }

    @Test
    public void fixInFixOutShort() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("10       x");
        out.add("5   X     ");
        out.add("8      X  ");

        File file = Helper.createFixedUnsortedFile(testName, out, 10);

        out.clear();
        out.add("10   ");
        out.add("5   X");
        out.add("8    ");

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --fixedIn 10 --fixedOut 5 -r -c original");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(3L, context.getWriteCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());

    }

    @Test
    public void fixInVarOut() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("10       x");
        out.add("5   X");
        out.add("8      X");

        File file = Helper.createFixedUnsortedFile(testName, out, 10);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --fixedIn 10 --variableOut CR LF -r -c original");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(3L, context.getWriteCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());

    }

    @Test
    public void replaceErrorOut() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String out = "line 1line 2";

        InputStream inputStream = new StringBufferInputStream(out);
        System.setIn(inputStream);

        File file = Helper.outFileWhenInIsSysin();
        try {
            PrintStream outputStream = new PrintStream(new FileOutputStream(file));
            System.setOut(outputStream);

            try {
                Sort.sort(Helper.config(), "--replace --fixedIn 6--row 2 --col(S -nS) --o(S)");
                Assertions.fail("Expected error");
            } catch (SortException e) {
                Assertions.assertEquals(
                  "--replace requires --inputFile, redirection or piped input is not allowed",
                  e.getMessage());
            } finally {
                outputStream.close();
                try {
                    file.delete();
                } catch (Exception e) {
                    //
                }
            }
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void replaceInput() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("line 1");
        out.add("line 2");

        StringBuilder sb = new StringBuilder();
        for (String outline : out) {
            sb.append(outline).append(System.getProperty("line.separator"));
        }

        File file = Helper.createUnsortedFile(testName, out);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --replace --row 2 -c original");

        Assertions.assertEquals(2L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void replacePipedInputNotAllowed() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        String out = "line 1line 2";

        InputStream inputStream = new StringBufferInputStream(out);
        System.setIn(inputStream);

        File file = Helper.outFileWhenInIsSysin();
        try {
            PrintStream outputStream = new PrintStream(new FileOutputStream(file));
            System.setOut(outputStream);

            try {
                Sort.sort(Helper.config(), "--replace --fixedIn 6--row 2 --col(-nc String) --o(c)");
                Assertions.fail("Expected error");
            } catch (SortException e) {
                Assertions.assertEquals(
                  "--replace requires --inputFile, redirection or piped input is not allowed",
                  e.getMessage());
            } finally {
                outputStream.close();
                try {
                    file.delete();
                } catch (Exception e) {
                    //
                }

            }
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void sysinFileout() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("line 1");
        out.add("line 2");

        StringBuilder sb = new StringBuilder();
        for (String outline : out) {
            sb.append(outline).append(System.getProperty("line.separator"));
        }
        InputStream inputStream = new StringBufferInputStream(sb.toString());
        System.setIn(inputStream);

        File file = Helper.outFileWhenInIsSysin();

        FunnelContext context = Sort.sort(Helper.config(),
                                          "-o" + Helper.quotedFileName(file)
                                            + " --row 2 -c original");

        Assertions.assertEquals(2L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void variableSysinSysout() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("line 1");
        out.add("line 2");

        StringBuilder sb = new StringBuilder();
        for (String outline : out) {
            sb.append(outline).append(System.getProperty("line.separator"));
        }
        InputStream inputStream = new StringBufferInputStream(sb.toString());
        System.setIn(inputStream);

        File file = Helper.outFileWhenInIsSysin();
        try (PrintStream outputStream = new PrintStream(new FileOutputStream(file))) {
            System.setOut(outputStream);
            FunnelContext context = Sort.sort(Helper.config(), "--row 2 -c original ");
            Assertions.assertEquals(2L, context.getRecordCount());
            Helper.compare(file, out);
        }
        try {
            file.delete();
        } catch (Exception e) {//
        }

    }

    @Test
    public void variableUnterminatedLastLine() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("line 1");
        out.add("line 2");
        out.add("unterminated");

        File file = Helper.createUnsortedFile(testName, out, false);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + "--row 2 -c original -r ");

        Assertions.assertEquals(3L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void varInFixOut() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("10       X");
        out.add("5   X     ");
        out.add("8      X  ");

        File file = Helper.createUnsortedFile(testName, out);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --fixedOut 10 -r -c original");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(3L, context.getWriteCount());
        Helper.compareFixed(file, out);
        Assertions.assertTrue(file.delete());

    }

    @Test
    public void varInVarOut() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("10       X");
        out.add("5   X");
        out.add("8      X");

        File file = Helper.createUnsortedFile(testName, out, true);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " -r -c original");

        Assertions.assertEquals(3L, context.getRecordCount());
        Assertions.assertEquals(3L, context.getWriteCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }
}
