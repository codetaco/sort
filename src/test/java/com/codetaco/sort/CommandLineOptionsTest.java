package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.orderby.KeyType;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

public class CommandLineOptionsTest {
    private FunnelContext ctx;

    @Test
    public void defineCacheInput() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        ctx = new FunnelContext(Helper.config());
        Assertions.assertFalse(ctx.isNoCacheInput());

        ctx = new FunnelContext(Helper.config(), "--nocache");
        Assertions.assertTrue(ctx.isNoCacheInput());

        ctx = new FunnelContext(Helper.config(), "-!nocache");
        Assertions.assertFalse(ctx.isNoCacheInput());
    }

    @Test
    public void defineCacheWork() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        ctx = new FunnelContext(Helper.config());
        Assertions.assertFalse(ctx.isDiskWork());
        Assertions.assertTrue(ctx.isCacheWork());

        ctx = new FunnelContext(Helper.config(), "--diskWork");
        Assertions.assertTrue(ctx.isDiskWork());
        Assertions.assertFalse(ctx.isCacheWork());

        ctx = new FunnelContext(Helper.config(), "-!diskWork");
        Assertions.assertFalse(ctx.isDiskWork());
        Assertions.assertTrue(ctx.isCacheWork());
    }

    @Test
    public void defineColumnsSubparser() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        ctx = new FunnelContext(Helper.config(), "--col(String)");
        Assertions.assertEquals(1, ctx.getColumnHelper().getNames().size());
        Assertions.assertNull(ctx.getColumnHelper().getNames().get(0));
        Assertions.assertEquals(KeyType.String, ctx.getColumnHelper().get(null).getTypeName());
        /*
         * Only one null named column
         */
        try {
            ctx = new FunnelContext(Helper.config(), "--col(string)(int)");
            Assertions.fail("should have failed because of more than one unnamed column");
        } catch (ParseException e) {
            Assertions.assertEquals("Column already defined: null", e.getMessage());
        }
        ctx = new FunnelContext(Helper.config(), "--col(String)(-n myInt int)");
        Assertions.assertEquals(2, ctx.getColumnHelper().getNames().size());
        Assertions.assertNull(ctx.getColumnHelper().getNames().get(0));
        Assertions.assertEquals("myInt", ctx.getColumnHelper().getNames().get(1));
        Assertions.assertEquals(KeyType.String, ctx.getColumnHelper().get(null).getTypeName());
        Assertions.assertEquals(KeyType.Integer, ctx.getColumnHelper().get("myInt").getTypeName());
        /*
         * More verbose
         */
        ctx = new FunnelContext(Helper.config(), "--col(String) --col(-n myInt int)");
        Assertions.assertEquals(2, ctx.getColumnHelper().getNames().size());
        Assertions.assertNull(ctx.getColumnHelper().getNames().get(0));
        Assertions.assertEquals("myInt", ctx.getColumnHelper().getNames().get(1));
        Assertions.assertEquals(KeyType.String, ctx.getColumnHelper().get(null).getTypeName());
        Assertions.assertEquals(KeyType.Integer, ctx.getColumnHelper().get("myInt").getTypeName());
        /*
         * Binary Integer type
         */
        ctx = new FunnelContext(Helper.config(), "--col(bI)");
        Assertions.assertEquals(KeyType.BInteger, ctx.getColumnHelper().get(null).getTypeName());
        /*
         * Display Float type
         */
        ctx = new FunnelContext(Helper.config(), "--col(float)");
        Assertions.assertEquals(KeyType.Float, ctx.getColumnHelper().get(null).getTypeName());
        /*
         * Binary Float type
         */
        ctx = new FunnelContext(Helper.config(), "--col(bfloat)");
        Assertions.assertEquals(KeyType.BFloat, ctx.getColumnHelper().get(null).getTypeName());
        /*
         * Date
         */
        ctx = new FunnelContext(Helper.config(), "--col(Date)");
        Assertions.assertEquals(KeyType.Date, ctx.getColumnHelper().get(null).getTypeName());
        /*
         * csvIn field
         */
        ctx = new FunnelContext(Helper.config(), "--col(String -f1)");
        Assertions.assertEquals(0, ctx.getColumnHelper().get(null).getCsvFieldNumber());
        /*
         * offset
         */
        ctx = new FunnelContext(Helper.config(), "--col(String -o10)");
        Assertions.assertEquals(10, ctx.getColumnHelper().get(null).getOffset());
        Assertions.assertEquals(255, ctx.getColumnHelper().get(null).getLength());
        /*
         * offset
         */
        ctx = new FunnelContext(Helper.config(), "--col(String -l5)");
        Assertions.assertEquals(5, ctx.getColumnHelper().get(null).getLength());
        /*
         * offset
         */
        ctx = new FunnelContext(Helper.config(), "--col(Date -d'yyyyMM')");
        Assertions.assertEquals("yyyyMM", ctx.getColumnHelper().get(null).getParseFormat());
    }

    @Test
    public void defineInputFile() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        /*
         * This will change if the number of class files changes.
         */
        ctx = new FunnelContext(Helper.config(), "**/main/**/sort/*.java");
        Assertions.assertEquals(7, ctx.getInputFiles().files().size());
        /*
         * This will change if the number of class files changes.
         */
        ctx = new FunnelContext(Helper.config(), "**/main/**/sort/*.java", "**/main/**/segment/*.java");
        Assertions.assertEquals(13, ctx.getInputFiles().files().size());
    }

    @Test
    public void version() {
        Assertions.assertEquals("JUNIT.TESTING", Helper.config().version);
    }
}
