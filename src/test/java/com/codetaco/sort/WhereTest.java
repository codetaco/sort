package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class WhereTest {

    @Test
    public void badDataOnFirstRow()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("07/09/2010 10:59:47 07/09/2010 00:00:00 0080                                    "
                  + "10B0000001023080400000QQO       Tumber Hull L lc            1519     M0000033333");

        File file = Helper.createUnsortedFile(testName, in1, false);

        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file)
                                            + " --fixedIn 80"
                                            + " --columns"
                                            + " (-n typeCode         integer --offset 0   --length 1)"
                                            + " (-n typeStatus       String  --offset 1   --length 1)"
                                            + " (-n typeInitial      String  --offset 2   --length 1)"
                                            + " (-n lastModifiedTime integer --offset 3   --length 19)"
                                            + " (-n initials         String  --offset 22  --length 10)"
                                            + " (-n memberName       String  --offset 32  --length 28)"
                                            + " (-n accountType      String  --offset 60  --length 1)"
                                            + " (-n clearFirm        String  --offset 63  --length 3)"
                                            + " (-n mailBox          String  --offset 67  --length 4)"
                                            + " (-n recordType       String  --offset 68  --length 1)"
                                            + " (-n role             String  --offset 60  --length 1)"
                                            + " (-n membershipKey    Integer --offset 70  --length 10)"
                                            + " --where \"rtrim(initials) = 'QQO'\""
                                            + " -r ");

        Assertions.assertEquals(1L, context.getWriteCount());

        List<String> exp = new ArrayList<>();
        exp.add("10B0000001023080400000QQO       Tumber Hull L lc            1519     M0000033333");
        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void badOrderByName() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(), "*"
                                         + " --col(int -o0 -l5 -n field1)"
                                         + " --col(int -o5 -l5 -n field2)"
                                         + " --where '(field1 != field2)'"
                                         + " --orderby(field1)(field3)");
            Assertions.fail("should have failed");
        } catch (SortException e) {
            Assertions.assertEquals("OrderBy must be a defined column: field3", e.getMessage());
        }
    }

    @Test
    public void columnDefinedWithinKey() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 99999; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --where '(zipcode >= 50100 && zipcode <= 50200)'"
                                                             + " --col(-n zipCode int -o0 -l5)"
                                                             + "--orderby(zipcode desc)"
                                                             + " -r ");

        Assertions.assertEquals(2L, context.getWriteCount());
        List<String> exp = new ArrayList<>();
        exp.add("50200");
        exp.add("50100");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void fixedLength() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 99999; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);
        int rowSize = System.lineSeparator().length() + 5;

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --fixedIn " + rowSize
                                                             + " --where 'zipcode = 50100'"
                                                             + " --col(-n zipCode int -o0 -l5)"
                                                             + " --orderby(zipcode asc)"
                                                             + " -r ");

        Assertions.assertEquals(1L, context.getWriteCount());
        List<String> exp = new ArrayList<>();
        exp.add("50100");
        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void fixedLengthSelectionOf2() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 99999; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);
        int rowSize = System.lineSeparator().length() + 5;

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --fixedIn " + rowSize + " --variableOut CR LF"
                                                             + " --where '(zipcode >= 50100 && zipcode <= 50200)'"
                                                             + " --col(-n zipCode int -o0 -l5)"
                                                             + " --formatOut(zipcode)"
                                                             + "--orderby(zipcode desc)"
                                                             + " -r ");

        Assertions.assertEquals(2L, context.getWriteCount());
        List<String> exp = new ArrayList<>();
        exp.add("50200");
        exp.add("50100");
        Helper.compare(file, exp);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void multiWhere() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 99999; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --col(int -o0 -l5 -n zipCode)"
                                                             + " --where 'zipcode > 50000' 'zipcode < 60000'"
                                                             + " --orderby(zipCode asc)"
                                                             + " -r ");

        Assertions.assertEquals(99L, context.getWriteCount());

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void oneColumnWhere() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 99999; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --col(int -o0 -l5 -n zipCode)"
                                                             + " --where 'zipcode = 50100'"
                                                             + " --orderby(zipcode asc)"
                                                             + " -r ");

        Assertions.assertEquals(1L, context.getWriteCount());
        List<String> exp = new ArrayList<>();
        exp.add("50100");
        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void selectOddNumberedRows() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 99999; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --where 'recordnumber % 2 = 1'"
                                                             + " -r ");

        Assertions.assertEquals(450L, context.getWriteCount());

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortOnColumn() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 99999; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --col(int -o0 -l5 -n zipCode)"
                                                             + " --where '(zipcode > 50000 && zipcode < 60000)'"
                                                             + " --orderby(zipCode asc)"
                                                             + " -r ");

        Assertions.assertEquals(99L, context.getWriteCount());

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void whereIntEqualTo() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 20000; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " --col(-n zipCode int -o0 -l5)"
                                                             + " --where 'zipcode = 10100'"
                                                             + " -r ");

        Assertions.assertEquals(1L, context.getWriteCount());

        Assertions.assertTrue(file.delete());
    }
}
