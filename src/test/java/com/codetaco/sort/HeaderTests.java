package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class HeaderTests {

    @Test
    public void addHeaderToFile() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --replace"
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --orderBy(seq desc)"
                                                                   + " --headerOut(-e'todate(\"20160609\", \"yyyyMMdd\")' -l 28 -d '%tF')");

        Assertions.assertEquals(2L, context.getWriteCount());

        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("2016-06-09                  ");
        expectedLines.add("TEST20160608 2");
        expectedLines.add("TEST20160608 1");
        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void consumeHeader() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --headerIn"
                                                                   + "      (date   -l8 -n RUNDATE -d'yyyyMMdd')"
                                                                   + "      (string -l4 -n RUNTYPE)"
                                                                   + " --headerOut()");

        Assertions.assertEquals(2L, context.getWriteCount());

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void duplicateNameWithColumn() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(), ""
                                         + " --col(string -l4 -n TYPE)"
                                         + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                         + "      (int    -l2 -n SEQ)"
                                         + " --headerIn"
                                         + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                         + "      (string -l4 -n TYPE)");
            Assertions.fail("Expected exception");
        } catch (SortException e) {
            Assertions.assertEquals("columnsIn must be unique from headerIn: TYPE", e.getMessage());
        }
    }

    @Test
    public void duplicateNameWithHeader() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(), ""
                                         + " --col(string -l4 -n type)"
                                         + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                         + "      (int    -l2 -n SEQ)"
                                         + " --headerIn"
                                         + "      (date   -l8 -n TYPE -d'yyyyMMdd')"
                                         + "      (string -l4 -n TYPE)");
            Assertions.fail("Expected exception");
        } catch (SortException e) {
            Assertions.assertEquals("headerIn must be unique: TYPE", e.getMessage());
        }
    }

    @Test
    public void filler() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("06/01/2016 16:43:56           06/01/2016 00:00:00 0230");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                + " --replace"
                                                + " --headerIn "
                                                + "      (Date -n RUNTIME -d'MM/dd/yyyy HH:mm:ss')"
                                                + "      (filler -l 11)"
                                                + "      (Date -n BUSDATE -d'MM/dd/yyyy HH:mm:ss')"
                                                + "      (filler -l 1)"
                                                + "      (Int  -n LRECL -l4)"
                                                + " --headerOut(-eRUNTIME  -l 28 -d '%tF')");
        });
        Assertions.assertEquals(2L, context.getWriteCount());

        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("2016-06-01                  ");
        expectedLines.add("TEST20160608 1");
        expectedLines.add("TEST20160608 2");
        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void notValidForCSV() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(), "--csvIn()"
                                         + " --col"
                                         + "      (int    -l2 -n SEQ)"
                                         + " --headerIn"
                                         + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                         + "      (string -l4 -n TYPE)");
            Assertions.fail("Expected exception");
        } catch (SortException e) {
            Assertions.assertEquals("headerIn not supported for csvIn files", e.getMessage());
        }
    }

    @Test
    public void useHeaderForStop() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --headerIn"
                                                                   + "      (date   -l8 -n RUNDATE -d'yyyyMMdd')"
                                                                   + "      (string -l4 -n RUNTYPE)"
                                                                   + " --headerOut()"
                                                                   + " --stop 'runtype = type'");

        Assertions.assertEquals(0L, context.getWriteCount());

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void useHeaderForWhere() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --headerIn"
                                                                   + "      (date   -l8 -n RUNDATE -d'yyyyMMdd')"
                                                                   + "      (string -l4 -n RUNTYPE)"
                                                                   + " --headerOut()"
                                                                   + " --where 'RUNTYPE = TYPE'");

        Assertions.assertEquals(2L, context.getWriteCount());

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void writeEntireHeaderToOutput() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --replace"
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --orderBy(seq desc)"
                                                                   + " --headerIn"
                                                                   + "      (date   -l8 -n RUNDATE -d'yyyyMMdd')"
                                                                   + "      (string -l4 -n RUNTYPE)");

        Assertions.assertEquals(2L, context.getWriteCount());

        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("20160608TEST");
        expectedLines.add("TEST20160608 2");
        expectedLines.add("TEST20160608 1");
        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void writeHeaderColumnToDetailRecordAsCol() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(),
                                                Helper.quotedFileName(file) + " --replace"
                                                  + " --col(string -l4 -n TYPE)"
                                                  + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                  + "      (int    -l2 -n SEQ)"
                                                  + " --headerIn"
                                                  + "      (date   -l8 -n RUNDATE -d'yyyyMMdd')"
                                                  + "      (string -l4 -n RUNTYPE)"
                                                  + " --headerOut()"
                                                  + " --formatOut(TYPE)(RUNTYPE)(DATE)(RUNDATE)(SEQ)");

        Assertions.assertEquals(2L, context.getWriteCount());

        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("TESTTEST2016060820160608 1");
        expectedLines.add("TESTTEST2016060820160608 2");
        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void writeHeaderColumnToDetailRecordAsEqu() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --headerIn"
                                                                   + "      (date   -l8 -n RUNDATE -d'yyyyMMdd')"
                                                                   + "      (string -l4 -n RUNTYPE)"
                                                                   + " --headerOut()"
                                                                   + " --formatOut(TYPE)(-eRUNTYPE -l4)(DATE)(-eRUNDATE -l8 -d'%tY%1$tm%1$td')(SEQ)");

        Assertions.assertEquals(2L, context.getWriteCount());

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void writeHeaderOutWithEqu() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --replace"
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --orderBy(seq desc)"
                                                                   + " --headerIn"
                                                                   + "      (date   -l8 -n RUNDATE -d'yyyyMMdd')"
                                                                   + "      (string -l4 -n RUNTYPE)"
                                                                   + " --headerOut(-eRUNDATE -l 28 -d '%tF')");

        Assertions.assertEquals(2L, context.getWriteCount());

        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("2016-06-08                  ");
        expectedLines.add("TEST20160608 2");
        expectedLines.add("TEST20160608 1");
        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void writeMinimalHeaderToOutput() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --replace"
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --orderBy(seq desc)"
                                                                   + " --headerIn(filler)");

        Assertions.assertEquals(2L, context.getWriteCount());

        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("20160608TEST");
        expectedLines.add("TEST20160608 2");
        expectedLines.add("TEST20160608 1");
        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void writeSpecificColumnsToHeaderOutput() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("20160608TEST");
        logFile.add("TEST20160608 1");
        logFile.add("TEST20160608 2");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --replace"
                                                                   + " --col(string -l4 -n TYPE)"
                                                                   + "      (date   -l8 -n DATE -d'yyyyMMdd')"
                                                                   + "      (int    -l2 -n SEQ)"
                                                                   + " --orderBy(seq desc)"
                                                                   + " --headerIn"
                                                                   + "      (date   -l8 -n RUNDATE -d'yyyyMMdd')"
                                                                   + "      (string -l4 -n RUNTYPE)"
                                                                   + " --headerOut(RUNTYPE)(RUNDATE)");

        Assertions.assertEquals(2L, context.getWriteCount());

        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("TEST20160608");
        expectedLines.add("TEST20160608 2");
        expectedLines.add("TEST20160608 1");
        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }
}
