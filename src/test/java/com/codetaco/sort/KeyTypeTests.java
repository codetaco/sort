package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class KeyTypeTests {

    @Test
    public void sortByDollarsAndCents() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("$2,125.01");
        out.add("$2,225.01");
        out.add("$2,325.01");
        out.add("$2,425.01");
        out.add("$3,325.01");
        out.add("$3,425.01");
        out.add("$3,525.00");
        out.add("$3,525.01");
        File file = Helper.createUnsortedFile(testName, out, true);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " --col(float -o0 -n dollar) --order(dollar desc)");

        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }
}
