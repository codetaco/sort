package com.codetaco.sort;

import com.codetaco.Helper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RecordNumberTest {

    @Test
    public void aggregate()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int x = 1; x <= 5; x += 1) {
            in.add("" + x);
        }
        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("15 15");

        File file = Helper.createUnsortedFile(testName, in);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " -r -c o --col(-n myRecordNumber -l 2 int)"
                                     + " --sum (-n sum1 myRecordNumber)"
                                     + " --sum (-n sum2 -e'recordnumber')"
                                     + " --formatOut(-esum1  -l 3 -d '%2d')(-esum2  -l 3 -d '%2d')");

        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void multiFileInput()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int x = 1; x <= 5; x += 1) {
            in.add("" + x);
        }

        List<String> in2 = new ArrayList<>();
        for (int x = 6; x <= 9; x += 1) {
            in2.add("" + x);
        }
        List<String> expectedLines = new ArrayList<>();
        for (int x = 1; x <= 9; x += 1) {
            expectedLines.add("" + x + " " + x);
        }

        File file = Helper.createUnsortedFile(testName, in);
        File file2 = Helper.createUnsortedFile(testName, in2);
        File fileOut = Helper.outFile(testName);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " " + Helper.quotedFileName(file2) + " --noc "
                                     + " --out " + Helper.quotedFileName(fileOut)
                                     + " --col(-n myRecordNumber -l 2 int) --orderBy (myRecordNumber)"
                                     + " --formatOut(-erecordNumber -l 2)(myRecordNumber)");

        Helper.compare(fileOut, expectedLines);
        Assertions.assertTrue(file.delete());
        Assertions.assertTrue(file2.delete());
        Assertions.assertTrue(fileOut.delete());
    }

    @Test
    public void normalFixed()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int x = 1; x <= 5; x += 1) {
            in.add("" + x);
        }
        List<String> expectedLines = new ArrayList<>();
        for (int x = 1; x <= 5; x += 1) {
            expectedLines.add("" + x + " " + x);
        }

        File file = Helper.createUnsortedFile(testName, in);
        int rowSize = System.lineSeparator().length() + 1;

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " -r -c o --col(-n myRecordNumber -l 2 int) --fixedIn " + rowSize
                                     + " --formatOut(-erecordNumber -l 2)(myRecordNumber)");

        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void normalVariable()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int x = 1; x <= 5; x += 1) {
            in.add("" + x);
        }
        List<String> expectedLines = new ArrayList<>();
        for (int x = 1; x <= 5; x += 1) {
            expectedLines.add("" + x + " " + x);
        }

        File file = Helper.createUnsortedFile(testName, in);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " -r -c o --col(-n myRecordNumber -l 2 int)"
                                     + " --formatOut(-erecordNumber -l 2)(myRecordNumber)");

        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void skip1Row()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int x = 1; x <= 5; x += 1) {
            in.add("" + x);
        }
        List<String> expectedLines = new ArrayList<>();
        for (int x = 2; x <= 5; x += 1) {
            expectedLines.add("" + x + " " + x);
        }

        File file = Helper.createUnsortedFile(testName, in);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " -r -c o --col(-n myRecordNumber -l 2 int)"
                                     + " --where 'recordNumber > 1'"
                                     + " --formatOut(-erecordNumber -l 2)(myRecordNumber)");

        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void stopWhen()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int x = 1; x <= 5; x += 1) {
            in.add("" + x);
        }
        List<String> expectedLines = new ArrayList<>();
        for (int x = 1; x <= 2; x += 1) {
            expectedLines.add("" + x + " " + x);
        }

        File file = Helper.createUnsortedFile(testName, in);

        Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                     + " -r -c o --col(-n myRecordNumber -l 2 int)"
                                     + " --stopWhen 'recordNumber = 3'"
                                     + " --formatOut(-erecordNumber -l 2)(myRecordNumber)");

        Helper.compare(file, expectedLines);
        Assertions.assertTrue(file.delete());
    }
}
