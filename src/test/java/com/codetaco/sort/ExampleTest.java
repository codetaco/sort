package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExampleTest {

    private void runExample(String specFilename,
                            long recordCount,
                            long writeCount,
                            long duplicateCount) {
        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), "@" + specFilename + ".fun");
        });
        Assertions.assertEquals(recordCount, context.getRecordCount());
        Assertions.assertEquals(writeCount, context.getWriteCount());
        Assertions.assertEquals(duplicateCount, context.getDuplicateCount());
    }

    @Test
    public void example01() {
        runExample("addHeader", 52, 52, 0);
    }

    @Test
    public void example02() {
        runExample("aggregates", 52, 6, 0);
    }

    @Test
    public void example03() {
        runExample("averageRowsPerSecond", 23, 1, 0);
    }

    @Test
    public void example04() {
        runExample("convertFixedToVariable", 52, 52, 0);
    }

    @Test
    public void example05() {
        runExample("convertVariableToFixed", 52, 52, 0);
    }

    @Test
    public void example06() {
        runExample("copyCollate", 104, 104, 0);
    }

    @Test
    public void example07() {
        runExample("copyOriginal", 52, 52, 0);
    }

    @Test
    public void example08() {
        runExample("copyReverse", 52, 52, 0);
    }

    @Test
    public void example09() {
        runExample("countJobs", 23, 1, 0);
    }

    @Test
    public void example10() {
        runExample("csvSort", 53, 52, 0);
    }

    @Test
    public void example11() {
        runExample("dos2unix", 23, 23, 0);
    }

    @Test
    public void example12() {
        runExample("dupFirstOnly", 23, 2, 21);
    }

    @Test
    public void example13() {
        runExample("eolWord", 23, 23, 0);
    }

    @Test
    public void example14() {
        runExample("formatComputedColumn", 52, 52, 0);
    }

    @Test
    public void example15() {
        runExample("formatDate", 23, 23, 0);
    }

    @Test
    public void example16() {
        runExample("formatFiller", 52, 52, 0);
    }

    @Test
    public void example17() {
        runExample("formatFormatNumber", 52, 52, 0);
    }

    @Test
    public void example18() {
        runExample("formatTwoColumns", 52, 52, 0);
    }

    @Test
    public void example19() {
        runExample("hexDumpColumns", 52, 7, 0);
    }

    @Test
    public void example20() {
        runExample("hexDumpRow", 52, 7, 0);
    }

    @Test
    public void example21() {
        runExample("multiKey", 52, 52, 0);
    }

    @Test
    public void example22() {
        runExample("oneKey", 28, 28, 0);
    }

    @Test
    public void example23() {
        runExample("orderByAbsInt", 52, 52, 0);
    }

    @Test
    public void example24() {
        runExample("orderByDescDate", 23, 23, 8);
    }

    @Test
    public void example25() {
        runExample("orderByFloat", 52, 52, 5);
    }

    @Test
    public void example26() {
        runExample("orderByInt", 52, 52, 0);
    }

    @Test
    public void example27() {
        runExample("orderWithHeader", 52, 52, 0);
    }

    @Test
    public void example28() {
        runExample("removeHeader", 52, 52, 0);
    }

    @Test
    public void example29() {
        runExample("sortMultipleFiles", 58, 58, 6);
    }

    @Test
    public void example30() {
        runExample("sortSingleFile", 52, 52, 0);
    }

    @Test
    public void example31() {
        runExample("sortWildFile", 58, 58, 6);
    }

    @Test
    public void example32() {
        runExample("stopAtRecordNumber", 10, 10, 0);
    }

    @Test
    public void example33() {
        runExample("stopAtTimestamp", 19, 19, 0);
    }

    @Test
    public void example34() {
        runExample("upperCase", 23, 23, 0);
    }

    @Test
    public void example35() {
        runExample("wherePattern", 23, 4, 0);
    }

    @Test
    public void example36() {
        runExample("whereRange", 52, 8, 0);
    }
}
