package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.orderby.DateKey;
import com.codetaco.sort.orderby.DisplayFloatKey;
import com.codetaco.sort.orderby.DisplayIntKey;
import com.codetaco.sort.orderby.KeyContext;
import com.codetaco.sort.orderby.KeyDirection;
import com.codetaco.sort.orderby.KeyPart;
import com.codetaco.sort.orderby.KeyType;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FormatTests {
    static private void verifyFormatted(byte[] bb, KeyContext kx) {
        for (int b = 0; b < bb.length; b++) {
            Assertions.assertEquals(bb[b], kx.getKey()[b]);
        }
    }

    @Test
    public void dateFormatInvalid() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        KeyPart key = KeyType.create(KeyType.Date.name());

        key.setOffset(0);
        key.setLength(20);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("yyyy-MM-dd");

        KeyContext kx = Helper.dummyKeyContext(" 1960-04-a");
        try {
            key.pack(kx);
            verifyFormatted(new byte[]{
              -128,
              0,
              0,
              0,
              0,
              0,
              0,
              0
            }, kx);
            Assertions.fail("ParseException was expected");
        } catch (Exception e) {
            Assertions.assertEquals("Unparseable date: \"1960-04-a\"", e.getMessage());
        }

    }

    /**
     * I think this test must be contingent on the date locality or something like that. It works on windows but fails
     * on
     * the Jenkins CI.
     *
     * @throws java.lang.Throwable if any.
     */
    // @Test
    public void dateFormatTrimLeft() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DateKey key = new DateKey();

        key.setOffset(0);
        key.setLength(20);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("yyyy-MM-dd");

        KeyContext kx = Helper.dummyKeyContext(" 1960-04-09");
        key.pack(kx);
        verifyFormatted(new byte[]{
          127,
          -1,
          -1,
          -72,
          -126,
          -64,
          95,
          0
        }, kx);
    }

    @Test
    public void floatFormatTrimRight() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayFloatKey key = new DisplayFloatKey();

        key.setOffset(0);
        key.setLength(8);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####.###");

        KeyContext kx = Helper.dummyKeyContext("5.1  ");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -64,
          20,
          102,
          102,
          102,
          102,
          102,
          102
        }, kx);
    }

    @Test
    public void floatNegDollarFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayFloatKey key = new DisplayFloatKey();

        key.setOffset(0);
        key.setLength(12);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####.##");

        KeyContext kx = Helper.dummyKeyContext("$5,000.10-");
        key.pack(kx);
        verifyFormatted(new byte[]{
          63,
          76,
          119,
          -26,
          102,
          102,
          102,
          101
        }, kx);
    }

    @Test
    public void floatPosDollarFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayFloatKey key = new DisplayFloatKey();

        key.setOffset(0);
        key.setLength(8);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####.##");

        KeyContext kx = Helper.dummyKeyContext("$5,000.10");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -64,
          -77,
          -120,
          25,
          -103,
          -103,
          -103,
          -102
        }, kx);
    }

    @Test
    public void integerFormatTrimLeft() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext(" 5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -128,
          0,
          0,
          0,
          0,
          0,
          0,
          5
        }, kx);
    }

    @Test
    public void integerFormatTrimRight() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("5   ");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -128,
          0,
          0,
          0,
          0,
          0,
          0,
          5
        }, kx);
    }

    @Test
    public void integerNegDollarFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(5);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("$-5,0");
        key.pack(kx);
        verifyFormatted(new byte[]{
          127,
          -1,
          -1,
          -1,
          -1,
          -1,
          -1,
          -50
        }, kx);
    }

    @Test
    public void integerNegFormatAASC() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.AASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("-5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -128,
          0,
          0,
          0,
          0,
          0,
          0,
          5
        }, kx);
    }

    @Test
    public void integerNegFormatADESC() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ADESC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("-5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          127,
          -1,
          -1,
          -1,
          -1,
          -1,
          -1,
          -5
        }, kx);
    }

    @Test
    public void integerNegFormatASC() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("-5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          127,
          -1,
          -1,
          -1,
          -1,
          -1,
          -1,
          -5
        }, kx);
    }

    @Test
    public void integerNegFormatDESC() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.DESC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("-5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -128,
          0,
          0,
          0,
          0,
          0,
          0,
          5
        }, kx);
    }

    @Test
    public void integerNegLeftFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext(" -5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          127,
          -1,
          -1,
          -1,
          -1,
          -1,
          -1,
          -5
        }, kx);
    }

    @Test
    public void integerNegRightFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext(" 5-");
        key.pack(kx);
        verifyFormatted(new byte[]{
          127,
          -1,
          -1,
          -1,
          -1,
          -1,
          -1,
          -5
        }, kx);
    }

    @Test
    public void integerPosDollarFormat() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("$5,0");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -128,
          0,
          0,
          0,
          0,
          0,
          0,
          50
        }, kx);
    }

    @Test
    public void integerPosFormatAASC() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.AASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -128,
          0,
          0,
          0,
          0,
          0,
          0,
          5
        }, kx);
    }

    @Test
    public void integerPosFormatADESC() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ADESC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          127,
          -1,
          -1,
          -1,
          -1,
          -1,
          -1,
          -5
        }, kx);
    }

    @Test
    public void integerPosFormatASC() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.ASC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          -128,
          0,
          0,
          0,
          0,
          0,
          0,
          5
        }, kx);
    }

    @Test
    public void integerPosFormatDESC() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        DisplayIntKey key = new DisplayIntKey();

        key.setOffset(0);
        key.setLength(4);
        key.setDirection(KeyDirection.DESC);
        key.setParseFormat("####");

        KeyContext kx = Helper.dummyKeyContext("5");
        key.pack(kx);
        verifyFormatted(new byte[]{
          127,
          -1,
          -1,
          -1,
          -1,
          -1,
          -1,
          -5
        }, kx);
    }

    @Test
    public void lengthMaxString() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        in.add("row 1000");
        File file = Helper.createUnsortedFile(testName, in);

        int lineSize = 8 + System.lineSeparator().length();

        FunnelContext context = Sort
                                  .sort(Helper.config(), Helper.quotedFileName(file)
                                                           + " -r --row 130 --fixedIn " + lineSize + " "
                                                           + "--col(-n col1 string -o0)"
                                                           + "--orderby(col1)");
        Assertions.assertEquals(255, context.getKeys().get(0).getLength());
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void lengthOverride() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        in.add("row 1000");
        File file = Helper.createUnsortedFile(testName, in);

        int lineSize = 8 + System.lineSeparator().length();

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " -r --row 130 --fixedIn " + lineSize + " "
                                                             + " --col(--name col1 integer -o4 -l4 --format '###')"
                                                             + " --orderby(col1)");
        Assertions.assertEquals(4, context.getKeys().get(0).getLength());
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void lengthUnspecified() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        in.add("row 1000");
        File file = Helper.createUnsortedFile(testName, in);

        int lineSize = 8 + System.lineSeparator().length();

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " -r --row 130 --fixedIn " + lineSize + " "
                                                             + " --col(-n col1 integer -o4 --format '###')"
                                                             + "--orderby(col1)");
        Assertions.assertEquals(3, context.getKeys().get(0).getLength());
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void misspelledCol() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        try {
            Sort.sort(Helper.config(), " --col(-n col1 string) --formatOut(colone)");
            Assertions.fail("Expected an exception");
        } catch (SortException e) {
            Assertions.assertEquals("--formatOut must be a defined column or header: colone", e.getMessage());
        }
    }

    @Test
    public void misspelledEqu() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        List<String> in = new ArrayList<>();
        in.add("row 1000");
        File file = Helper.createUnsortedFile(testName, in);
        try {
            Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                         + " --col(-n col1 string) --formatOut(-ecolone -l1 -d '%03d')");
            Assertions.fail("Expected an exception");
        } catch (Exception e) {
            Assertions.assertEquals("unresolved variable: \"colone\": --format(colone)", e.getMessage());
        }
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void offsetDefault() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 130; r++) {
            in.add("row " + (r + 1000));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                             + " -r --row 130 --fixedIn 10"
                                                             + " --col(-n col1 string)"
                                                             + " --orderby(col1)");
        Assertions.assertEquals(0, context.getKeys().get(0).getOffset());

        context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                               + " -r --row 130 --fixedIn 10"
                                               + " --col(-n col1 -o1 string)"
                                               + " --orderby(col1)");
        Assertions.assertEquals(1, context.getKeys().get(0).getOffset());
        Assertions.assertTrue(file.delete());
    }
}
