package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SortTest {
    @Test
    public void copyOriginalFixed130() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 130; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 0; r < 130; r++) {
            out.add(in.get(r));
        }

        int rowSize = System.lineSeparator().length() + 8;

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 130 -c original --fixedIn " + rowSize);

        Assertions.assertEquals(130L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void copyOriginalFixed50() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 50; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 0; r < 50; r++) {
            out.add(in.get(r));
        }

        int rowSize = System.lineSeparator().length() + 8;

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 50 -c original --fixedIn " + rowSize);

        Assertions.assertEquals(50L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void copyOriginalVar1000() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 1000; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 0; r < 1000; r++) {
            out.add(in.get(r));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 1000 -c original");

        Assertions.assertEquals(1000L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void copyOriginalVar130() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 130; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 0; r < 130; r++) {
            out.add(in.get(r));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 130 -c original");

        Assertions.assertEquals(130L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void copyOriginalVar50() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 50; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 0; r < 50; r++) {
            out.add(in.get(r));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 50 -c original");

        Assertions.assertEquals(50L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void copyReverseFixed() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 50; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 49; r >= 0; r--) {
            out.add(in.get(r));
        }

        int rowSize = System.lineSeparator().length() + 8;

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 50 -c reverse --fixedIn " + rowSize);

        Assertions.assertEquals(50L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void copyReverseVar() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 50; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 49; r >= 0; r--) {
            out.add(in.get(r));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 50 -c reverse");

        Assertions.assertEquals(50L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortFixed1000() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 1000; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 999; r >= 0; r--) {
            out.add(in.get(r));
        }
        int rowSize = System.lineSeparator().length() + 8;

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --row 1000 --fixedIn " + rowSize
                                            + "--col(-nc Integer -o4 -l4 )"
                                            + "--order(c desc)");

        Assertions.assertEquals(1000L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortFullKeyVariable1000Asc() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 1000; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 0; r < 1000; r++) {
            out.add(in.get(r));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --power 4");

        Assertions.assertEquals(1000L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortIntVar1000() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 1000; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 999; r >= 0; r--) {
            out.add(in.get(r));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --power 4"
                                            + "--col(-nc Integer -o4 -l4)"
                                            + "--orderby(c desc)");

        Assertions.assertEquals(1000L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortStringVar1000() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 1000; r++) {
            in.add("row " + (r + 1000));
        }
        List<String> out = new ArrayList<>();
        for (int r = 999; r >= 0; r--) {
            out.add(in.get(r));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file)
                                            + " -r --power 4 "
                                            + "--col(-nn String -o4 -l4)"
                                            + "--order(n desc)");
        Assertions.assertEquals(1000L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortVarP2() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = new ArrayList<>();
        for (int r = 0; r < 10; r++) {
            in.add("row " + (r + 10));
        }
        List<String> out = new ArrayList<>();
        for (int r = 9; r >= 0; r--) {
            out.add(in.get(r));
        }

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --power 2"
                                            + "--col(-ncol1 Integer,-o4,-l2)"
                                            + "--ord(col1 desc) ");

        Assertions.assertEquals(10L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void unexpectedInput() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(), "--ThisIsNotAValueArgument");
        } catch (SortException pe) {
            Assertions.assertEquals("unexpected input: --ThisIsNotAValueArgument ", pe.getCause().getMessage());
        }
    }

    @Test
    public void usageMessage() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(), "--usage");
        } catch (SortException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void version() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        Sort.sort(Helper.config(), "--version");
    }
//
//    @Test
//    public void help() {
//        String testName = Helper.testName();
//        Helper.initializeFor(testName);
//        Sort.sort(Helper.config(), "--help");
//    }
}
