package com.codetaco.sort;

import com.codetaco.Helper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ZoneIdTests {

    @Test
    public void showZoneIds() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "ZoneId.json",
          () -> Sort.sort(Helper.config(),
                          "--showZoneIds Chi"));
        List<String> zoneIds = new ArrayList<>();
        zoneIds.add("Asia/Kuching");
        zoneIds.add("America/Chicago");
        zoneIds.add("Chile/Continental");
        zoneIds.add("Asia/Karachi");
        zoneIds.add("Asia/Chita");
        zoneIds.add("US/Michigan");
        zoneIds.add("Europe/Chisinau");
        zoneIds.add("Asia/Ho_Chi_Minh");
        zoneIds.add("America/Chihuahua");
        zoneIds.add("Chile/EasterIsland");
        Helper.compare(outfile, zoneIds);
    }

    @Test
    public void sortZoned() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "ZoneId.json",
          () -> Sort.sort(Helper.config(),
                          "--zoneId Asia/Karachi "
                            + "--jsonin(--path '$[*]')"
                            + "--col(datetime, --path withZone -n withZone)"
                            + "--col(datetime, --path withoutZone -n withoutZone)"
                            + "--orderBy (withZone)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{withZone:\"2018-10-21T10:02:00.123-05:00\",");
        expectedResults.append("withoutZone:\"2018-10-21T10:02:00.123\"}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }
}
