package com.codetaco.sort;

import com.codetaco.Helper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FieldTests {

    @Test
    public void dupsColumnName() {
        try {
            Helper.captureSysoutWhileRunning(
              () -> Sort.sort(Helper.config(),
                              " --col(string -n logLine)"
                                + " --field (-n logLine -e \"toUpper(logLine)\")")
            );
            Assertions.fail("expecting an exception");
        } catch (Exception e) {
            Assertions.assertEquals("formatIn \"logLine\" is already defined as a column", e.getMessage());
        }
    }

    @Test
    public void dupsAnotherField() {
        try {
            Helper.captureSysoutWhileRunning(
              () -> Sort.sort(Helper.config(),
                              " --field (-n logLine -e \"toUpper(logLine)\")"
                                + "(-n logLine -e \"toUpper(logLine)\")")
            );
            Assertions.fail("expecting an exception");
        } catch (Exception e) {
            Assertions.assertEquals("formatIn \"logLine\" must have a unique name", e.getMessage());
        }
    }

    @Test
    public void where() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("  FATAL");
        logFile.add("INFO");
        logFile.add(" WARN");

        List<String> out = new ArrayList<>();
        out.add("  FATAL");

        File file = Helper.createUnsortedFile(testName, logFile);
        try {
            Helper.captureSysoutWhileRunning(
              () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                 + " --replace "
                                                 + " --col(string -n logLine)"
                                                 + " --field (-n severity -e 'match(logLine, \"^[ ]*([A-Z])\", 1)')"
                                                 + " --where \"(severity = 'F')\"")
            );
            Helper.compare(file, out, true);
        } finally {
            Assertions.assertTrue(file.delete());
        }
    }

    @Test
    public void formatOut() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("  FATAL");
        logFile.add("INFO");
        logFile.add(" WARN");

        List<String> out = new ArrayList<>();
        out.add("A");
        out.add("R");
        out.add("O");

        File file = Helper.createUnsortedFile(testName, logFile);
        try {
            Helper.captureSysoutWhileRunning(
              () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                 + " --replace "
                                                 + " --col(string -n logLine)"
                                                 + " --field (-n severity -e 'substr(logLine, 3, 1)')"
                                                 + " --fOut (severity)"));
            Helper.compare(file, out, true);
        } finally {
            Assertions.assertTrue(file.delete());
        }
    }

    @Test
    public void orderBy() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("  FATAL");
        logFile.add("INFO");
        logFile.add(" WARN");

        List<String> out = new ArrayList<>();
        out.add(" WARN");
        out.add("INFO");
        out.add("  FATAL");

        File file = Helper.createUnsortedFile(testName, logFile);
        try {
            Helper.captureSysoutWhileRunning(
              () -> Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                 + " --replace "
                                                 + " --col(string -n logLine)"
                                                 + " --field (-n severity -e 'match(logLine, \"^[ ]*([A-Z])\", 1)')"
                                                 + " --orderby (severity desc)"));
            Helper.compare(file, out, true);
        } finally {
            Assertions.assertTrue(file.delete());
        }
    }
}
