package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.variable.csv.CsvProvider;
import org.apache.commons.csv.CSVFormat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class CsvTest {

    static private String csvColumns = " --col(Int --field 1 -n Number)(String --field 2 -n A)(String --field 3 -n B) ";

    static private List<String> csvInput() {
        List<String> in = new ArrayList<>();
        in.add("1,a,b");
        in.add("2,a,bb");
        in.add("3,a,bbb");
        in.add("4,a,bbbb");
        in.add("5,a,bbbbb");
        in.add("6,a,b");
        in.add("7,aa,b");
        in.add("8,aaa,b");
        in.add("9,aaaa,b");
        in.add("10,aaaaa,b");
        return in;
    }

    @Test
    public void csvAllDefault() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("abc,4");
        out.add("def,3");

        File infile = Helper.createUnsortedFile(testName, out);

        File file = Helper.outFileWhenInIsSysin();
        PrintStream outputStream = new PrintStream(new FileOutputStream(file));
        System.setOut(outputStream);

        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(infile) + " --csvIn() --row 2 "
                                            + "--col(S, --fi 1 -n field1)(I --fi 2 -n field2) "
                                            + "--formatOut(field1)(field2)"
                                            + "--orderBy (field1)(field2)");

        Assertions.assertEquals(2L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvFile() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("def,3");
        out.add("abc,5");

        File infile = Helper.createUnsortedFile(testName, out);

        File file = Helper.outFileWhenInIsSysin();
        PrintStream outputStream = new PrintStream(new FileOutputStream(file));
        System.setOut(outputStream);

        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(infile)
                                            + " --csvIn() "
                                            + "--col(String -f1 -nA)(Int -f2 -nB)"
                                            + "--formatOut(A)(B)"
                                            + "--orderBy(A desc) --row 2 ");

        Assertions.assertEquals(2L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void noFormatOut() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("def,3");
        out.add("abc,5");

        File infile = Helper.createUnsortedFile(testName, out);

        File file = Helper.outFileWhenInIsSysin();
        PrintStream outputStream = new PrintStream(new FileOutputStream(file));
        System.setOut(outputStream);

        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(infile)
                                            + " --csvIn() "
                                            + "--col(String -f1 -nA)(Int -f2 -nB)"
                                            + "--orderBy(A desc) --row 2 ");

        Assertions.assertEquals(2L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvFileKey2() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("abc,5");
        out.add("def,3");

        File infile = Helper.createUnsortedFile(testName, out);

        File file = Helper.outFileWhenInIsSysin();
        PrintStream outputStream = new PrintStream(new FileOutputStream(file));
        System.setOut(outputStream);

        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(infile)
                                            + " --csvIn()"
                                            + "--col(String -f1 -nA)(Int -f2 -nB)"
                                            + "--formatOut(A)(B)"
                                            + "--orderBy(B desc) --row 2 ");

        Assertions.assertEquals(2L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvInAndOut() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "CsvTest.csv",
          () -> Sort.sort(Helper.config(),
                          "--csvIn(-h) --csvOut(-h)"
                            + "--col(String -f1 -nlastname)(String -f2 -nfirstname)"
                            + "--formatOut(firstname)(lastname)"
                            + "--orderBy (firstname)"));
        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("firstname,lastname");
        expectedResults.add("Albert,Irwin");
        expectedResults.add("Carey,Cobb");
        expectedResults.add("Edwina,Neal");
        expectedResults.add("Garrett,Dudley");
        expectedResults.add("Harrell,Holder");
        expectedResults.add("Maggie,Carrillo");
        expectedResults.add("Stout,Delacruz");
        Helper.compare(outfile, expectedResults);
    }

    @Test
    public void jsonOut() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "CsvTest.csv",
          () -> Sort.sort(Helper.config(),
                          "--csvIn(-h) --jsonOut()"
                            + "--col(String -f1 -nlastname )(String -f2 -nfirstname )"
                            + "--formatOut(firstname)(lastname)"
                            + "--orderBy (firstname)"));
        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("[{\"firstname\":\"Albert\",\"lastname\":\"Irwin\"},{\"firstname\":\"Carey\",\"lastname\":\"Cobb\"},{\"firstname\":\"Edwina\",\"lastname\":\"Neal\"},{\"firstname\":\"Garrett\",\"lastname\":\"Dudley\"},{\"firstname\":\"Harrell\",\"lastname\":\"Holder\"},{\"firstname\":\"Maggie\",\"lastname\":\"Carrillo\"},{\"firstname\":\"Stout\",\"lastname\":\"Delacruz\"}]");
        Helper.compare(outfile, expectedResults);
    }

    @Test
    public void varOut() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "CsvTest.csv",
          () -> Sort.sort(Helper.config(),
                          "--csvIn(-h) --VO cr,lf"
                            + "--col(String -f1 -nlastname )(String -f2 -nfirstname )"
                            + "--formatOut(firstname -l9 --format '%9s')(--equ '\" | \"' -l 3)(lastname)"
                            + "--orderBy (firstname)"));
        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("   Albert | Irwin");
        expectedResults.add("    Carey | Cobb");
        expectedResults.add("   Edwina | Neal");
        expectedResults.add("  Garrett | Dudley");
        expectedResults.add("  Harrell | Holder");
        expectedResults.add("   Maggie | Carrillo");
        expectedResults.add("    Stout | Delacruz");
        Helper.compare(outfile, expectedResults);
    }

    @Test
    public void fixedOut() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "CsvTest.csv",
          () -> Sort.sort(Helper.config(),
                          "--csvIn(-h) --fixedOut 40"
                            + "--col(String -f1 -nlastname)(String -f2 -nfirstname)"
                            + "--formatOut(firstname -l8)(lastname -l9)"
                            + "--orderBy (firstname)"));
        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("Albert  Irwin    ");
        expectedResults.add("Carey   Cobb     ");
        expectedResults.add("Edwina  Neal     ");
        expectedResults.add("Garrett Dudley   ");
        expectedResults.add("Harrell Holder   ");
        expectedResults.add("Maggie  Carrillo ");
        expectedResults.add("Stout   Delacruz ");
        Helper.compareFixed(outfile, expectedResults);
    }

    @Test
    public void csvHeader() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> out = new ArrayList<>();
        out.add("letters,numbers");
        out.add("def,3");
        out.add("abc,5");

        File infile = Helper.createUnsortedFile(testName, out);

        File file = Helper.outFileWhenInIsSysin();
        PrintStream outputStream = new PrintStream(new FileOutputStream(file));
        System.setOut(outputStream);

        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(infile)
                                            + " --csvIn(-h) "
                                            + "--col(String -f1 -nletters)(Int -f2 -nnumbers) "
                                            + "--formatOut(letters)(numbers)"
                                            + "--orderBy(numbers) --row 2 ");

        Assertions.assertEquals(3L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvParserCommentMarker() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();
        File file = Helper.createUnsortedFile(testName, in);

        Sort.sort(Helper.config(), Helper.quotedFileName(file) + " -r --csvIn(--commentMarker '#') "
                                     + csvColumns
                                     + "--formatOut(Number)(A)(B)"
                                     + "--orderBy(Number desc)");

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvParserDelimiter() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        File file = Helper.createUnsortedFile(testName, in);
        Sort.sort(Helper.config(), Helper.quotedFileName(file) + " -r --csvIn(--delimiter ',') "
                                     + csvColumns
                                     + "--formatOut(Number)(A)(B)"
                                     + "--orderBy(Number desc)");

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvParserEscape() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        File file = Helper.createUnsortedFile(testName, in);
        Sort.sort(Helper.config(), Helper.quotedFileName(file) + " -r --csvIn(--escape '~') "
                                     + csvColumns
                                     + "--formatOut(Number)(A)(B)"
                                     + "--orderBy(Number desc)");

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvParserIgnoreEmptyLines() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        File file = Helper.createUnsortedFile(testName, in);
        Sort.sort(Helper.config(), Helper.quotedFileName(file) + " -r --csvIn(--ignoreEmptyLines) "
                                     + csvColumns
                                     + "--formatOut(Number)(A)(B)"
                                     + "--orderBy(Number desc)");

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvParserIgnoreSurroundingSpaces() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        File file = Helper.createUnsortedFile(testName, in);
        Sort.sort(Helper.config(),
                  Helper.quotedFileName(file) + " -r --csvIn(--ignoreSurroundingSpaces) "
                    + csvColumns
                    + "--formatOut(Number)(A)(B)"
                    + "--orderBy(Number desc)");

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvParserNullString() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        File file = Helper.createUnsortedFile("csvParserNullString", in);
        Sort.sort(Helper.config(),
                  Helper.quotedFileName(file) + " -o" + Helper.quotedFileName(file)
                    + " --csvIn(--NullString 'n/a') "
                    + csvColumns
                    + "--formatOut(Number)(A)(B)"
                    + "--orderBy(Number desc)");

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void csvParserQuote() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        File file = Helper.createUnsortedFile(testName, in);
        Sort.sort(Helper.config(), Helper.quotedFileName(file) + " -r --csvIn(--quote \"'\") "
                                     + csvColumns
                                     + "--formatOut(Number)(A)(B)"
                                     + "--orderBy(Number desc)");

        Assertions.assertTrue(file.delete());
    }

    @Test
    public void field1() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        byte[] csvData = "field1,field2".getBytes();

        CsvProvider csv = new CsvProvider(new boolean[]{
          true,
          false
        });

        CSVFormat format = CSVFormat.Predefined.Default.getFormat();
        byte[][] result = csv.decodeCsv(csvData, csvData.length, format);

        Assertions.assertEquals("field1", new String(result[0]));
    }

    @Test
    public void field1TrimNeeded() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        byte[] csvData = "\" \tfield1\t , \",field2".getBytes();
        CsvProvider csv = new CsvProvider(new boolean[]{
          true,
          false
        });
        CSVFormat format = CSVFormat.Predefined.Default.getFormat();
        byte[][] result = csv.decodeCsv(csvData, csvData.length, format);

        Assertions.assertEquals("field1\t ,", new String(result[0]));
    }

    @Test
    public void field1WithQuotedComma() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        byte[] csvData = "\"field1,\",field2".getBytes();
        CsvProvider csv = new CsvProvider(new boolean[]{
          true,
          false
        });
        CSVFormat format = CSVFormat.Predefined.Default.getFormat();
        byte[][] result = csv.decodeCsv(csvData, csvData.length, format);

        Assertions.assertEquals("field1,", new String(result[0]));
    }

    @Test
    public void field2() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        byte[] csvData = "field1,field2".getBytes();
        CsvProvider csv = new CsvProvider(new boolean[]{
          false,
          true
        });
        CSVFormat format = CSVFormat.Predefined.Default.getFormat();
        byte[][] result = csv.decodeCsv(csvData, csvData.length, format);

        Assertions.assertEquals("field2", new String(result[1]));
    }

    @Test
    public void field2WithQuotedComma() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        byte[] csvData = "\"field1,\",field2".getBytes();
        CsvProvider csv = new CsvProvider(new boolean[]{
          false,
          true
        });
        CSVFormat format = CSVFormat.Predefined.Default.getFormat();
        byte[][] result = csv.decodeCsv(csvData, csvData.length, format);

        Assertions.assertEquals("field2", new String(result[1]));
    }

    @Test
    public void keyCsvFields() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        Helper.createSysin(csvInput());

        try {
            FunnelContext context = Sort.sort(Helper.config(),
                                              "--csvIn () "
                                                + "--col(String -f1 -na)(String -f2 -nd)"
                                                + "--formatOut(a)(d)"
                                                + "--orderBy (d)(a desc)");
            Assertions.assertEquals(10, context.getWriteCount());
        } catch (Throwable pe) {
            Assertions.fail(pe.getMessage());
        }
    }

    @Test
    public void orderByField() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        Helper.createSysin(csvInput());

        try {
            FunnelContext context = Sort.sort(Helper.config(),
                                              "--csvIn () "
                                                + "--col(String -f1 -na)(String -f2 -nd)"
                                                + "--field(-n field -e 'a')"
                                                + "--formatOut(a)(d)"
                                                + "--orderBy (d)(field desc)");
            Assertions.assertEquals(10, context.getWriteCount());
        } catch (Throwable pe) {
            Assertions.fail(pe.getMessage());
        }
    }

    @Test
    public void keyCsvFieldsOnNonCsvFile() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(),
                      csvColumns
                        + "--formatOut(Number)(A)(B)"
                        + "--orderBy (Number)(A)");
            Assertions.fail("Expected a ParseException");

        } catch (SortException pe) {
            Assertions.assertEquals("unexpected CSV key (--field) on a non-CSV file", pe.getMessage());
        }
    }

    @Test
    public void normalKeysOnCsvFile() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(),
                      "--csvIn() --col(String -o0 -n Name) "
                        + "--formatOut(Name)"
                        + "--orderBy(Name)");
            Assertions.fail("Expected a ParseException");

        } catch (SortException pe) {
            Assertions.assertEquals("only CSV keys (--field) allowed on a CSV file", pe.getMessage());
        }
    }

    @Test
    public void repeatedField() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            Sort.sort(Helper.config(),
                      "--csvIn() "
                        + csvColumns
                        + "--formatOut(Number)(A)(B)"
                        + "--orderBy (A)(Number)(Number)");
            Assertions.fail("Expected a ParseException");

        } catch (SortException pe) {
            Assertions.assertEquals("sorting on the same field (--field 0) is not allowed", pe.getMessage());
        }
    }

    @Test
    public void sortField0Desc() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        List<String> out = new ArrayList<>();
        out.add("10,aaaaa,b");
        out.add("9,aaaa,b");
        out.add("8,aaa,b");
        out.add("7,aa,b");
        out.add("6,a,b");
        out.add("5,a,bbbbb");
        out.add("4,a,bbbb");
        out.add("3,a,bbb");
        out.add("2,a,bb");
        out.add("1,a,b");

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(
          Helper.config(),
          Helper.quotedFileName(file) + " -r --csvIn() "
            + csvColumns
            + "--formatOut(Number)(A)(B)"
            + "--orderBy(Number desc)");

        Assertions.assertEquals(10L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortField1Asc() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        List<String> out = new ArrayList<>();
        out.add("1,a,b");
        out.add("2,a,bb");
        out.add("3,a,bbb");
        out.add("4,a,bbbb");
        out.add("5,a,bbbbb");
        out.add("6,a,b");
        out.add("7,aa,b");
        out.add("8,aaa,b");
        out.add("9,aaaa,b");
        out.add("10,aaaaa,b");

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --csvIn() "
                                            + csvColumns
                                            + "--formatOut(Number)(A)(B)"
                                            + "--orderBy(A)");

        Assertions.assertEquals(10L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortField1Desc() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        List<String> out = new ArrayList<>();
        out.add("10,aaaaa,b");
        out.add("9,aaaa,b");
        out.add("8,aaa,b");
        out.add("7,aa,b");
        out.add("1,a,b");
        out.add("2,a,bb");
        out.add("3,a,bbb");
        out.add("4,a,bbbb");
        out.add("5,a,bbbbb");
        out.add("6,a,b");

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --csvIn() "
                                            + csvColumns
                                            + "--formatOut(Number)(A)(B)"
                                            + "--orderBy(A desc)");

        Assertions.assertEquals(10L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortField2Asc() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        List<String> out = new ArrayList<>();
        out.add("1,a,b");
        out.add("6,a,b");
        out.add("7,aa,b");
        out.add("8,aaa,b");
        out.add("9,aaaa,b");
        out.add("10,aaaaa,b");
        out.add("2,a,bb");
        out.add("3,a,bbb");
        out.add("4,a,bbbb");
        out.add("5,a,bbbbb");

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --csvIn() "
                                            + csvColumns
                                            + "--formatOut(Number)(A)(B)"
                                            + "--orderBy (B) ");

        Assertions.assertEquals(10L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortField2Desc() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        List<String> out = new ArrayList<>();
        out.add("5,a,bbbbb");
        out.add("4,a,bbbb");
        out.add("3,a,bbb");
        out.add("2,a,bb");
        out.add("1,a,b");
        out.add("6,a,b");
        out.add("7,aa,b");
        out.add("8,aaa,b");
        out.add("9,aaaa,b");
        out.add("10,aaaaa,b");

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --csvIn() "
                                            + csvColumns
                                            + "--formatOut(Number)(A)(B)"
                                            + "--orderBy(B desc)");

        Assertions.assertEquals(10L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortOnTwoFieldsAsc() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in = csvInput();

        List<String> out = new ArrayList<>();
        out.add("10,aaaaa,b");
        out.add("9,aaaa,b");
        out.add("8,aaa,b");
        out.add("7,aa,b");
        out.add("6,a,b");
        out.add("1,a,b");
        out.add("2,a,bb");
        out.add("3,a,bbb");
        out.add("4,a,bbbb");
        out.add("5,a,bbbbb");

        File file = Helper.createUnsortedFile(testName, in);
        FunnelContext context = Sort.sort(Helper.config(),
                                          Helper.quotedFileName(file) + " -r --csvIn() "
                                            + csvColumns
                                            + "--formatOut(Number)(A)(B)"
                                            + "--orderBy(B)(Number desc)");

        Assertions.assertEquals(10L, context.getRecordCount());
        Helper.compare(file, out);
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void twoFields() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        byte[] csvData = "\" \tfield1\t , \",field2".getBytes();
        CsvProvider csv = new CsvProvider(new boolean[]{
          true,
          true
        });
        CSVFormat format = CSVFormat.Predefined.Default.getFormat();
        byte[][] result = csv.decodeCsv(csvData, csvData.length, format);

        Assertions.assertEquals("field1\t ,", new String(result[0]));
        Assertions.assertEquals("field2", new String(result[1]));
    }
}
