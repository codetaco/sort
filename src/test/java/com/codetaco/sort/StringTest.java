package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StringTest {

    @Test
    public void caseMatters() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("z");
        in1.add("a");
        in1.add("M");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                                   + " -o " + Helper.quotedFileName(output)
                                                                   + " --col(string -o0 -l1 -n col1)"
                                                                   + " --orderby(col1 asc)");

        Assertions.assertEquals(3L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("M");
        expectedOut.add("a");
        expectedOut.add("z");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }

    @Test
    public void ignoreCase() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        File output = Helper.outFile(testName);

        List<String> in1 = new ArrayList<>();
        in1.add("z");
        in1.add("a");
        in1.add("M");

        File file1 = Helper.createUnsortedFile(testName, in1);
        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file1)
                                                                   + " -o " + Helper.quotedFileName(output)
                                                                   + " --col(string -o0 -l1 -n col1)"
                                                                   + " --orderby(col1 aasc)");

        Assertions.assertEquals(3L, context.getWriteCount());

        List<String> expectedOut = new ArrayList<>();
        expectedOut.add("a");
        expectedOut.add("M");
        expectedOut.add("z");

        Helper.compare(output, expectedOut);
        Assertions.assertTrue(file1.delete());
        Assertions.assertTrue(output.delete());
    }
}
