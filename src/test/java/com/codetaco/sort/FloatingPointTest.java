package com.codetaco.sort;

import com.codetaco.Helper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FloatingPointTest {
    @Test
    public void bigNumberNoFormat() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "FloatingPointTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$')"
                            + "--col(I, --path id -n id)"
                            + "     (Float, --path bigNumber -n bigNumber)"
                            + "     (Float, --path reallyBigNumber -n reallyBigNumber)"
                            + "--orderBy (bigNumber)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{bigNumber:1123.456,reallyBigNumber:1.123456E15,id:3},");
        expectedResults.append("{bigNumber:11234.56,reallyBigNumber:1.123456E25,id:2},");
        expectedResults.append("{bigNumber:112345.6,reallyBigNumber:1.1234567890123457E35,id:1}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void bigNumberFormat() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "FloatingPointTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$')"
                            + "--col(I, --path id -n id)"
                            + "     (Float, --path bigNumber -n bigNumber)"
                            + "     (Float, --path reallyBigNumber -n reallyBigNumber)"
                            + "--formatOut(id)(bigNumber)(reallyBigNumber)"
                            + "--orderBy (bigNumber)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{bigNumber:1123.456,reallyBigNumber:1.123456E15,id:3},");
        expectedResults.append("{bigNumber:11234.56,reallyBigNumber:1.123456E25,id:2},");
        expectedResults.append("{bigNumber:112345.6,reallyBigNumber:1.1234568E35,id:1}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void reallyBigNumberNoFormat() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "FloatingPointTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$')"
                            + "--col(I, --path id -n id)"
                            + "     (Float, --path bigNumber -n bigNumber)"
                            + "     (Float, --path reallyBigNumber -n reallyBigNumber)"
                            + "--orderBy (reallyBigNumber)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{bigNumber:1123.456,reallyBigNumber:1.123456E15,id:3},");
        expectedResults.append("{bigNumber:11234.56,reallyBigNumber:1.123456E25,id:2},");
        expectedResults.append("{bigNumber:112345.6,reallyBigNumber:1.1234567890123457E35,id:1}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void reallyBigNumberFormat() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "FloatingPointTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$')"
                            + "--col(I, --path id -n id)"
                            + "     (Float, --path bigNumber -n bigNumber)"
                            + "     (Float, --path reallyBigNumber -n reallyBigNumber)"
                            + "--formatOut(id)(bigNumber)(reallyBigNumber)"
                            + "--orderBy (reallyBigNumber)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{bigNumber:1123.456,reallyBigNumber:1.123456E15,id:3},");
        expectedResults.append("{bigNumber:11234.56,reallyBigNumber:1.123456E25,id:2},");
        expectedResults.append("{bigNumber:112345.6,reallyBigNumber:1.1234568E35,id:1}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void selectOnBigNumber() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "FloatingPointTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$')"
                            + "--col(I, --path id -n id)"
                            + "     (Float, --path bigNumber -n bigNumber)"
                            + "     (Float, --path reallyBigNumber -n reallyBigNumber)"
                            + "--where 'bigNumber>1000'"
                            + "--orderby (bigNumber)"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{bigNumber:1123.456,reallyBigNumber:1.123456E15,id:3},");
        expectedResults.append("{bigNumber:11234.56,reallyBigNumber:1.123456E25,id:2},");
        expectedResults.append("{bigNumber:112345.6,reallyBigNumber:1.1234567890123457E35,id:1}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }

    @Test
    public void exactness() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "FloatingPointTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$')"
                            + "--col(I, --path id -n id)"
                            + "     (Float, --path bigNumber -n bigNumber)"
                            + "     (Float, --path reallyBigNumber -n reallyBigNumber)"
                            + "--formatOut(id)"
                            + "            (-s1)(bigNumber --format '%20.0f')"
                            + "            (-s1)(reallyBigNumber --format '%35.0f' -l35)"
                            + "--vOut cr lf"));

        List<String> expectedResults = new ArrayList<>();
        expectedResults.add("1               112346 11234568299803154000000000000000000");
        expectedResults.add("2                11235          11234559438371584000000000");
        expectedResults.add("3                 1123                    1123456003342336");
        Helper.compare(outfile, expectedResults);
    }

    @Test
    public void selectOnReallyBigNumber() {
        File outfile = Helper.captureSysinSysoutWhileRunning(
          "FloatingPointTest.json",
          () -> Sort.sort(Helper.config(),
                          "--jsonin(--path '$')"
                            + "--col(I, --path id -n id)"
                            + "     (Float, --path bigNumber -n bigNumber)"
                            + "     (Float, --path reallyBigNumber -n reallyBigNumber)"
                            + "--where 'reallyBigNumber>1.123456E25'"));

        StringBuilder expectedResults = new StringBuilder();
        expectedResults.append('[');
        expectedResults.append("{bigNumber:112345.6,reallyBigNumber:1.1234567890123457E35,id:1}");
        expectedResults.append(']');
        Helper.compareJson(outfile, expectedResults.toString());
    }
}
