package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class URLTest {

    @Test
    public void fileCanNotBeURL() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        try {
            FunnelContext context = Sort.sort(Helper.config(),
                                                    "http://ip.jsontest.com/");
            Assertions.fail("expected exception");
        } catch (SortException e) {
            Assertions.assertEquals("file not found: http://ip.jsontest.com/",
                                    e.getMessage());
        }
    }

    @Test
    public void errorIfReplaceURL() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        try {
            FunnelContext context = Sort.sort(Helper.config(),
                                                    "--replace --url http://ip.jsontest.com/");
            Assertions.fail("expected exception");
        } catch (SortException e) {
            Assertions.assertEquals("--replace requires --inputFile, redirection or piped input is not allowed",
                                    e.getMessage());
        }
    }

    @Test
    public void errorIfFileAndURL() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        try {
            FunnelContext context = Sort.sort(Helper.config(),
                                                    "*.* --url http://ip.jsontest.com/");
            Assertions.fail("expected exception");
        } catch (SortException e) {
            Assertions.assertEquals("--inputFileName and --url can not be specified together",
                                    e.getMessage());
        }
    }
}
