package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import com.codetaco.sort.provider.AbstractInputCache;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class InputCacheTests {
    static private FunnelContext createDummyContext(InputStream in, PrintStream out) throws Exception {
        System.setIn(in);
        System.setOut(out);
        return new FunnelContext(Helper.config(), "");
    }

    @Test
    public void bufferPositioningLargeNumbers() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        long[] bufStarts = {
          0,
          32768
        };

        Assertions.assertEquals(0, AbstractInputCache.findBufferIndexForPosition(32767, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(32768, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(32846, bufStarts));
    }

    @Test
    public void bufferPositioningNotPow2() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        long[] bufStarts = {
          0,
          10,
          20,
          30
        };

        Assertions.assertEquals(0, AbstractInputCache.findBufferIndexForPosition(0, bufStarts));
        Assertions.assertEquals(0, AbstractInputCache.findBufferIndexForPosition(9, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(10, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(19, bufStarts));
        Assertions.assertEquals(2, AbstractInputCache.findBufferIndexForPosition(20, bufStarts));
        Assertions.assertEquals(2, AbstractInputCache.findBufferIndexForPosition(29, bufStarts));
        Assertions.assertEquals(3, AbstractInputCache.findBufferIndexForPosition(30, bufStarts));
    }

    @Test
    public void bufferPositioningPow2() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        long[] bufStarts = {
          0,
          10
        };

        Assertions.assertEquals(0, AbstractInputCache.findBufferIndexForPosition(0, bufStarts));
        Assertions.assertEquals(0, AbstractInputCache.findBufferIndexForPosition(9, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(10, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(19, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(20, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(29, bufStarts));
        Assertions.assertEquals(1, AbstractInputCache.findBufferIndexForPosition(30, bufStarts));
    }

    @Test
    public void inputStreamWith2BuffersByArray() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        int minRow = 100000;
        int maxRows = 104106;

        List<String> input = new ArrayList<>();
        for (long num = minRow; num < maxRows; num++) {
            input.add("" + num);
        }
        File infile = Helper.createUnsortedFile(testName, input);

        int lineSize = 6 + System.lineSeparator().length();

        InputStream testStream = new FileInputStream(infile);
        File file = Helper.outFileWhenInIsSysin();
        try (PrintStream outputStream = new PrintStream(new FileOutputStream(file))) {
            FunnelContext context = createDummyContext(testStream, outputStream);

            byte[] testBytes = new byte[8];
            for (long num = minRow; num < maxRows; num++) {
                context.getInputCache().read(context.inputFileIndex(), testBytes, (num - minRow) * lineSize, lineSize);
                Assertions.assertEquals("" + num, new String(testBytes).trim());
            }
        }
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void inputStreamWith2BuffersByByte() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        int minRow = 100000;
        int maxRows = 104106;

        List<String> input = new ArrayList<>();
        for (long num = minRow; num < maxRows; num++) {
            input.add("" + num);
        }
        File infile = Helper.createUnsortedFile(testName, input);

        int lineSize = 6 + System.lineSeparator().length();

        InputStream testStream = new FileInputStream(infile);
        File file = Helper.outFileWhenInIsSysin();
        try (PrintStream outputStream = new PrintStream(new FileOutputStream(file))) {
            FunnelContext context = createDummyContext(testStream, outputStream);

            byte[] testBytes = new byte[8];
            for (long num = minRow; num < maxRows; num++) {
                for (int b = 0; b < lineSize; b++) {
                    testBytes[b] = context.getInputCache().readNextByte();
                }
                Assertions.assertEquals("" + num, new String(testBytes).trim());
            }
        }
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void sortWith2Buffers() throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        int minRow = 100000;
        int maxRows = 104106;

        List<String> input = new ArrayList<>();
        for (long num = maxRows - 1; num >= minRow; num--) {
            input.add("" + num);
        }
        Helper.createSysin(input);

        File file = Helper.outFileWhenInIsSysin();
        try (PrintStream outputStream = new PrintStream(new FileOutputStream(file))) {
            System.setOut(outputStream);
            FunnelContext context = Sort.sort(Helper.config());
            outputStream.flush();
            Assertions.assertEquals(4106, context.getWriteCount());
        }
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            for (long num = minRow; num < maxRows; num++) {
                line = br.readLine();
                Assertions.assertEquals("" + num, line);
            }
        }
        Assertions.assertTrue(file.delete());
    }
}
