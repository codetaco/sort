package com.codetaco.sort;

import com.codetaco.Helper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BinaryTest {
    static final int binaryfind(long[] values, int max, long value) {
        int b = 0;
        int t = max;

        while (b < t) {

            int m = (t + b) >>> 1;
            long v = values[m];

            if (v == value) {
                return m;
            }

            if (value < v) {
                t = m;
                continue;
            }
            if (b == m) {
                return b;
            }
            b = m;
        }
        if (b == 0) {
            return -1;
        }
        return b;
    }

    static private int find(long[] values, int value) {
        long loop = 10000000;

        long start = System.currentTimeMillis();
        for (long x = 0; x < loop; x++) {
            binaryfind(values, values.length, value);
        }
        long end = System.currentTimeMillis();

        System.out.println("value " + value + " " + (long) ((double) loop / (double) (end - start))
                             + " / ms (15000 = best so far)");

        return binaryfind(values, values.length, value);
    }

    static private long[] values(int count, int incr) {
        long[] values = new long[count];
        long current = 0;
        for (int x = 0; x < count; x++) {
            values[x] = current += incr;
        }
        return values;
    }

    @Test
    public void binary0() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        long[] values = values(8000, 10);
        Assertions.assertEquals(-1, find(values, 0));
    }

    @Test
    public void binary10() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        long[] values = values(8000, 10);
        Assertions.assertEquals(0, find(values, 10));

    }

    @Test
    public void binary11() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        long[] values = values(8000, 10);
        Assertions.assertEquals(0, find(values, 11));
    }

    @Test
    public void binary20() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        long[] values = values(8000, 10);
        Assertions.assertEquals(1, find(values, 20));
    }

    @Test
    public void binary40000_direct_immediate_hit() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        long[] values = values(8000, 10);
        Assertions.assertEquals(4000, find(values, 40010));

    }

    @Test
    public void binary79999() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        long[] values = values(8000, 10);
        Assertions.assertEquals(7998, find(values, 79999));

    }

    @Test
    public void binary80000() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        long[] values = values(8000, 10);
        Assertions.assertEquals(7999, find(values, 80000));
    }

    @Test
    public void binary80001() throws Exception {
        String testName = Helper.testName();
        Helper.initializeFor(testName);
        long[] values = values(1000000, 10);
        Assertions.assertEquals(7999, find(values, 80001));
    }
}
