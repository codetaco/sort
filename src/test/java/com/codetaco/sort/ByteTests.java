package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ByteTests {

    @Test
    public void asHex() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("a\t\t\t");
        logFile.add("b\r\r\r");

        File file = Helper.createUnsortedFile(testName, logFile);

        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                + " --col"
                                                + " (String -l1 -n string)"
                                                + " (Byte -l3 -n bytes)"
                                                + " --where \"matches(bytes, '\t+')\""
                                                + " --orderBy (bytes desc)"
                                                + " --formatOut"
                                                + "  (-e 'toHex(bytes)' -l 6)(string)");
        });
        Assertions.assertEquals(1L, context.getWriteCount());
    }

    @Test
    public void asNumberInEqu() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("a\t\t\t");
        logFile.add("b\r\r\r");

        File file = Helper.createUnsortedFile(testName, logFile);
        Helper.captureSysoutWhileRunning(() -> {
            try {
                FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                     + " --col"
                                                                     + " (String -l1 -n string)"
                                                                     + " (Byte -l3 -n bytes)"
                                                                     + " --orderBy (bytes desc)"
                                                                     + " --formatOut"
                                                                     + "  (-e'bytes*2' -l 3)(string)");
                Assertions.fail("expected exception");
                return context;
            } catch (Exception e) {
                Assertions.assertEquals("op(multiply); invalid type byte[]: --format(bytes*2)", e.getMessage());
            } finally {
                Assertions.assertTrue(file.delete());
            }
            return null;
        });
    }

    @Test
    public void asSearchableField() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("a\t\t\t");
        logFile.add("b\r\r\r");

        File file = Helper.createUnsortedFile(testName, logFile);
        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                + " --col"
                                                + " (String -l1 -n string)"
                                                + " (Byte -l3 -n bytes)"
                                                + " --where \"matches(bytes, '\t+')\""
                                                + " --orderBy (bytes desc)"
                                                + " --formatOut"
                                                + "  (bytes)(string)");
        });
        Assertions.assertEquals(1L, context.getWriteCount());
        Assertions.assertTrue(file.delete());
    }

    @Test
    public void basicUse() {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> logFile = new ArrayList<>();
        logFile.add("a\t\t\t");
        logFile.add("b\r\r\r");

        File file = Helper.createUnsortedFile(testName, logFile);
        FunnelContext context = Helper.captureSysoutWhileRunning(() -> {
            return Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                + " --col"
                                                + " (String -l1 -n string)"
                                                + " (Byte -l3 -n bytes)"
                                                + " --orderBy (bytes desc)"
                                                + " --formatOut"
                                                + "  (bytes -l 3)(string)");
        });

        Assertions.assertEquals(2L, context.getWriteCount());
        Assertions.assertTrue(file.delete());
    }
}
