package com.codetaco.sort;

import com.codetaco.Helper;
import com.codetaco.sort.parameters.FunnelContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StopWhenTest {

    @Test
    public void stopAfter10()
      throws Throwable {
        String testName = Helper.testName();
        Helper.initializeFor(testName);

        List<String> in1 = new ArrayList<>();
        for (int x = 10000; x <= 99999; x += 100) {
            in1.add("" + x);
        }

        File file = Helper.createUnsortedFile(testName, in1);

        FunnelContext context = Sort.sort(Helper.config(), Helper.quotedFileName(file)
                                                                   + " -r "
                                                                   + " --stopWhen 'recordNumber > 1'");

        Assertions.assertEquals(1L, context.getWriteCount());
        List<String> exp = new ArrayList<>();
        exp.add("10000");
        Helper.compare(file, exp);

        Assertions.assertTrue(file.delete());
    }
}
